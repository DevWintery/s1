// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "EnemyHpBarWidget.generated.h"

/**
 *
 */
UCLASS()
class S1_API UEnemyHpBarWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	virtual void NativeOnInitialized() override;

	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

public:
	class UEnemyInfoViewModel* GetInfoViewModel() const { return InfoVM; }

public:
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
	//class UProgressBar* EnemyHpBar;

private:
	class UEnemyInfoViewModel* InfoVM;

	float DeltaTimeAcc = 0.f;
};
