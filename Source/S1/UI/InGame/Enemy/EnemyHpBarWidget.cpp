// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyHpBarWidget.h"
#include "EnemyInfoViewModel.h"
#include "MVVMSubsystem.h"
#include "View/MVVMView.h"
#include "StageGameMode.h"
#include "Kismet/GameplayStatics.h"

void UEnemyHpBarWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	InfoVM = NewObject<UEnemyInfoViewModel>();

	if (UMVVMView* View = UMVVMSubsystem::GetViewFromUserWidget(this))
	{
		View->SetViewModel(TEXT("EnemyInfoViewModel"), InfoVM);
	}
}

void UEnemyHpBarWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);
  
	if (AStageGameMode* GameMode = Cast<AStageGameMode>(UGameplayStatics::GetGameMode(GetWorld())))
	{
		if (GameMode->GetPlayingCine())
		{
			SetVisibility(ESlateVisibility::Collapsed);
			return;
		}
	}

	if (ESlateVisibility::HitTestInvisible == GetVisibility())
	{
		DeltaTimeAcc += InDeltaTime;

		if (5.f < DeltaTimeAcc)
		{
			SetVisibility(ESlateVisibility::Collapsed);
			DeltaTimeAcc = 0.f;
		}
	}

}
