// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameOverWidget.generated.h"

/**
 * 
 */
UCLASS()
class S1_API UGameOverWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	virtual void NativeConstruct() override;
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime);

public:
	UFUNCTION()
	void PressButtonSpectator();

	void SetScreen();

private:
	UPROPERTY(meta = (BindWidget))
	class UButton* ButtonSpectator;

	UPROPERTY(meta = (BindWidget))
	class UImage* GameOverImage;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* TextOfButton;

private:
	class UTexture2D* ImageOfDie;
	class UTexture2D* MissionFailed;
	class UTexture2D* MissionSucess;

private:
	bool bIsSetScreen = false;
};
