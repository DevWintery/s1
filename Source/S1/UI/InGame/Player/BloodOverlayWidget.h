// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "BloodOverlayWidget.generated.h"

/**
 * 
 */
UCLASS()
class S1_API UBloodOverlayWidget : public UUserWidget
{
	GENERATED_BODY()
protected:
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

public:
	class UImage* GetBloodOverlay() const {return BloodOverlay;}

private:
	UPROPERTY(meta = (BindWidget))
	class UImage* BloodOverlay;

private:
	float ImageAlpha = 0.f;
	bool bIsShowing = false;
	
};
