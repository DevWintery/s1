// Fill out your copyright notice in the Description page of Project Settings.


#include "BloodOverlayWidget.h"
#include "Components/Image.h"
#include "StageGameMode.h"
#include "Kismet/GameplayStatics.h"
#include "HeroPlayer.h"

void UBloodOverlayWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	if(AStageGameMode* GM = Cast<AStageGameMode>(UGameplayStatics::GetGameMode(GetWorld())))
	{
		if (GM->GetPlayingCine() || GM->GetIsClear())
		{
			SetVisibility(ESlateVisibility::Collapsed);
			ImageAlpha = 0.f;
			return;
		}
	}

	if (GetVisibility() == ESlateVisibility::Collapsed)
	{
		ImageAlpha = 0.f;
		return;
	}

	else if(GetVisibility() == ESlateVisibility::HitTestInvisible)
	{
		FLinearColor Color = BloodOverlay->GetColorAndOpacity();
		Color.A = ImageAlpha;
		BloodOverlay->SetColorAndOpacity(Color);

		ImageAlpha = bIsShowing ? ImageAlpha += GetWorld()->GetDeltaSeconds() : ImageAlpha -= GetWorld()->GetDeltaSeconds();

		if (bIsShowing && ImageAlpha >= 1.0f)
		{
			bIsShowing = false;
			ImageAlpha =  1.0f;
		}

		else if(!bIsShowing && ImageAlpha <= 0.f)
		{
			bIsShowing = true;
			ImageAlpha = 0.f;
		}
	}
}