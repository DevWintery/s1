// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/InGame/Player/HeroInfoWidget.h"
#include "PlayerInfoViewModel.h"
#include "MVVMSubsystem.h"
#include "View/MVVMView.h"
#include "Components/VerticalBox.h"	
#include "Components/HorizontalBox.h"	
#include "Blueprint/WidgetLayoutLibrary.h"

void UHeroInfoWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	InfoVM = NewObject<UPlayerInfoViewModel>();

	if (UMVVMView* View = UMVVMSubsystem::GetViewFromUserWidget(this))
	{
		View->SetViewModel(TEXT("PlayerInfoViewModel"), InfoVM);

	}
}

void UHeroInfoWidget::SetEquipWeapon(int WeaponIdx)
{
	switch (WeaponIdx)
	{
	case 1:
		PlayAnimation(SelectWeapon1);
	break;
	case 2:
		PlayAnimation(SelectWeapon2);
	break;
	default:
		break;
	}
}
