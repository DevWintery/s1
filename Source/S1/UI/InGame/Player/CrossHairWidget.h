// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "CrossHairWidget.generated.h"

/**
 *
 */
UCLASS()
class S1_API UCrossHairWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	void PlayAnimationFire();

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true, BindWidgetAnim), Transient)
	class UWidgetAnimation* Fire;
};
