// Fill out your copyright notice in the Description page of Project Settings.


#include "GameOverWidget.h"
#include "Components/Button.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "HeroController.h"
#include "S1GameInstance.h"

void UGameOverWidget::NativeConstruct()
{
	Super::NativeConstruct();

	ButtonSpectator->OnClicked.AddDynamic(this, &UGameOverWidget::PressButtonSpectator);

	ImageOfDie = LoadObject<UTexture2D>(NULL, TEXT("/Game/S1/UI/Lobby/Texture/YouDied.YouDied"), NULL, LOAD_None, NULL);;
	MissionFailed = LoadObject<UTexture2D>(NULL, TEXT("/Game/S1/UI/InGame/Player/Texture/MissionFailed.MissionFailed"), NULL, LOAD_None, NULL);;
	MissionSucess = LoadObject<UTexture2D>(NULL, TEXT("/Game/S1/UI/InGame/Player/Texture/MissionSuccess.MissionSuccess"), NULL, LOAD_None, NULL);;
}

void UGameOverWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	if (IsInViewport() || bIsSetScreen)
	{
		SetScreen();
		bIsSetScreen = false;
	}
}

void UGameOverWidget::PressButtonSpectator()
{
	US1GameInstance* GI = Cast<US1GameInstance>(GetGameInstance());

	if (0 == GI->GetNumOfPlayer())
	{
		Protocol::C_LEAVE_GAME pkt;
		SEND_PACKET(pkt);

		if (IsInViewport())
		{
			this->RemoveFromParent();
		}
	}

	else
	{
		if (AHeroController* PC = Cast<AHeroController>(UGameplayStatics::GetPlayerController(GetWorld(), 0)))
		{
			PC->ChangeModeSpectator();
		}
	}
}

void UGameOverWidget::SetScreen()
{
	US1GameInstance* GI = Cast<US1GameInstance>(GetGameInstance());
	
	if (0 == GI->GetNumOfPlayer())
	{
		GameOverImage->SetBrushFromTexture(MissionFailed);
		TextOfButton->SetText(FText::FromString(TEXT("Exit")));
	}

	else
	{
		GameOverImage->SetBrushFromTexture(ImageOfDie);
		TextOfButton->SetText(FText::FromString(TEXT("Spectate")));
	}

	bIsSetScreen = true;
  
}
