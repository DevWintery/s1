// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "S1Defines.h"
#include "HeroInGameHUD.generated.h"

/**
 * 
 */
UCLASS()
class S1_API AHeroInGameHUD : public AHUD
{
	GENERATED_BODY()

public:
	AHeroInGameHUD();

public:
	void PawnKilled();
	void SpectatorOption(bool bShow);
	void StageClear();
	void PlayerHUDAnimation(EUIType UIType);
	void SetMinimapTexture();
	void SetQuestSummary(bool bPlayTimer);
	void PlayCine(bool bPlayCine);
	void ChangeModeSpectator();
	void Interactable(bool bCanInteractable);

protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	virtual void DrawHUD() override;

private:
	UPROPERTY(EditAnywhere)
	TSubclassOf<class UUserWidget> HeroHUDClass;

	class UUserWidget* HeroHUDWidget;
	
};
