// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "HeroInfoWidget.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class S1_API UHeroInfoWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	virtual void NativeOnInitialized() override;

public:
	void SetEquipWeapon(int WeaponIdx);

public:
	class UPlayerInfoViewModel* GetInfoViewModel() const { return InfoVM; }

private:
	UPROPERTY(meta = (BindWidget))
	class UVerticalBox* ListOfAmmo;

	UPROPERTY(meta = (BindWidget))
	class UHorizontalBox* BoxOfWeaponIcon;

	UPROPERTY(meta = (BindWidgetAnim), Transient)
	class UWidgetAnimation* SelectWeapon1;

	UPROPERTY(meta = (BindWidgetAnim), Transient)
	class UWidgetAnimation* SelectWeapon2;

private:
	class UPlayerInfoViewModel* InfoVM;

	
};
