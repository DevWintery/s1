// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "S1Defines.h"
#include "HeroHUDWidget.generated.h"

/**
 *
 */
UCLASS()
class S1_API UHeroHUDWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	virtual void NativeConstruct() override;
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

public:
	void ShowUI(EUIType UIType, bool _bResult);
	void ShowAllHUD(bool bIsShow);
	void PlayWidgetAnimation(EUIType UIType);
	void SetMinimapTexture();

private:
	void CalcSightAngle();

private:
	UPROPERTY(meta = (BindWidget))
	class UOverlay* QuestSummaryOL;

	UPROPERTY(meta = (BindWidget))
	class UOverlay* TimerOL;

	UPROPERTY(meta = (BindWidget))
	class UOverlay* CrossHairOL;

	UPROPERTY(meta = (BindWidget))
	class UOverlay* BloodOL;

	UPROPERTY(meta = (BindWidget))
	class UOverlay* MinimapOL;

	UPROPERTY(meta = (BindWidget))
	class UOverlay* BlurEffectOL;

	UPROPERTY(meta = (BindWidget))
	class UOverlay* InteractOL;

	TMap<EUIType, UOverlay*> UIList;

private:
	UPROPERTY(meta = (BindWidgetAnim), Transient)
	class UWidgetAnimation* StartTimer;

	UPROPERTY(meta = (BindWidgetAnim), Transient)
	class UWidgetAnimation* SetQuestSummary;

private:
	bool bIsRight = true;
};
