// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/InGame/Player/HeroInGameHUD.h"
#include "HeroHUDWidget.h"
#include "HeroPlayer.h"
#include "StageGameMode.h"

AHeroInGameHUD::AHeroInGameHUD()
{
	static ConstructorHelpers::FClassFinder<UHeroHUDWidget> HUD_CLASS(TEXT("/Game/S1/UI/InGame/Player/WBP_PlayerHUD.WBP_PlayerHUD_C"));

	if (HUD_CLASS.Succeeded())
	{
		HeroHUDClass = HUD_CLASS.Class;
	}
}

void AHeroInGameHUD::BeginPlay()
{
	Super::BeginPlay();

	if (nullptr != HeroHUDClass)
	{
		HeroHUDWidget = CreateWidget<UHeroHUDWidget>(GetWorld(), HeroHUDClass);
	}

	if (nullptr != HeroHUDWidget)
	{
		HeroHUDWidget->AddToViewport();
	}
}

void AHeroInGameHUD::DrawHUD()
{
	Super::DrawHUD();
}


void AHeroInGameHUD::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

void AHeroInGameHUD::PawnKilled()
{
	Cast<UHeroHUDWidget>(HeroHUDWidget)->ShowAllHUD(false);
	Cast<UHeroHUDWidget>(HeroHUDWidget)->ShowUI(EUIType::GAMEOVER, true);
}

void AHeroInGameHUD::SpectatorOption(bool bShow)
{
	Cast<UHeroHUDWidget>(HeroHUDWidget)->ShowUI(EUIType::GAMEOVER, !bShow);
}

void AHeroInGameHUD::StageClear()
{
	Cast<UHeroHUDWidget>(HeroHUDWidget)->ShowAllHUD(false);
}

void AHeroInGameHUD::PlayerHUDAnimation(EUIType UIType)
{
	Cast<UHeroHUDWidget>(HeroHUDWidget)->PlayWidgetAnimation(UIType);
}

void AHeroInGameHUD::SetMinimapTexture()
{
	Cast<UHeroHUDWidget>(HeroHUDWidget)->SetMinimapTexture();
}

void AHeroInGameHUD::SetQuestSummary(bool bPlayTimer)
{
	Cast<UHeroHUDWidget>(HeroHUDWidget)->ShowUI(EUIType::QUESTSUMMARY, true);

	if (bPlayTimer)
	{
		Cast<UHeroHUDWidget>(HeroHUDWidget)->ShowUI(EUIType::TIMER, true);
	}
}

void AHeroInGameHUD::PlayCine(bool bPlayCine)
{
	Cast<UHeroHUDWidget>(HeroHUDWidget)->ShowAllHUD(!bPlayCine);
	//Cast<UHeroHUDWidget>(HeroHUDWidget)->ShowUI(EUIType::QUESTSUMMARY);
}


void AHeroInGameHUD::ChangeModeSpectator()
{
}

void AHeroInGameHUD::Interactable(bool bCanInteractable)
{
	Cast<UHeroHUDWidget>(HeroHUDWidget)->ShowUI(EUIType::INTERACT, bCanInteractable);
}
