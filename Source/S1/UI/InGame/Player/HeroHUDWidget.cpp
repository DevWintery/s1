// Fill out your copyright notice in the Description page of Project Settings.


#include "HeroHUDWidget.h"
#include "CrossHairWidget.h"
#include "BloodOverlayWidget.h"
#include "GoalIconWidget.h"
#include "Kismet/GameplayStatics.h"
#include "HeroPlayer.h"
#include "Components/Image.h"
#include "QuestManager.h"
#include "Components/TextBlock.h"
#include "TimerWidget.h"
#include "Components/Overlay.h"
#include "BlurEffectWidget.h"
#include "Camera/CameraComponent.h"
#include "StageGameMode.h"

void UHeroHUDWidget::NativeConstruct()
{
	Super::NativeConstruct();

	UIList.Emplace(EUIType::BLOODOVERLAY, BloodOL);
	UIList.Emplace(EUIType::CROSSHAIR, CrossHairOL);
	UIList.Emplace(EUIType::MINIMAP, MinimapOL);
	UIList.Emplace(EUIType::TIMER, TimerOL);
	UIList.Emplace(EUIType::QUESTSUMMARY, QuestSummaryOL);
	UIList.Emplace(EUIType::BLUREFFECT, BlurEffectOL);
	UIList.Emplace(EUIType::INTERACT, InteractOL);

	
}

void UHeroHUDWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	if (AStageGameMode* GM = Cast<AStageGameMode>(UGameplayStatics::GetGameMode(GetWorld())))
	{
		if (GM->GetPlayingCine())
		{
			return;
		}
	}

	if (GetOwningPlayerPawn())
	{
		ShowUI(EUIType::CROSSHAIR, Cast<AHeroPlayer>(GetOwningPlayerPawn())->GetIsAim());

		bool Result = (Cast<AHeroPlayer>(GetOwningPlayerPawn())->GetCurrentHp() / Cast<AHeroPlayer>(GetOwningPlayerPawn())->GetMaxHp()) < 0.8f;

		ShowUI(EUIType::BLOODOVERLAY, Result);
		ShowUI(EUIType::INTERACT, Cast<AHeroPlayer>(GetOwningPlayerPawn())->GetNumOfInteractObject() > 0);

		if (UImage* MinimapSight = Cast<UImage>(MinimapOL->GetChildAt(2)))
		{
			CalcSightAngle();
		}
	}

}

void UHeroHUDWidget::ShowUI(EUIType UIType, bool _bResult)
{
	switch (UIType)
	{
	case EUIType::CROSSHAIR:
	{
		if (_bResult)
		{
			CrossHairOL->SetVisibility(ESlateVisibility::HitTestInvisible);
		}
		else
		{
			CrossHairOL->SetVisibility(ESlateVisibility::Collapsed);
		}
	}
	break;

	case EUIType::BLOODOVERLAY:
	{
		if (_bResult)
		{
			BloodOL->GetChildAt(0)->SetVisibility(ESlateVisibility::HitTestInvisible);
		}
		else
		{
			BloodOL->GetChildAt(0)->SetVisibility(ESlateVisibility::Collapsed);
		}
	}

	break;

	case EUIType::QUESTSUMMARY:
		if (_bResult)
		{
			QuestSummaryOL->SetVisibility(ESlateVisibility::HitTestInvisible);
			UWidget* QuestSummaryText = QuestSummaryOL->GetChildAt(1);

			UGameInstance* GameInstance = GetGameInstance();
			UQuestManager* QuestManager = GameInstance->GetSubsystem<UQuestManager>();
			Cast<UTextBlock>(QuestSummaryText)->SetText(FText::FromString(QuestManager->GetSummary()));
		}

		else
		{
			QuestSummaryOL->SetVisibility(ESlateVisibility::Collapsed);
		}
		break;

	case EUIType::MINIMAP:
	{
		if (_bResult)
		{
			MinimapOL->SetVisibility(ESlateVisibility::HitTestInvisible);
		}

		else
		{
			MinimapOL->SetVisibility(ESlateVisibility::Collapsed);
		}
	}
	break;

	case EUIType::TIMER:
	{
		if (_bResult)
		{
			UGameInstance* GameInstance = GetGameInstance();
			UQuestManager* QuestManager = GameInstance->GetSubsystem<UQuestManager>();

			UTimerWidget* Timer = Cast<UTimerWidget>(TimerOL->GetChildAt(0));
			Timer->SetTimer(QuestManager->GetTimerDelay(), _bResult);
			PlayAnimation(StartTimer);
			TimerOL->SetVisibility(ESlateVisibility::HitTestInvisible);
		}

		else
		{
			TimerOL->SetVisibility(ESlateVisibility::Collapsed);
		}
	}
	break;
	case EUIType::INTERACT:
	{
		if (_bResult)
		{
			InteractOL->SetVisibility(ESlateVisibility::HitTestInvisible);
		}

		else
		{
			InteractOL->SetVisibility(ESlateVisibility::Collapsed);
		}
	}
	break;

	default:
		break;
	}
}

void UHeroHUDWidget::ShowAllHUD(bool bIsShow)
{
	if (bIsShow)
	{
		for (auto& UI : UIList)
		{
			UOverlay* OL = (UI.Value);
			OL->SetVisibility(ESlateVisibility::HitTestInvisible);
		}
	}

	else
	{
		for (auto& UI : UIList)
		{
			UOverlay* OL = (UI.Value);
			OL->SetVisibility(ESlateVisibility::Collapsed);
		}
	}
}

void UHeroHUDWidget::PlayWidgetAnimation(EUIType UIType)
{
	switch (UIType)
	{
	case EUIType::CROSSHAIR:
	{
		if (UCrossHairWidget* CrossHair = Cast<UCrossHairWidget>(CrossHairOL->GetChildAt(0)))
		{
			CrossHair->PlayAnimationFire();
		}
	}
	break;

	case EUIType::QUESTSUMMARY:
		PlayAnimation(SetQuestSummary);
		break;

	case EUIType::BLUREFFECT:
	{
		if (UBlurEffectWidget* BlurEffect = Cast<UBlurEffectWidget>(BlurEffectOL->GetChildAt(0)))
		{
			BlurEffect->PlayAnimationBlur();
		}
	}
	break;

	default:
		break;
	}
}

void UHeroHUDWidget::SetMinimapTexture()
{
	if (AHeroPlayer* Player = Cast<AHeroPlayer>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)))
	{
		if (UImage* Minimap = Cast<UImage>(MinimapOL->GetChildAt(0)))
		{
			Minimap->SetBrushFromMaterial(Player->GetMinimapMaterial());
		}
	}
}

void UHeroHUDWidget::CalcSightAngle()
{
	if (AHeroPlayer* HeroPlayer = Cast<AHeroPlayer>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)))
	{
		//카메라 Forward 벡터
		FVector CamForwardVector = HeroPlayer->GetFollowCamera()->GetComponentRotation().Vector();
		CamForwardVector.Normalize();

		// Theta 값 구하기
		float DotProductResult = 0.f;

		DotProductResult = FMath::RadiansToDegrees(FMath::Acos(FVector::DotProduct(CamForwardVector, FVector(1.f, 0.f, 0.f))));

		if (0.f < FVector::DotProduct(FVector(0.f, 0.f, 1.f), FVector::CrossProduct(CamForwardVector, FVector(1.f, 0.f, 0.f))))
		{
			//오른쪽
			DotProductResult *= -1.f;
		}

		else
		{
			//왼쪽
		}

		if (UImage* MinimapSight = Cast<UImage>(MinimapOL->GetChildAt(2)))
		{
			FWidgetTransform Transform = MinimapSight->RenderTransform;
			Transform.Angle = DotProductResult;
			MinimapSight->SetRenderTransform(Transform);
		}
	}
}

