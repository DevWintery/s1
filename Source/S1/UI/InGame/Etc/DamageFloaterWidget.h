// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "DamageFloaterWidget.generated.h"

/**
 * 
 */
UCLASS()
class S1_API UDamageFloaterWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UDamageFloaterWidget(const FObjectInitializer &ObjectInitializer);

public:
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

public:
	void SetDamageFloater(int _NumOfDamage, bool _bIsCritical = false);

private:
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* DamageText;

	UPROPERTY(meta = (BindWidgetAnim), Transient)
	class UWidgetAnimation* DamageFloater;

	UPROPERTY()
	class UMaterialInterface* CriticalMT;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Color, meta = (AllowPrivateAccess = true))
	FLinearColor ColorValue;
};
