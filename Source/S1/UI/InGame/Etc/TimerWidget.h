// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TimerWidget.generated.h"

/**
 * 
 */
UCLASS()
class S1_API UTimerWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;
	
public:
	void SetTimer(float Time, bool bTimerStart);

private:
	UPROPERTY(meta = (BindWidget))
	class UImage* TimerBG;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* TextOfTimer;

	float CurrentTime;
	float MaxTime;
	bool bStart;
	FLinearColor StartColor;
	FLinearColor FinishColor;
	
};
