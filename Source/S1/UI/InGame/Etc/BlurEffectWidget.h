// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "BlurEffectWidget.generated.h"

/**
 * 
 */
UCLASS()
class S1_API UBlurEffectWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	void PlayAnimationBlur();

private:
	UPROPERTY(meta = (BindWidgetAnim), Transient)
	class UWidgetAnimation* BlurStrength;
};
