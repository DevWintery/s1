// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GoalIconWidget.generated.h"

/**
 * 
 */
UCLASS()
class S1_API UGoalIconWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	UGoalIconWidget(const FObjectInitializer& ObjectInitializer);

public:
	virtual void NativeConstruct() override; 
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

public:
	void SetGoalLocation(FVector CurentGoalLocation);
	void SetIsWidgetComponent(bool bIsResult);
	void SetText(FString Text);
	void SetTarget(class ABasePlayer* CurrentTarget);

private:
	FVector2D CalculatePos();

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget, AllowPrivateAccess))
	class UImage* GoalIcon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget, AllowPrivateAccess))
	class UTextBlock* Distance;

	FVector GoalLocation;

	UPROPERTY(EditAnywhere)
	class UDataTable* QuestData;

private:
	bool bIsWidgetComponent = false;

	class ABasePlayer* Target;
};
