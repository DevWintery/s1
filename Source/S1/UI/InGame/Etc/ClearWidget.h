// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ClearWidget.generated.h"

/**
 * 
 */
UCLASS()
class S1_API UClearWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	virtual void NativeConstruct() override;
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

public:
	UFUNCTION()
	void OpenLobby();

	void SetReward(int Gold, int Sp);

private:
	UPROPERTY(meta = (BindWidget, AllowPrivateAccess))
	class UButton* ButtonExit;

	UPROPERTY(meta = (BindWidget, AllowPrivateAccess))
	class UOverlay* SummaryOL_1;

	UPROPERTY(meta = (BindWidget, AllowPrivateAccess))
	class UOverlay* SummaryOL_2;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* TextOfGold;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* TextOfSp;

private:
	UPROPERTY(meta = (BindWidgetAnim), Transient)
	class UWidgetAnimation* ClearReward;

};
