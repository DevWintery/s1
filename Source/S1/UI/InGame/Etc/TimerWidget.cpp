// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/InGame/Etc/TimerWidget.h"
#include "Components/TextBlock.h"
#include "Components/Image.h"

void UTimerWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	if (bStart)
	{
		CurrentTime -= InDeltaTime;

		CurrentTime = FMath::Max(CurrentTime, 0.f);

		if (0.f == CurrentTime)
		{
			bStart = false;
			return;
		}

		int IntTime = CurrentTime;

		float RationalTime = CurrentTime - static_cast<float>(IntTime);

		FString RationalTimeStr = FString::SanitizeFloat(RationalTime);
		FString TimeStr{};

		if (10.f > CurrentTime)
		{
			TimeStr = "0";
		}

		TimeStr += FString::FromInt(IntTime);
		TimeStr += ":";

		if (RationalTimeStr.Len() > 2)
		{
			TimeStr += FString::SanitizeFloat(RationalTime)[2];
		}

		if (RationalTimeStr.Len() > 3)
		{
			TimeStr += FString::SanitizeFloat(RationalTime)[3];
		}

		else
		{
			TimeStr += "0";
		}
	


		FText Time = FText::FromString(TimeStr);

		TextOfTimer->SetText(Time);

		float TimeRatio = fabs((CurrentTime / MaxTime) - 1.f);

		FLinearColor CurrnetColor;
		CurrnetColor.A = 0.5f;

		CurrnetColor.R = FMath::Lerp(StartColor.R, FinishColor.R, TimeRatio);
		CurrnetColor.G = FMath::Lerp(StartColor.G, FinishColor.G, TimeRatio);
		CurrnetColor.B = FMath::Lerp(StartColor.B, FinishColor.B, TimeRatio);

		TimerBG->SetBrushTintColor(CurrnetColor);
	}

}

void UTimerWidget::SetTimer(float Time, bool bTimerStart)
{
	CurrentTime = Time;
	MaxTime = Time;
	bStart = bTimerStart;
	StartColor = FLinearColor(0.f, 0.5, 1.f, 0.5f);
	FinishColor = FLinearColor(1.f, 0.f, 0.f, 0.5f);
}