// Fill out your copyright notice in the Description page of Project Settings.


#include "DamageFloaterWidget.h"
#include "Components/TextBlock.h"
#include "Materials/MaterialInterface.h"
#include "Styling/SlateColor.h"


UDamageFloaterWidget::UDamageFloaterWidget(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> MT_CRITICAL(TEXT("/Game/S1/UI/InGame/Player/Font/Material/MT_FontCritical.MT_FontCritical"));

	if (MT_CRITICAL.Succeeded())
	{
		CriticalMT = MT_CRITICAL.Object;
	}
}

void UDamageFloaterWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	if (!IsAnimationPlaying(DamageFloater))
	{
		this->RemoveFromParent();
	}
}

void UDamageFloaterWidget::SetDamageFloater(int _NumOfDamage, bool _bIsCritical)
{
	FString TotalStr {};
	FString DamageStr {};

	DamageStr = FString::FromInt(_NumOfDamage).Reverse();

	int NumOfComma = 3;

	for (int i = 0; i < DamageStr.Len();++i)
	{
		if(i == NumOfComma)
		{
			TotalStr += ",";
			TotalStr += DamageStr[i];
			NumOfComma += 3;
		}
		else
		{
			TotalStr += DamageStr[i];
		}
	}

	TotalStr[TotalStr.Len() - 1] = DamageStr[DamageStr.Len() - 1];

	TotalStr = TotalStr.Reverse();

	FText damageText = FText::FromString(TotalStr);

	DamageText->SetText(damageText);

	if (_bIsCritical)
	{
		DamageText->SetColorAndOpacity(FLinearColor(1.0f, 0.6f, 0.f, 1.0f));
		DamageText->SetFontOutlineMaterial(CriticalMT);
	}

	PlayAnimation(DamageFloater);

	//생각해보니까 색을 지정하는 게 아니라 애니메이션을 나눠서 그 애니메이션에서 색을 나누는게 더 좋을 거 같다. 
}
