// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/InGame/Etc/GoalIconWidget.h"
#include "Kismet/GameplayStatics.h"
#include "Blueprint/WidgetLayoutLibrary.h"
#include "HeroPlayer.h"
#include "QuestData.h"
#include "Engine/DataTable.h"
#include "Components/TextBlock.h"
#include "Camera/CameraComponent.h"
#include "StageGameMode.h"

UGoalIconWidget::UGoalIconWidget(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{
	static ConstructorHelpers::FObjectFinder<UDataTable> QUESTDATA(TEXT("/Game/S1/DataTables/DT_QuestData.DT_QuestData"));

	if (QUESTDATA.Succeeded())
	{
		QuestData = QUESTDATA.Object;
	}
}

void UGoalIconWidget::NativeConstruct()
{
	Super::NativeConstruct();

	Target = Cast<ABasePlayer>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
}

void UGoalIconWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	if(!Target)
	{
		return;
	}

	if (AStageGameMode* GM = Cast<AStageGameMode>(UGameplayStatics::GetGameMode(GetWorld())))
	{
		if (GM->GetPlayingCine())
		{
			SetVisibility(ESlateVisibility::Collapsed);
			return;
		}
	}

	if (!bIsWidgetComponent)
	{
		if (FVector(0.f, 0.f, 0.f) == GoalLocation)
		{
			SetVisibility(ESlateVisibility::Collapsed);
			return;
		}
		else if (ESlateVisibility::HitTestInvisible == GetVisibility())
		{
			SetPositionInViewport(CalculatePos());
		} 
	}

	int IntDistance = (Target->GetActorLocation() - GoalLocation).Length() / 100;

	SetText(FString::FromInt(IntDistance));

}

void UGoalIconWidget::SetGoalLocation(FVector CurentGoalLocation)
{
	GoalLocation = CurentGoalLocation;
}

void UGoalIconWidget::SetIsWidgetComponent(bool bIsResult)
{
	bIsWidgetComponent = bIsResult;
}

void UGoalIconWidget::SetText(FString Text)
{
	Text += 'm';
	Distance->SetText(FText::FromString(Text));
}

void UGoalIconWidget::SetTarget(ABasePlayer* CurrentTarget)
{
	Target = CurrentTarget;
}

FVector2D UGoalIconWidget::CalculatePos()
{	// 아이콘 띄울 범위 구하기

	FVector2D viewportSize = UWidgetLayoutLibrary::GetViewportSize(GetWorld());
	FVector2D WindowPos;

	float WindowMinX = viewportSize.X / 4;
	float WindowMinY = viewportSize.Y / 4;
	float WindowMaxX = viewportSize.X / 4 * 3;  
	float WindowMaxY = viewportSize.Y / 4 * 3;

	//카메라 위치
	FVector CamPosition = Target->GetFollowCamera()->GetComponentLocation();
	//CamPosition.Z = 0.f;

	//타겟 위치
	FVector TargetPosition = GoalLocation;
	//TargetPosition.Z = 0.f;

	//카메라 Forward 벡터
	FVector CamForwardVector = Target->GetFollowCamera()->GetComponentRotation().Vector();
	CamForwardVector.Normalize();

	//플레이어 Forward 벡터
	//FVector PlayerForwardVector = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)->GetActorForwardVector();
	//PlayerForwardVector.Normalize();

	FVector CamToTarget = TargetPosition - CamPosition;
	CamToTarget.Normalize(); // 벡터 정규화

	FVector ForwardVector = CamForwardVector;

	// Theta 값 구하기
	float DotProductResult = 0.f;

	float correctionValue = 90.f;

	//if (UGameplayStatics::GetPlayerCameraManager(GetWorld(), 0)->GetCameraLocation().Z > UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)->GetActorLocation().Z)
	//{
	//	ForwardVector = PlayerForwardVector;

	//	correctionValue = 0.f;
	//}

	//else
	//{
	//	ForwardVector = CamForwardVector;

	//	correctionValue = 90.f;
	//}

	DotProductResult = FMath::RadiansToDegrees(FMath::Acos(FVector::DotProduct(ForwardVector, CamToTarget)));

	//DotProductResult += correctionValue;

	//좌우 판단
	if (0.f < FVector::DotProduct(FVector(0.f, 0.f, 1.f), FVector::CrossProduct(ForwardVector, CamToTarget)))
	{
		//오른쪽

		if (0.f <= DotProductResult && 90.f > DotProductResult)
		{


			if (45.f >= DotProductResult)
			{
				WindowPos.X = FMath::Lerp(viewportSize.X / 2.f, WindowMaxX, DotProductResult / 45.f);
				WindowPos.Y = WindowMinY;
			}

			else if (90.f > DotProductResult)
			{
				WindowPos.X = WindowMaxX;
				WindowPos.Y = FMath::Lerp(WindowMinY, viewportSize.Y / 2.f, (DotProductResult - 45.f) / 45.f);
			}
		}

		else if (90.f <= DotProductResult && 180 >= DotProductResult)
		{

			if (135.f >= DotProductResult)
			{
				WindowPos.X = WindowMaxX;
				WindowPos.Y = FMath::Lerp(viewportSize.Y / 2.f, WindowMaxY, (DotProductResult - 90.f) / 45.f);
			}

			else
			{
				WindowPos.X = FMath::Lerp(viewportSize.X / 2.f, WindowMinX, (DotProductResult - 135.f) / 45.f);
				WindowPos.Y = WindowMaxY;
			}
		}
	}

	else
	{
		//왼쪽

		if (0.f <= DotProductResult && 90.f > DotProductResult)
		{

			if (45.f >= DotProductResult)
			{
				WindowPos.X = FMath::Lerp(viewportSize.X / 2.f, WindowMinX, DotProductResult / 45.f);
				WindowPos.Y = WindowMinY;
			}

			else
			{
				WindowPos.X = WindowMinX;
				WindowPos.Y = FMath::Lerp(WindowMinY, viewportSize.Y / 2.f, (DotProductResult - 45.f) / 45.f);
			}
		}

		else if (90.f <= DotProductResult && 180 >= DotProductResult)
		{

			if (135.f >= DotProductResult)
			{
				WindowPos.X = WindowMinX;
				WindowPos.Y = FMath::Lerp(viewportSize.Y / 2.f, WindowMaxY, (DotProductResult - 90.f) / 45.f);
			}

			else
			{
				WindowPos.X = FMath::Lerp(WindowMinX, viewportSize.X / 2.f, (DotProductResult - 135.f) / 45.f);
				WindowPos.Y = WindowMaxY;
			}
		}
	}

	return WindowPos;
}
