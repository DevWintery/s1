// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/InGame/Etc/SpectatorWidget.h"
#include "Components/Button.h"
#include "HeroController.h"
#include "HeroPlayer.h"
#include "Kismet/GameplayStatics.h"
#include "StageGameMode.h"
#include "UIManager.h"

void USpectatorWidget::NativeConstruct()
{
	Super::NativeConstruct();

	ButtonFinish->OnClicked.AddDynamic(this, &USpectatorWidget::FinishSpectator);
}

void USpectatorWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	if(AStageGameMode* GM = Cast<AStageGameMode>(UGameplayStatics::GetGameMode(GetWorld())))
	{
		if (GM->GetIsClear())
		{
			RemoveFromParent();
		}
	}
}

void USpectatorWidget::FinishSpectator()
{
	if (AHeroController* HC = Cast<AHeroController>(UGameplayStatics::GetPlayerController(GetWorld(), 0)))
	{
		HC->FinishSpectator();
	}
}
