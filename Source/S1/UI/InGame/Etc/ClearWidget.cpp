// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/InGame/Etc/ClearWidget.h"
#include "Components/Button.h"
#include "S1GameInstance.h"
#include "Kismet/GameplayStatics.h"
#include "Animation/WidgetAnimation.h"
#include "Components/Overlay.h"
#include "Components/TextBlock.h"
#include "S1Defines.h"

void UClearWidget::NativeConstruct()
{
	Super::NativeConstruct();

	PlayAnimation(ClearReward);

	ButtonExit->OnClicked.AddDynamic(this, &UClearWidget::OpenLobby);
}

void UClearWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);
}

void UClearWidget::OpenLobby()
{
	Protocol::C_LEAVE_GAME pkt;
	SEND_PACKET(pkt);

	if (IsInViewport())
	{
		this->RemoveFromParent();
	}
}

void UClearWidget::SetReward(int Gold, int Sp)
{
	TextOfGold->SetText(FText::FromString(FString::FromInt(Gold)));
	TextOfSp->SetText(FText::FromString(FString::FromInt(Sp)));
}
