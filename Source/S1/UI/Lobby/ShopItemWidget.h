// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Blueprint/IUserObjectListEntry.h"
#include "S1Defines.h"
#include "ShopItemWidget.generated.h"

/**
 * 
 */
UCLASS()
class S1_API UShopItemWidget : public UUserWidget, public IUserObjectListEntry
{
	GENERATED_BODY()

protected:
	virtual void NativeConstruct() override;

public:
	virtual void NativeOnListItemObjectSet(UObject* ListItemObject)override;

private:
	UFUNCTION()
	void SetMeshOfParts();

private:
	UPROPERTY(meta = (BindWidget))
	class UButton* ShopItemButton;

	UPROPERTY(meta = (BindWidget))
	class UImage* ItemIconImage;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* ItemNameText;

	UPROPERTY(meta = (BindWidget))
	class UImage* ImageOfMoney;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* TextOfPrice;

private:
	FString MeshPath;
	FName	ItemName;
	const TCHAR* IconPath;
	ESkeletalMeshParts ItemType = ESkeletalMeshParts::NONE;
	EItemType ItemClassification;
	int ItemID;
	int ItemPrice;
	
};
