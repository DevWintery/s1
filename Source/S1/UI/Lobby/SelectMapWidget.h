// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "S1Defines.h"
#include "SelectMapWidget.generated.h"

DECLARE_MULTICAST_DELEGATE(FOnGoInRoomScene);
/**
 * 
 */
UCLASS()
class S1_API USelectMapWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	virtual void NativeConstruct() override;

public:
	FOnGoInRoomScene OnGoInRoomScene;

private:
	UFUNCTION()
	void FocusOn();

	UFUNCTION()
	void FocusOff();

	UFUNCTION()
	void GoInRoomScene();

	UFUNCTION()
	void SelectStageOne();
	UFUNCTION()
	void SelectStageTwo();
	UFUNCTION()
	void SelectStageThree();

	void SetTextOfStage(FString StageStr);

private:
	UPROPERTY(meta = (BindWidget))
	class UButton* SelectMap_0;

	UPROPERTY(meta = (BindWidget))
	class UButton* SelectMap_1;	

	UPROPERTY(meta = (BindWidget))
	class UButton* SelectMap_2;

	UPROPERTY(meta = (BindWidget))
	class UOverlay* StageSummaryOL;

	UPROPERTY(meta = (BindWidget))
	class UButton* SummaryExitButton;

	UPROPERTY(meta = (BindWidget))
	class UButton* StageSelectButton;

	UPROPERTY(meta = (BindWidget))
	class UImage* StageImage;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* SummaryOfStage;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* NumOfStage;

private:
	UPROPERTY(meta = (BindWidgetAnim), Transient)
	class UWidgetAnimation* OpenSummary;

	UPROPERTY(meta = (BindWidgetAnim), Transient)
	class UWidgetAnimation* CloseSummary;

private:
	FString SummaryText;

};
