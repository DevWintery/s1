// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Blueprint/IUserObjectListEntry.h"
#include "S1Defines.h"
#include "InventoryItemWidget.generated.h"

/**
 * 
 */
UCLASS()
class S1_API UInventoryItemWidget : public UUserWidget, public IUserObjectListEntry
{
	GENERATED_BODY()

protected:
	virtual void NativeConstruct() override;

public:
	virtual void NativeOnListItemObjectSet(UObject* ListItemObject)override;

private:
	UFUNCTION()
	void SetMeshOfParts();

	UFUNCTION()
	void SetItemInfoAdd();

	UFUNCTION()
	void SetItemInfoRemove();

private:
	UPROPERTY(meta = (BindWidget))
	class UButton* InventoryItemButton;

	UPROPERTY(meta = (BindWidget))
	class UImage* InventoryIconImage;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* ItemNameText;

private:
	FString MeshPath;
	FString ItemSummary;
	FName	ItemName;
	int ItemID;
	const TCHAR* IconPath;
	int ItemStat;
	ESkeletalMeshParts ItemType = ESkeletalMeshParts::NONE;
	ETypeOfStat TypeOfStat;
};
