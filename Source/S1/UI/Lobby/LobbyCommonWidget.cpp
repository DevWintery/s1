// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Lobby/LobbyCommonWidget.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "LobbyController.h"
#include "Kismet/GameplayStatics.h"

void ULobbyCommonWidget::NativeConstruct()
{
	Super::NativeConstruct();

	if (BackButton)
	{
		BackButton->OnClicked.AddDynamic(this, &ULobbyCommonWidget::GoBackScene);
	}

	ALobbyController* LC = Cast<ALobbyController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
}

void ULobbyCommonWidget::SetTextOfScene(FString NameOfScene)
{
	if (TextOfScene)
	{
		TextOfScene->SetText(FText::FromString(NameOfScene));
	}
}

void ULobbyCommonWidget::SetGoldAndSp(int Gold, int Sp)
{
	FString GoldStr = FString::FromInt(Gold);
	FString Spstr = FString::FromInt(Sp);

	TextOfGold->SetText(FText::FromString(GoldStr));
	TextOfSp->SetText(FText::FromString(Spstr));
}


void ULobbyCommonWidget::GoBackScene()
{
	OnGoBackScene.Broadcast();
}
