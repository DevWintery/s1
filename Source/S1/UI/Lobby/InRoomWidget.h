// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "S1Defines.h"
#include "Protocol.pb.h"
#include "InRoomWidget.generated.h"

DECLARE_MULTICAST_DELEGATE_OneParam(FOnSelectMapDelegate, ELobbyScene);

/**
 * 
 */
UCLASS()
class S1_API UInRoomWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	virtual void NativeConstruct() override;

public:
	void UpdatePlayer(const TArray<FString>& Players);
	void UpdatePlayer(const FString& Player);
	void RefreshUserList();

public:
	FOnSelectMapDelegate OnSelectMap;
	
private:
	UFUNCTION(BlueprintCallable)
	void Start();

	UFUNCTION()
	void SelectMap();

private:
	UPROPERTY(meta = (BindWidget))
	class UVerticalBox* UserList;

	UPROPERTY(meta = (BindWidget))
	class UButton* StartButton;

	UPROPERTY(meta = (BindWidget))
	class UButton* SelectMapButton;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	TSubclassOf<class UUserWidget> UserItemInstance;

	TArray<FString> UserLists;
};
