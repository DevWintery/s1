// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "S1Defines.h"
#include "InventoryItemData.generated.h"

/**
 *
 */
UCLASS()
class S1_API UInventoryItemData : public UObject
{
	GENERATED_BODY()

public:
	UInventoryItemData();

public:
	void SetData(FString i);

public:
	FORCEINLINE ESkeletalMeshParts GetItemType() const { return ItemType; }
	FORCEINLINE	FString GetItemName() const { return ItemName; }
	FORCEINLINE FString GetMeshPath() const { return MeshPath; }
	FORCEINLINE FString GetRowName() const { return RowName; }
	FORCEINLINE int GetItemPrice() const { return ItemPrice; }
	FORCEINLINE FString GetThumbnailPath() const { return ThumbnailPath; }
	FORCEINLINE EItemType GetItemClassification() const { return ItemClassification; }
	FORCEINLINE FString GetItemSummary() const { return ItemSummary; }
	FORCEINLINE int GetItemStat() const { return ItemStat; }
	FORCEINLINE ETypeOfStat GetTypeOfStat() const {return TypeOfStat;}

private:
	class UDataTable* ItemDT;

	ESkeletalMeshParts ItemType = ESkeletalMeshParts::NONE;
	FString ItemName;
	FString MeshPath;
	FString RowName;
	FString ThumbnailPath;
	EItemType ItemClassification;
	int ItemPrice;
	int ItemStat;
	FString ItemSummary;
	ETypeOfStat TypeOfStat;
};
