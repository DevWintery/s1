// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Lobby/ShopWidget.h"
#include "Components/Button.h"
#include "Components/WidgetSwitcher.h"
#include "Components/ListView.h"
#include "Kismet/GameplayStatics.h"

#include "S1GameInstance.h"
#include "S1Defines.h"
#include "DataManager.h"
#include "LobbyController.h"
#include "InventoryComponent.h"
#include "InventoryItemData.h"
#include "ItemData.h"


void UShopWidget::NativeConstruct()
{
	Super::NativeConstruct();

	EtcButton->OnClicked.AddDynamic(this, &UShopWidget::ChangeTapEtc);
	EquipButton->OnClicked.AddDynamic(this, &UShopWidget::ChangeTapEquip);

	UDataManager* DataManager = Cast<US1GameInstance>(GetGameInstance())->GetDataManager();

	TArray<FName> AllItemData = DataManager->GetAllItemData();

	SizeOfShop = AllItemData.Num();


	for (int i = 0; i < SizeOfShop; ++i)
	{
		if (UInventoryItemData* NewItem = NewObject<UInventoryItemData>(this, UInventoryItemData::StaticClass()))
		{			
			ItemList.Emplace(NewItem);
		}
	}
	
	for (int i = 0; i < SizeOfShop; ++i)
	{
		ItemList[i]->SetData(AllItemData[i].ToString());

		switch (ItemList[i]->GetItemClassification())
		{
		case  EItemType::NONE:
			break;
		case  EItemType::EQUIPMENT:
			EquipmentList->AddItem(ItemList[i]);
			break;
		case  EItemType::ETC:
			EtcList->AddItem(ItemList[i]);
			break;
		default:
			break;
		}
	}
}

void UShopWidget::ChangeTapEtc()
{
	ShopSwitcher->SetActiveWidgetIndex(1);
}

void UShopWidget::ChangeTapEquip()
{
	ShopSwitcher->SetActiveWidgetIndex(0);
}

void UShopWidget::RefreshShop()
{

}
