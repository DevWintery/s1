// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ShopWidget.generated.h"

/**
 * 
 */
UCLASS()
class S1_API UShopWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	virtual void NativeConstruct() override;

private:
	UFUNCTION()
	void ChangeTapEtc();

	UFUNCTION()
	void ChangeTapEquip();

	void RefreshShop();

private:
	UPROPERTY(meta = (BindWidget))
	class UButton* EtcButton;

	UPROPERTY(meta = (BindWidget))
	class UButton* EquipButton;

	UPROPERTY(meta = (BindWidget))
	class UWidgetSwitcher* ShopSwitcher;

	UPROPERTY(meta = (BindWidget))
	class UListView* EtcList;

	UPROPERTY(meta = (BindWidget))
	class UListView* EquipmentList;

	UPROPERTY(EditDefaultsOnly, Category = Num, meta = (AllowPrivateAccess = true))
	int SizeOfShop;

private:
	TArray<class UInventoryItemData*> ItemList;
	
};
