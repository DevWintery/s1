// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "S1Defines.h"
#include "LobbyMainWidget.generated.h"

DECLARE_MULTICAST_DELEGATE_OneParam(FOnGoSceneCreateRoomDelegate, ELobbyScene);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnGoSceneInventoryDelegate, ELobbyScene);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnGoSceneShopDelegate, ELobbyScene);

/**
 * 
 */
UCLASS()
class S1_API ULobbyMainWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	virtual void NativeConstruct() override;

public:
	FOnGoSceneCreateRoomDelegate		OnGoSceneCreateRoom;
	FOnGoSceneInventoryDelegate			OnGoSceneInventory;
	FOnGoSceneShopDelegate				OnGoSceneShop;

public:
	UFUNCTION()
	void GoSceneCreateRoom();

	UFUNCTION()
	void GoSceneInventory();

	UFUNCTION()
	void GoSceneShop();

	UFUNCTION()
	void ExitGame();

private:
	UPROPERTY(meta = (BindWidget))
	class UButton* SceneCreateRoomButton;

	UPROPERTY(meta = (BindWidget))
	class UButton* SceneInventoryButton;

	UPROPERTY(meta = (BindWidget))
	class UButton* SceneShopButton;

	UPROPERTY(meta = (BindWidget))
	class UButton* ExitButton;
	
	
};
