// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Lobby/ItemSummaryWidget.h"
#include "Components/TextBlock.h"

void UItemSummaryWidget::NativeConstruct()
{
	Super::NativeConstruct();
}

void UItemSummaryWidget::SetSummaryText(FString SummaryOfText, FName ItemName, FString StatType, int Stat, FVector2D Location)
{
	SetPositionInViewport(Location);
	SummaryOfItem->SetText(FText::FromString(SummaryOfText));
	NameOfItem->SetText(FText::FromName(ItemName));
	TypeOfStat->SetText(FText::FromString(StatType));

	FString StatStr;
	if (0 != Stat)
	{
		StatStr = " : ";
		StatStr += FString::FromInt(Stat);
	}

	TextOfStat->SetText(FText::FromString(StatStr));
}
