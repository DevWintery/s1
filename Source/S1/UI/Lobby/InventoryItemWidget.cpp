// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Lobby/InventoryItemWidget.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "Components/Image.h"
#include "S1Defines.h"
#include "LobbyCharacter.h"
#include "InventoryItemData.h"
#include "Engine/DataTable.h"
#include "LobbyGameModeBase.h"
#include "UIManager.h"

void UInventoryItemWidget::NativeConstruct()
{
	Super::NativeConstruct();

	InventoryItemButton->OnClicked.AddDynamic(this, &UInventoryItemWidget::SetMeshOfParts);
	InventoryItemButton->OnHovered.AddDynamic(this, &UInventoryItemWidget::SetItemInfoAdd);
	InventoryItemButton->OnUnhovered.AddDynamic(this, &UInventoryItemWidget::SetItemInfoRemove);
}

void UInventoryItemWidget::NativeOnListItemObjectSet(UObject* ListItemObject)
{
	IUserObjectListEntry::NativeOnListItemObjectSet(ListItemObject);

	ItemNameText->SetVisibility(ESlateVisibility::HitTestInvisible);
	InventoryIconImage->SetVisibility(ESlateVisibility::HitTestInvisible);

	if (UInventoryItemData* ItemDT = Cast<UInventoryItemData>(ListItemObject))
	{
		MeshPath = ItemDT->GetMeshPath();
		ItemName = FName(*(ItemDT->GetItemName()));
		ItemType = ItemDT->GetItemType();
		ItemID = FCString::Atoi(*(ItemDT->GetRowName()));
		FString TestIconPath = ItemDT->GetThumbnailPath();
		TypeOfStat = ItemDT->GetTypeOfStat();

		ItemSummary = ItemDT->GetItemSummary();
		ItemStat = ItemDT->GetItemStat();
		ItemNameText->SetText(FText::FromName(ItemName));
		UTexture2D* Thumbnail = LoadObject<UTexture2D>(NULL, *TestIconPath, NULL, LOAD_None, NULL);
		InventoryIconImage->SetBrushFromTexture(Thumbnail);
	}
}

void UInventoryItemWidget::SetMeshOfParts()
{
	ALobbyGameModeBase* LGM = Cast<ALobbyGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	if (nullptr == LGM)
	{
		return;
	}

	if (ALobbyCharacter* LobbyCharacter = Cast<ALobbyCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)))
	{
		if (USkeletalMesh* PartsOfMesh = LoadObject<USkeletalMesh>(nullptr, *MeshPath))
		{
			LobbyCharacter->SetPartOfMesh(ItemType, PartsOfMesh, ItemID);

			LGM->OnError.Broadcast(7);
		}
	}
}

void UInventoryItemWidget::SetItemInfoAdd()
{
	APlayerController* Controller =  UGameplayStatics::GetPlayerController(GetWorld(), 0);
	FVector2D MousePos;

	Controller->GetMousePosition(MousePos.X, MousePos.Y);
	
	GET_UIMANAGER(UIManager);

	if (nullptr == UIManager->GetUI(EUIType::ITEMSUMMARY))
	{
		UIManager->CreateUIWidget(EUIType::ITEMSUMMARY);
	}
	else
	{
		UIManager->ShowUI(EUIType::ITEMSUMMARY);
	}

	
	UIManager->SetItemSummary(ItemSummary, ItemName, TypeOfStat, ItemStat, MousePos);
}

void UInventoryItemWidget::SetItemInfoRemove()
{
	GET_UIMANAGER(UIManager);

	UIManager->GetUI(EUIType::ITEMSUMMARY)->RemoveFromParent();
}
