// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Lobby/CreateRoomWidget.h"
#include "Components/Button.h"
#include "Components/EditableTextBox.h"
#include "Protocol.pb.h"
#include "S1Defines.h"
#include "S1GameInstance.h"
#include "HttpSubSystem.h"
#include "Containers/StringFwd.h"
#include "LobbyGameModeBase.h"
#include "LobbyCharacter.h"
#include "Kismet/GameplayStatics.h"

void UCreateRoomWidget::Confirm()
{
	US1GameInstance* GI = Cast<US1GameInstance>(GetGameInstance());
	if (GI == nullptr)
	{
		return;
	}

	if (UHTTPSubsystem* Http = GetGameInstance()->GetSubsystem<UHTTPSubsystem>())
	{
		FString IP = Input_IP->GetText().ToString();
		FString Url = "http://";
		Url += IP;
		Url += ":34568"; //port
		Url += "/rooms";

		TArray< FStringFormatArg > Args;
		Args.Add(FStringFormatArg("\"" + Input_RoomName->GetText().ToString() + "\""));
		Args.Add(FStringFormatArg("\"" + GI->GetPlayerName() + "\""));
		Args.Add(FStringFormatArg("\"" + Input_IP->GetText().ToString() + "\""));

		FString JsonContent = FString::Format(TEXT("{"
														"\"name\" : {0},"
														"\"player_name\" : {1},"
														"\"room_ip\" : {2}"
													"}"), Args);

		Http->SendHttpPostRequest(Url, JsonContent, [this, GI, IP](bool bWasSuccessful, const FString& ResponseContent, int32 StatusCode)
		{
			if (bWasSuccessful == false)
			{
				return;
			}

			UWorld* World = GetWorld();
			if (World == nullptr)
			{
				return;
			}

			if (ALobbyGameModeBase* LobbyGameMode = Cast<ALobbyGameModeBase>(UGameplayStatics::GetGameMode(World)))
			{
				FString Left, Right;
				if (ResponseContent.Split(TEXT(":"), &Left, &Right))
				{
					int RoomID = FCString::Atoi(*Right);
					LobbyGameMode->SetRoomID(RoomID);
				}

				GI->SetIP(IP);
				GI->ConnectToGameServer();

				//if (ALobbyGameModeBase* GM = Cast<ALobbyGameModeBase>(UGameplayStatics::GetGameMode(GetWorld())))
				//{
				//	TArray<FString> Players;
				//	Players.Add(GI->GetPlayerName());
				//	GM->RefreshUserList(Players);
				//}

				OnConfirm.Broadcast();
			}
		});
	}
}

void UCreateRoomWidget::Cancel()
{
	OnCancel.Broadcast();
}

void UCreateRoomWidget::NativeConstruct()
{
	Super::NativeConstruct();

	if (ConfirmButton)
	{
		ConfirmButton->OnClicked.AddDynamic(this, &UCreateRoomWidget::Confirm);
	}

	if (CancelButton)
	{
		CancelButton->OnClicked.AddDynamic(this, &UCreateRoomWidget::Cancel);
	}
}