// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Lobby/UserItemWidget.h"
#include "Components/TextBlock.h"

void UUserItemWidget::SetUp(const FString& Name)
{
	UserName->SetText(FText::FromString(Name));
}
