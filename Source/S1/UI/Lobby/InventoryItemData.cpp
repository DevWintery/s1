// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Lobby/InventoryItemData.h"
#include "ItemData.h"

UInventoryItemData::UInventoryItemData()
{
	static ConstructorHelpers::FObjectFinder<UDataTable> ITEMDATA(TEXT("/Game/S1/DataTables/DT_ItemData.DT_ItemData"));

	if (ITEMDATA.Succeeded())
	{
		ItemDT = ITEMDATA.Object;
	}
}

void UInventoryItemData::SetData(FString i)
{
	FItemData* ItemData = ItemDT->FindRow<FItemData>(FName(*i), FString(""));

	if (ItemData)
	{
		ItemType = ItemData->ItemType;
		ItemName = ItemData->ItemName;
		MeshPath = ItemData->MeshPath;
		RowName = i;
		ItemPrice = ItemData->ItemPrice;
		ThumbnailPath = ItemData->ThumbnailPath;
		ItemClassification = ItemData->ItemClassification;
		ItemStat = ItemData->Stat;
		ItemSummary = ItemData->ItemSummary;
		TypeOfStat = ItemData->TypeOfStat;
	}
}
