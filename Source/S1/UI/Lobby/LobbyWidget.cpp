// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Lobby/LobbyWidget.h"
#include "Components/Button.h"
#include "Components/VerticalBox.h"
#include "RoomItemWidget.h"
#include "S1Defines.h"
#include "HttpSubsystem.h"

void ULobbyWidget::NativeConstruct()
{
	Super::NativeConstruct();

	if (CreateRoomButton)
	{
		CreateRoomButton->OnClicked.AddDynamic(this, &ULobbyWidget::CreateRoom);
	}

	if (RefreshButton)
	{
		RefreshButton->OnClicked.AddDynamic(this, &ULobbyWidget::RequestRoomList);
	}
}

void ULobbyWidget::CreateRoom()
{
	//OnCreateRoom.Broadcast(ELobbyScene::INROOM);

	OnCreateRoom.Broadcast();
}

void ULobbyWidget::RequestRoomList()
{
	US1GameInstance* GI = Cast<US1GameInstance>(GetGameInstance());

	if (UHTTPSubsystem* Http = GetGameInstance()->GetSubsystem<UHTTPSubsystem>())
	{
		FString IP = GI->GetIP();
		FString Url = "http://";
		Url += IP;
		Url += ":34568"; //port
		Url += "/rooms";

		Http->SendHttpGetRequest(Url, [this](bool bWasSuccessful, const FString& ResponseContent, int32 StatusCode)
			{
				if (ResponseContent.Len() > 3)
				{
					RefreshRoomList(ResponseContent);
				}
			});
	}
}

void ULobbyWidget::RefreshRoomList(const FString& JsonContent)
{
	RoomList->ClearChildren();

	FString Left, Right;
	FString Args[] = { "id\":", "name\":", "players\":", "room_ip\":" };

	int RoomID = 0;
	FString RoomName = "";
	TArray<FString> Players;
	FString RoomIP = "";

	//Room Id
	if (JsonContent.Split(Args[0], &Left, &Right))
	{
		FString IDStr = "";
		IDStr += Right[0];//첫번째가 ID

		RoomID = FCString::Atoi(*IDStr);
	}

	//Room Name
	if (JsonContent.Split(Args[1], &Left, &Right))
	{
		bool DoWrite = false;
		for (char Ch : Right)
		{
			if (Ch == '\"')
			{
				if (DoWrite) //기록 다했으면 끝
				{
					break;
				}
				else
				{
					DoWrite = true;
					continue;
				}
			}
			RoomName += Ch;
		}
	}

	//Players
	if (JsonContent.Split(Args[2], &Left, &Right))
	{
		FString Player;
		bool DoWrite = false;
		for (char Ch : Right)
		{
			if (Ch == '[') // 배열 시작점
				continue;

			if (Ch == ']') //배열 끝
				break;

			if (Ch == '\"') //플레이어 이름의 시작이거나 끝
			{
				if (DoWrite) //기록된거 있으면
				{
					Players.Add(Player);
					DoWrite = false;
				}
				else //이제 시작하면
				{
					Player = ""; //기록할 준비
					DoWrite = true;
				}
				continue;
			}

			if (DoWrite)
			{
				Player += Ch;
			}
		}
	}

	//Room IP
	if (JsonContent.Split(Args[3], &Left, &Right))
	{
		bool DoWrite = false;
		for (char Ch : Right)
		{
			if (Ch == '\"')
			{
				if (DoWrite)
					break;

				DoWrite = true;
				continue;
			}
			RoomIP += Ch;
		}
	}

	URoomItemWidget* NewItem = CreateWidget<URoomItemWidget>(this, RoomItemInstance);
	NewItem->Setup(RoomID, RoomName, Players, RoomIP); // Setup 함수는 URoomItemWidget에서 방 정보를 설정
	RoomList->AddChild(NewItem);
}