// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "CreateRoomWidget.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnCreateRoomConfirmDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnCreateRoomCancelDelegate);

/**
 * 
 */
UCLASS()
class S1_API UCreateRoomWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable, Category = "Lobby")
	void Confirm();

	UFUNCTION(BlueprintCallable, Category = "Lobby")
	void Cancel();

public:
	UPROPERTY(BlueprintAssignable)
	FOnCreateRoomConfirmDelegate OnConfirm;

	UPROPERTY(BlueprintAssignable)
	FOnCreateRoomCancelDelegate OnCancel;

protected:
	virtual void NativeConstruct() override;

private:
	UPROPERTY(meta = (BindWidget))
	class UEditableTextBox* Input_RoomName;

	UPROPERTY(meta = (BindWidget))
	class UEditableTextBox* Input_IP;

	UPROPERTY(meta = (BindWidget))
	class UButton* ConfirmButton;

	UPROPERTY(meta = (BindWidget))
	class UButton* CancelButton;
};
