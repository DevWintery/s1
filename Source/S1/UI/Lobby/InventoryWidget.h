// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "InventoryWidget.generated.h"


/**
 * 
 */
UCLASS()
class S1_API UInventoryWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	virtual void NativeConstruct() override;

private:
	UFUNCTION()
	void ChangeTapEtc();

	UFUNCTION()
	void ChangeTapEquip();

	void RefreshInventory();

	void CreateItemList();

private:
	UPROPERTY(meta = (BindWidget))
	class UButton* EtcButton;

	UPROPERTY(meta = (BindWidget))
	class UButton* EquipButton;

	UPROPERTY(meta = (BindWidget))
	class UWidgetSwitcher* InventorySwitcher;

	UPROPERTY(meta = (BindWidget))
	class UListView* EtcList;

	UPROPERTY(meta = (BindWidget))
	class UListView* EquipmentList;

	UPROPERTY(EditDefaultsOnly, Category = Num, meta = (AllowPrivateAccess = true))
	int SizeOfInventory; // InventoryComponent에서 받아오는 것으로 변경하기


	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	TSubclassOf<class UUserWidget> InventoryItemInstance;

private:
	TArray<class UInventoryItemData*> ItemList;

	
};
