// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Protocol.pb.h"
#include "UserItemWidget.generated.h"

/**
 * 
 */
UCLASS()
class S1_API UUserItemWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	void SetUp(const FString& Name);

private:
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* UserName;
};
