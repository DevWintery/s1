// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Lobby/InventoryWidget.h"
#include "Components/Button.h"
#include "Components/WidgetSwitcher.h"
#include "Components/ListView.h"
#include "InventoryItemWidget.h"
#include "Kismet/GameplayStatics.h"
#include "LobbyController.h"
#include "InventoryComponent.h"
#include "InventoryItemData.h"

void UInventoryWidget::NativeConstruct()
{
	Super::NativeConstruct();

	EtcButton->OnClicked.AddDynamic(this, &UInventoryWidget::ChangeTapEtc);
	EquipButton->OnClicked.AddDynamic(this, &UInventoryWidget::ChangeTapEquip);

	if (ALobbyController* LC = Cast<ALobbyController>(UGameplayStatics::GetPlayerController(GetWorld(), 0)))
	{
		if (UInventoryComponent* Inventory = LC->GetInventoryComponent())
		{
			SizeOfInventory = Inventory->GetCurrentSize();
			Inventory->OnRefreshInventory.AddUObject(this, &UInventoryWidget::RefreshInventory);
		}
	}

	SizeOfInventory = 0;
	RefreshInventory();
}

void UInventoryWidget::ChangeTapEtc()
{
	InventorySwitcher->SetActiveWidgetIndex(1);
}

void UInventoryWidget::ChangeTapEquip()
{
	InventorySwitcher->SetActiveWidgetIndex(0);

}

void UInventoryWidget::RefreshInventory()
{
	if (ALobbyController* LC = Cast<ALobbyController>(UGameplayStatics::GetPlayerController(GetWorld(), 0)))
	{
		if (UInventoryComponent* Inventory = LC->GetInventoryComponent())
		{
			int CurrentSize = Inventory->GetCurrentSize();

			for (int i = SizeOfInventory; i < CurrentSize; ++i)
			{
				CreateItemList();
				ItemList[i]->SetData(Inventory->GetInventoryAtIndex(i));

				switch (ItemList[i]->GetItemClassification())
				{
				case  EItemType::NONE:
					break;
				case  EItemType::EQUIPMENT:
					EquipmentList->AddItem(ItemList[i]);
					break;
				case  EItemType::ETC:
					EtcList->AddItem(ItemList[i]);
					break;
				default:
					break;
				}
			}

			SizeOfInventory = CurrentSize;
		}
	}
}

void UInventoryWidget::CreateItemList()
{

	if (UInventoryItemData* NewItem = NewObject<UInventoryItemData>(this, UInventoryItemData::StaticClass()))
	{

		ItemList.Add(NewItem);
	}
}


