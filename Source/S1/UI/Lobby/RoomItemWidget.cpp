// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Lobby/RoomItemWidget.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "S1Defines.h"
#include "Kismet/GameplayStatics.h"
#include "LobbyGameModeBase.h"
#include "LobbyCharacter.h"
#include "Serialization/AsyncPackageLoader.h"

void URoomItemWidget::NativeConstruct()
{
	Super::NativeConstruct();

	if (RoomButton)
	{
		RoomButton->OnClicked.AddDynamic(this, &URoomItemWidget::EnterRoom);
	}
}

void URoomItemWidget::EnterRoom()
{
	if (US1GameInstance* GameInstance = Cast<US1GameInstance>(GetGameInstance()))
	{
		GameInstance->SetIP(RoomIP);
		GameInstance->ConnectToGameServer();

		if (ALobbyGameModeBase* GM = Cast<ALobbyGameModeBase>(UGameplayStatics::GetGameMode(GetWorld())))
		{
			//내 이름도 넣자
			Players.Add(GameInstance->GetPlayerName());
			GM->RefreshUserList(Players);
			GM->SetRefreshScene(ELobbyScene::INROOM);
		}
	}
}

void URoomItemWidget::Setup(int _RoomID, const FString& _RoomName, const TArray<FString>& _Players, const FString _RoomIP)
{
	RoomID = _RoomID;
	RoomName->SetText(FText::FromString(_RoomName));
	for (const auto& Player : _Players)
	{
		Players.Add(Player);
	}
	RoomIP = _RoomIP;
}
