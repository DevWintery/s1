// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Protocol.pb.h"
#include "S1Defines.h"
#include "LobbyWidget.generated.h"

//DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnCreateRoomDelegate);

//DECLARE_MULTICAST_DELEGATE_OneParam(FOnCreateRoomDelegate, ELobbyScene);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnCreateRoomDelegate);


/**
 * 
 */
UCLASS()
class S1_API ULobbyWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category = "Lobby")
	void CreateRoom();

	UFUNCTION(BlueprintCallable, Category = "Lobby")
	void RequestRoomList();

	void RefreshRoomList(const FString& JsonContent);

public:
	UPROPERTY(BlueprintAssignable)
	FOnCreateRoomDelegate OnCreateRoom;

protected:
	virtual void NativeConstruct() override;

private:
	UPROPERTY(meta = (BindWidget))
	class UButton* CreateRoomButton;

	UPROPERTY(meta = (BindWidget))
	class UButton* RefreshButton;

	UPROPERTY(meta = (BindWidget))
	class UVerticalBox* RoomList;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	TSubclassOf<class UUserWidget> RoomItemInstance;

private:
	TArray<Protocol::RoomInfo> RoomInfos; // �� ���
};
