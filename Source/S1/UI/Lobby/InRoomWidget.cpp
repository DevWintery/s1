// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Lobby/InRoomWidget.h"
#include "Components/Button.h"
#include "Components/VerticalBox.h"
#include "UserItemWidget.h"
#include "S1Defines.h"
#include "LobbyCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "HttpSubsystem.h"
#include"LobbyGameModeBase.h"

void UInRoomWidget::NativeConstruct()
{
	Super::NativeConstruct();

	if (StartButton)
	{
		StartButton->OnClicked.AddDynamic(this, &UInRoomWidget::Start);
	}

	if (SelectMapButton)
	{
		SelectMapButton->OnClicked.AddDynamic(this, &UInRoomWidget::SelectMap);
	}
}

void UInRoomWidget::UpdatePlayer(const TArray<FString>& Players)
{
	UserLists.Empty();

	for (const auto& Player : Players)
	{
		UserLists.Add(Player);
	}

	RefreshUserList();
}

void UInRoomWidget::UpdatePlayer(const FString& Player)
{
	UserLists.Add(Player);

	RefreshUserList();
}

void UInRoomWidget::RefreshUserList()
{
	UserList->ClearChildren();

	for (const auto& User : UserLists)
	{
		UUserItemWidget* NewItem = CreateWidget<UUserItemWidget>(this, UserItemInstance);
		NewItem->SetUp(User);
		UserList->AddChild(NewItem);
	}
}

void UInRoomWidget::Start()
{
	int RoomID = 0;
	const char* CurrentRoomName = nullptr;

	ALobbyGameModeBase* LobbyGameMode = Cast<ALobbyGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));

	if (!LobbyGameMode)
	{
		return;
	}

	RoomID = LobbyGameMode->GetRoomID();
	CurrentRoomName = LobbyGameMode->GetCurrnetStage();

	US1GameInstance* GI = Cast<US1GameInstance>(GetGameInstance());
	if (GI == nullptr)
	{
		return;
	}

	if (nullptr == CurrentRoomName)
	{
		LobbyGameMode->OnError.Broadcast(2);
		return;
	}
	
	else if(LobbyGameMode->GetNumOfSelectStage() > (GI->GetNumOfClearStage() + 1))
	{
		LobbyGameMode->OnError.Broadcast(3);
		return;
	}

	else if (LobbyGameMode->GetNumOfSelectStage() < (GI->GetNumOfClearStage() + 1))
	{
		LobbyGameMode->OnError.Broadcast(4);
		return;
	}

	//디버깅용 코드 
#if !UE_BUILD_SHIPPING
	//CurrentRoomName = "Demonstration";
	//CurrentRoomName = "Yumin";
#endif

	Protocol::C_ENTER_GAME EnterGamePkt;
	EnterGamePkt.set_room_id(RoomID);
	//TODO: 이거 맵 고른걸로 변경
	EnterGamePkt.set_map_name(CurrentRoomName);

	SEND_PACKET(EnterGamePkt);

	if (UHTTPSubsystem* Http = GetGameInstance()->GetSubsystem<UHTTPSubsystem>())
	{
		FString IP = GI->GetIP();
		FString Url = "http://";
		Url += IP;
		Url += ":34568"; //port
		Url += "/start_room";

		FString JsonContent = TEXT("{ \"id\": ");
		JsonContent += FString::FromInt(RoomID);
		JsonContent += "}";

		Http->SendHttpPostRequest(Url, JsonContent, [this, GI, IP](bool bWasSuccessful, const FString& ResponseContent, int32 StatusCode)
		{
		});
	}
}

void UInRoomWidget::SelectMap()
{
	OnSelectMap.Broadcast(ELobbyScene::SELECTSTAGE);
}

