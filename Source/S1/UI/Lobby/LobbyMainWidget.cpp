// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Lobby/LobbyMainWidget.h"
#include "Components/Button.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"

void ULobbyMainWidget::NativeConstruct()
{
	Super::NativeConstruct();

	if (SceneCreateRoomButton)
	{
		SceneCreateRoomButton->OnClicked.AddDynamic(this, &ULobbyMainWidget::GoSceneCreateRoom);
	}

	if (SceneInventoryButton)
	{
		SceneInventoryButton->OnClicked.AddDynamic(this, &ULobbyMainWidget::GoSceneInventory);
	}

	if (SceneShopButton)
	{
		SceneShopButton->OnClicked.AddDynamic(this, &ULobbyMainWidget::GoSceneShop);
	}

	if (ExitButton)
	{
		ExitButton->OnClicked.AddDynamic(this, &ULobbyMainWidget::ExitGame);
	}
}

void ULobbyMainWidget::GoSceneCreateRoom()
{
	OnGoSceneCreateRoom.Broadcast(ELobbyScene::CREATEROOM);
}

void ULobbyMainWidget::GoSceneInventory()
{
	OnGoSceneInventory.Broadcast(ELobbyScene::INVENTORY);
}

void ULobbyMainWidget::GoSceneShop()
{
	OnGoSceneShop.Broadcast(ELobbyScene::SHOP);
}

void ULobbyMainWidget::ExitGame()
{
	APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);

	UKismetSystemLibrary::QuitGame(GetWorld(), PlayerController, EQuitPreference::Quit, true);
}
