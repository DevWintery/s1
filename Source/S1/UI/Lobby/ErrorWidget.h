// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ErrorWidget.generated.h"

/**
 * 
 */
UCLASS()
class S1_API UErrorWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	void SetErrorMessage(FString ErrorMsg);

private:
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* TextOfError;

private:
	UPROPERTY(meta = (BindWidgetAnim), Transient)
	class UWidgetAnimation* Error;
};
