// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Lobby/SelectMapWidget.h"
#include "Components/Button.h"
#include "Components/Overlay.h"
#include "Kismet/GameplayStatics.h"
#include "LobbyGameModeBase.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "DataManager.h"

void USelectMapWidget::NativeConstruct()
{
	Super::NativeConstruct();

	SelectMap_0->OnClicked.AddDynamic(this, &USelectMapWidget::FocusOn);
	SelectMap_1->OnClicked.AddDynamic(this, &USelectMapWidget::FocusOn);
	SelectMap_2->OnClicked.AddDynamic(this, &USelectMapWidget::FocusOn);

	SelectMap_0->OnClicked.AddDynamic(this, &USelectMapWidget::SelectStageOne);
	SelectMap_1->OnClicked.AddDynamic(this, &USelectMapWidget::SelectStageTwo);
	SelectMap_2->OnClicked.AddDynamic(this, &USelectMapWidget::SelectStageThree);

	SummaryExitButton->OnClicked.AddDynamic(this, &USelectMapWidget::FocusOff);
	StageSelectButton->OnClicked.AddDynamic(this, &USelectMapWidget::FocusOff);
	StageSelectButton->OnClicked.AddDynamic(this, &USelectMapWidget::GoInRoomScene);
}

void USelectMapWidget::FocusOn()
{
	StageSummaryOL->SetVisibility(ESlateVisibility::Visible);
	//PlayAnimation(OpenSummary);
	//그냥 이미지들 크기를 키워서 이동시키는 게 나을 듯
}

void USelectMapWidget::FocusOff()
{
	StageSummaryOL->SetVisibility(ESlateVisibility::Collapsed);
	//PlayAnimation(CloseSummary);
}

void USelectMapWidget::GoInRoomScene()
{
	OnGoInRoomScene.Broadcast();
}

void USelectMapWidget::SelectStageOne()
{
	ALobbyGameModeBase* GM = Cast<ALobbyGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	GM->SetCurrentStage("Hangar");
	GM->SetNumOfSelectStage(1);

	UTexture2D* Thumbnail = LoadObject<UTexture2D>(NULL, TEXT("/Game/S1/UI/Lobby/Texture/T_Stage1.T_Stage1"), NULL, LOAD_None, NULL);
	StageImage->SetBrushFromTexture(Thumbnail);

	SetTextOfStage(TEXT("Stage1"));
}

void USelectMapWidget::SelectStageTwo()
{
	ALobbyGameModeBase* GM = Cast<ALobbyGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	GM->SetCurrentStage("Demonstration");
	GM->SetNumOfSelectStage(2);

	UTexture2D* Thumbnail = LoadObject<UTexture2D>(NULL, TEXT("/Game/S1/UI/Lobby/Texture/T_Stage2.T_Stage2"), NULL, LOAD_None, NULL);
	StageImage->SetBrushFromTexture(Thumbnail);

	SetTextOfStage(TEXT("Stage2"));
}

void USelectMapWidget::SelectStageThree()
{
	ALobbyGameModeBase* GM = Cast<ALobbyGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	GM->SetCurrentStage("Yumin");
	GM->SetNumOfSelectStage(3);

	UTexture2D* Thumbnail = LoadObject<UTexture2D>(NULL, TEXT("/Game/S1/UI/Lobby/Texture/T_Stage3.T_Stage3"), NULL, LOAD_None, NULL);
	StageImage->SetBrushFromTexture(Thumbnail);

	SetTextOfStage(TEXT("Stage3"));
}

void USelectMapWidget::SetTextOfStage(FString StageStr)
{
	US1GameInstance* GI = Cast<US1GameInstance>(GetGameInstance());
	UDataManager* DM = GI->GetDataManager();

	SummaryText = StageStr;
	NumOfStage->SetText(FText::FromString(SummaryText));
	SummaryOfStage->SetText(FText::FromString(DM->GetSummaryOfStage(SummaryText)));
}
