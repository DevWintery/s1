// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Lobby/ErrorWidget.h"
#include "Components/TextBlock.h"

void UErrorWidget::SetErrorMessage(FString ErrorMsg)
{
	PlayAnimation(Error);
	TextOfError->SetText(FText::FromString(ErrorMsg));
}
