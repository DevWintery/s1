// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ItemSummaryWidget.generated.h"

/**
 *
 */
UCLASS()
class S1_API UItemSummaryWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	virtual void NativeConstruct() override;

public:
	void SetSummaryText(FString SummaryOfText, FName ItemName, FString StatType, int Stat, FVector2D Location);

private:
	UPROPERTY(meta = (BindWidget))
	class UImage* ImageBG;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* SummaryOfItem;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* NameOfItem;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* TypeOfStat;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* TextOfStat;

};
