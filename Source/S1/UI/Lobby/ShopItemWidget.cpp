// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Lobby/ShopItemWidget.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "Components/Image.h"
#include "Engine/DataTable.h"

#include "S1Defines.h"
#include "LobbyCharacter.h"
#include "InventoryItemData.h"
#include "LobbyController.h"
#include "InventoryComponent.h"
#include "S1GameInstance.h"
#include "DataManager.h"
#include "LobbyGameModeBase.h"


void UShopItemWidget::NativeConstruct()
{
	Super::NativeConstruct();

	ShopItemButton->OnClicked.AddDynamic(this, &UShopItemWidget::SetMeshOfParts);
}

void UShopItemWidget::NativeOnListItemObjectSet(UObject* ListItemObject)
{
	IUserObjectListEntry::NativeOnListItemObjectSet(ListItemObject);

	ItemNameText->SetVisibility(ESlateVisibility::HitTestInvisible);
	ItemIconImage->SetVisibility(ESlateVisibility::HitTestInvisible);

	if (UInventoryItemData* ItemDT = Cast<UInventoryItemData>(ListItemObject))
	{
		MeshPath = ItemDT->GetMeshPath();
		ItemName = FName(*(ItemDT->GetItemName()));
		ItemType = ItemDT->GetItemType();
		ItemID = FCString::Atoi(*(ItemDT->GetRowName()));
		ItemPrice = ItemDT->GetItemPrice();
		FString TestIconPath = ItemDT->GetThumbnailPath();
		ItemClassification = ItemDT->GetItemClassification();

		ItemNameText->SetText(FText::FromName(ItemName));
		TextOfPrice->SetText(FText::FromString(FString::FromInt(ItemPrice)));

		UTexture2D* Thumbnail = LoadObject<UTexture2D>(NULL, *TestIconPath, NULL, LOAD_None, NULL);
		ItemIconImage->SetBrushFromTexture(Thumbnail);

		FString ImagePath;
		switch (ItemClassification)
		{
		case EItemType::NONE:
			break;

		case EItemType::EQUIPMENT:
		{
			ImagePath = TEXT("/Game/S1/UI/Lobby/Texture/T_Icon_Gold.T_Icon_Gold");
		}
			break;

		case EItemType::ETC:
		{
			ImagePath = TEXT("/Game/S1/UI/Lobby/Texture/T_Icon_SP.T_Icon_SP");
		}
			break;
		default:
			break;
		}

		UTexture2D* Image = LoadObject<UTexture2D>(NULL, *ImagePath, NULL, LOAD_None, NULL);
		ImageOfMoney->SetBrushFromTexture(Image);
		if (ESkeletalMeshParts::NONE == ItemType)
		{
			SetVisibility(ESlateVisibility::Collapsed);
		}
	}
}

void UShopItemWidget::SetMeshOfParts()
{
	US1GameInstance* GI = Cast<US1GameInstance>(GetGameInstance());

	ALobbyGameModeBase* LGM = Cast<ALobbyGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));

	if (UDataManager* DM = GI->GetDataManager())
	{
		if (ALobbyController* LC = Cast<ALobbyController>(UGameplayStatics::GetPlayerController(GetWorld(), 0)))
		{
			if (UInventoryComponent* Inventory = LC->GetInventoryComponent())
			{
				if (EItemType::EQUIPMENT == ItemClassification && ItemPrice > DM->GetCurrentGold())
				{
					LGM->OnError.Broadcast(0);
					return;
				}

				else if (EItemType::ETC == ItemClassification && ItemPrice > DM->GetCurrentSp())
				{
					LGM->OnError.Broadcast(8);
					return;
				}

				else if (Inventory->AddInvenItemData(FString::FromInt(ItemID)))
				{
					switch (ItemClassification)
					{
					case EItemType::NONE:
						break;
					case EItemType::EQUIPMENT:
					{
						DM->SetCurrentGold(DM->GetCurrentGold() - ItemPrice);
					}
					break;
					case EItemType::ETC:
					{
						DM->SetCurrentSp(DM->GetCurrentSp() - ItemPrice);
					}
					break;
					default:
						break;
					}

					LC->OnBuyItem.Broadcast();
					LGM->OnError.Broadcast(6);
				}

				else
				{
					LGM->OnError.Broadcast(1);
				}
			}
		}
	}
}
