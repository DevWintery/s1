// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "LobbyCommonWidget.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnGoBackScene);

/**
 * 
 */
UCLASS()
class S1_API ULobbyCommonWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION()
	void GoBackScene();

public:
	UPROPERTY(BlueprintAssignable)
	FOnGoBackScene OnGoBackScene;

protected:
	virtual void NativeConstruct() override;

public:
	void SetTextOfScene(FString NameOfScene);
	void SetGoldAndSp(int Gold, int Sp);

private:
	UPROPERTY(meta = (BindWidget))
	class UButton* BackButton;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* TextOfScene;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* TextOfGold;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* TextOfSp;

private:
	UPROPERTY(meta = (BindWidgetAnim), Transient)
	class UWidgetAnimation* MessageOfError;
};
