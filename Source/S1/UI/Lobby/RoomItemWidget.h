// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Protocol.pb.h"
#include "RoomItemWidget.generated.h"

/**
 * 
 */
UCLASS()
class S1_API URoomItemWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	virtual void NativeConstruct() override;

public:
	UFUNCTION(BlueprintCallable)
	void EnterRoom();

public:
	void Setup(int _RoomID, const FString& _RoomName, const TArray<FString>& _Players, const FString _RoomIP);

private:
	UPROPERTY(meta = (BindWidget))
	class UButton* RoomButton;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* RoomName;

	int64 RoomID;
	TArray<FString> Players;
	FString RoomIP;
};
