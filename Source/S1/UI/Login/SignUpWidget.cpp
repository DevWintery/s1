// Fill out your copyright notice in the Description page of Project Settings.


#include "SignUpWidget.h"
#include "Components/Button.h"
#include "Protocol.pb.h"
#include "S1GameInstance.h"
#include "ClientPacketHandler.h"


void USignUpWidget::NativeConstruct()
{
	Button_Exit->OnClicked.AddDynamic(this, &USignUpWidget::OnExitButtonClicked);

	Button_Create->OnClicked.AddDynamic(this, &USignUpWidget::OnCreateButtonClicked);

	SetVisibility(ESlateVisibility::Collapsed);
}

void USignUpWidget::OnExitButtonClicked()
{
	SetVisibility(ESlateVisibility::Collapsed);
}

void USignUpWidget::OnCreateButtonClicked()
{

}
