// Fill out your copyright notice in the Description page of Project Settings.


#include "LoginScreenWidget.h"
#include "Kismet/GameplayStatics.h"
#include "Components/Button.h"
#include "SignUpWidget.h"
#include "Protocol.pb.h"
#include "S1GameInstance.h"
#include "ClientPacketHandler.h"
#include "Components/EditableTextBox.h"

void ULoginScreenWidget::NativeConstruct()
{
	Button_Login->OnClicked.AddDynamic(this, &ULoginScreenWidget::OnClickLoginButton);

	PlayAnimation(Title);
}

void ULoginScreenWidget::OnClickLoginButton()
{
	UWorld* World = GetWorld();
	if (World == nullptr)
	{
		return;
	}

	if (auto* GameInstance = Cast<US1GameInstance>(World->GetGameInstance()))
	{
		GameInstance->SetPlayerName(Input_ID->GetText().ToString());
		GameInstance->OpenLevel("Lobby");
		GameInstance->SetIP(Input_Password->GetText().ToString());
	}
}
