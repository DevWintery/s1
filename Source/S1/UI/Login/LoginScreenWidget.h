// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "LoginScreenWidget.generated.h"

/**
 * 
 */
UCLASS()
class S1_API ULoginScreenWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	virtual void NativeConstruct() override;

private:
	UFUNCTION()
	void OnClickLoginButton();

private:
	UPROPERTY(meta = (BindWidget))
	class UEditableTextBox* Input_ID;

	UPROPERTY(meta = (BindWidget))
	class UEditableTextBox* Input_Password;

	UPROPERTY(meta = (BindWidget))
	class UButton* Button_Login;

private:
	UPROPERTY(meta = (BindWidgetAnim), Transient)
	class UWidgetAnimation* Title;
};
