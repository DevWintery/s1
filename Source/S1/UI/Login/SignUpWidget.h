// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SignUpWidget.generated.h"

/**
 * 
 */
UCLASS()
class S1_API USignUpWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	virtual void NativeConstruct() override;

private:
	UFUNCTION()
	void OnExitButtonClicked();

	UFUNCTION()
	void OnCreateButtonClicked();

private:
	UPROPERTY(meta = (BindWidget))
	class UButton* Button_Exit;

	UPROPERTY(meta = (BindWidget))
	class UButton* Button_Create;
	
};
