// Fill out your copyright notice in the Description page of Project Settings.


#include "TraceEffect.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"

// Sets default values
ATraceEffect::ATraceEffect()
 : EffectSpeed(300.f), StartLocation(FVector::ZeroVector), FinishLocation(FVector::ZeroVector)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	SetRootComponent(Root);

	TraceEffectSystem = LoadObject<UNiagaraSystem>(nullptr, TEXT("/Game/S1/Effects/N_Projectile_System.N_Projectile_System"));

	TraceEffect = CreateDefaultSubobject<UNiagaraComponent>(TEXT("TraceEffect"));
	TraceEffect->SetupAttachment(Root);
}

// Called every frame
void ATraceEffect::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (nullptr != TraceEffect)
	{
		StartLocation += (DirEffect * EffectSpeed);

		TraceEffect->SetVariableVec3(FName("BeamStart"), StartLocation);
		TraceEffect->SetVariableVec3(FName("BeamEnd"), StartLocation + (DirEffect * 100.f));

		if ((FinishLocation - StartLocation).Size() < 30.f)
		{
			TraceEffect->DestroyComponent();
			Destroy();
		}
	}
}

void ATraceEffect::SetMoveingPoint(FVector StartPoint, FVector FinishPoint)
{
	StartLocation = StartPoint;
	FinishLocation = FinishPoint;

	DirEffect = FinishLocation - StartLocation;
	DirEffect.Normalize();

	TraceEffect = UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), TraceEffectSystem, StartLocation);

	TraceEffect->SetVariableVec3(FName("BeamStart"), StartLocation);
	TraceEffect->SetVariableVec3(FName("BeamEnd"), StartLocation + (DirEffect * 300.f));
}

