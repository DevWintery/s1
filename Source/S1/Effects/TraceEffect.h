// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TraceEffect.generated.h"

UCLASS()
class S1_API ATraceEffect : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATraceEffect();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	void SetMoveingPoint(FVector StartPoint, FVector FinishPoint);

private:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Niagara, meta = (AllowPrivateAccess = true))
	TObjectPtr<class UNiagaraSystem> TraceEffectSystem;

private:
	UPROPERTY()
	class USceneComponent* Root;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Niagara, meta = (AllowPrivateAccess = true))
	class UNiagaraComponent* TraceEffect;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Speed , meta = (AllowPrivateAccess = true))
	float EffectSpeed;

private:
	FVector StartLocation;
	FVector FinishLocation;
	FVector DirEffect;
};
