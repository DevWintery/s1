// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "LoginGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class S1_API ALoginGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	ALoginGameModeBase();

public:
	virtual void StartPlay() override;

private:
	UPROPERTY();
	TSubclassOf<class UUserWidget> m_LoginClass;

	UPROPERTY();
	class UUserWidget* m_LoginWidget;

	UPROPERTY()
	TSubclassOf<class UUserWidget> m_SignUpClass;

	UPROPERTY()
	class UUserWidget* m_SignUpWidget;
	
};
