// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "S1Defines.h"
#include "GameFramework/GameModeBase.h"
#include "Protocol.pb.h"
#include "LobbyGameModeBase.generated.h"

DECLARE_MULTICAST_DELEGATE_OneParam(FOnErrorDelegate, int);

/**
 *
 */
UCLASS()
class S1_API ALobbyGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	ALobbyGameModeBase();

public:
	virtual void StartPlay() override;

public:
	//void RefreshRoomList(const Protocol::S_ROOM_LIST& Pkt);
	void RefreshUserList(const TArray<FString>& Players);
	void RefreshUserList(const FString& Player);

public:
	void SetRoomID(int64 ID);
	int64 GetRoomID() const { return RoomID; }

	void SetCurrentStage(const char* NameOfStage);
	const char* GetCurrnetStage() const { return NameOfCurrentStage; }

	void SetNumOfSelectStage(int Num) { NumOfSelectStage  = Num; }

	int GetNumOfSelectStage() const { return NumOfSelectStage; }

public:
	FOnErrorDelegate OnError;

private:
	UFUNCTION()
	void SendEnterPacket();
	
	UFUNCTION(BlueprintCallable)
	void ShowInRoomWidget();

	UFUNCTION(BlueprintCallable)
	void HideLobbyWidget();

	UFUNCTION(BlueprintCallable)
	void ShowCreateRoomWidget();

	UFUNCTION(BlueprintCallable)
	void HideCreateRoomWidget();

	UFUNCTION(BlueprintCallable)
	void HideInRoomWidget();

	UFUNCTION()
	void GoBackScene();

public:
	UFUNCTION()
	void SetRefreshScene(ELobbyScene LastScene);
	void ManageVisible(ELobbyScene KeyScene);
	void SetGoldAndSp();
	void SetErrorMsg(int ErrorIdx);

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	TSubclassOf<class UUserWidget> LobbyWidgetInstance;

	UPROPERTY(VisibleAnywhere)
	class UUserWidget* LobbyWidget;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	TSubclassOf<class UUserWidget> CreateRoomInstance;

	UPROPERTY(VisibleAnywhere)
	class UUserWidget* CreateRoomWidget;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	TSubclassOf<class UUserWidget> InRoomInstance;

	UPROPERTY(VisibleAnywhere)
	class UUserWidget* InRoomWidget;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	TSubclassOf<class UUserWidget> SelectMapInstance;

	UPROPERTY(VisibleAnywhere)
	class UUserWidget* SelectMapWidget;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	TSubclassOf<class UUserWidget> LobbyCommonInstance;

	UPROPERTY(VisibleAnywhere)
	class UUserWidget* LobbyCommonWidget;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	TSubclassOf<class UUserWidget> LobbyMainInstance;

	UPROPERTY(VisibleAnywhere)
	class UUserWidget* LobbyMainWidget;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	TSubclassOf<class UUserWidget> InventoryInstance;

	UPROPERTY(VisibleAnywhere)
	class UUserWidget* InventoryWidget;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	TSubclassOf<class UUserWidget> ShopInstance;

	UPROPERTY(VisibleAnywhere)
	class UUserWidget* ShopWidget;


private:
	TMap<ELobbyScene, class UUserWidget*> LobbyUIList;
	TArray<ELobbyScene> SceneStack;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true))
	class USoundBase* MsgSound;

private:
	int64 RoomID;
	const char* NameOfCurrentStage;
	int NumOfSelectStage = 0;

};
