// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "StageGameMode.generated.h"

/**
 *
 */
UCLASS()
class S1_API AStageGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AStageGameMode();

protected:
	virtual void StartPlay() override;
	virtual void Tick(float DeltaSeconds) override;

public:
	uint64 RegisterObject(AActor* Actor);
	bool UnRegisterObject(uint64 ID);

	AActor* GetObject(uint64 ID);

	void SetStep(uint64 Step);
	FORCEINLINE uint64 GetNumOfStep() const { return StepNumber; }

	void NextStep();

	void StageClear();
	void TimerFunction();
	void MissionFailed();

	FORCEINLINE bool GetPlayingCine() { return bPlayingCine; }
	FORCEINLINE bool GetIsClear() { return bIsClear; }

	void StartCine(FString NameOfCine);

	UFUNCTION()
	void FinishCine();

private:
	uint64 GenerateID();

private:
	TMap<uint64, AActor*> Objects;
	uint64 IDGenerator = 0;
	uint64 StepNumber = 0;
	bool bPlayingCine = false;
	bool bIsClear = false;
};
