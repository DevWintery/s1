// Fill out your copyright notice in the Description page of Project Settings.


#include "LoginGameModeBase.h"
#include "Blueprint/UserWidget.h"
#include "Kismet/GameplayStatics.h"

ALoginGameModeBase::ALoginGameModeBase()
{
	static ConstructorHelpers::FClassFinder<UUserWidget> LOGIN_CLASS(TEXT("/Game/S1/UI/Login/WBP_Login_Screen.WBP_Login_Screen_C"));

	if (LOGIN_CLASS.Succeeded())
	{
		m_LoginClass = LOGIN_CLASS.Class;
	}

	static ConstructorHelpers::FClassFinder<UUserWidget> SIGNUP_CLASS(TEXT("/Game/S1/UI/Login/WBP_SignUp.WBP_SignUp_C"));

	if (SIGNUP_CLASS.Succeeded())
	{
		m_SignUpClass = SIGNUP_CLASS.Class;
	}
}

void ALoginGameModeBase::StartPlay()
{
	Super::StartPlay();

	if (nullptr != m_LoginClass)
	{
		m_LoginWidget = CreateWidget(UGameplayStatics::GetPlayerController(GetWorld(), 0), m_LoginClass);
	}

	if (nullptr != m_LoginWidget)
	{
		m_LoginWidget->AddToViewport();

		UGameplayStatics::GetPlayerController(GetWorld(), 0)->SetShowMouseCursor(true);
	}

	if (nullptr != m_SignUpClass)
	{
		m_SignUpWidget = CreateWidget(UGameplayStatics::GetPlayerController(GetWorld(), 0), m_SignUpClass);
	}
}
