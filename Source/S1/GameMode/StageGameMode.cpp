// Fill out your copyright notice in the Description page of Project Settings.


#include "GameMode/StageGameMode.h"
#include "BaseActor.h"
#include "Kismet/GameplayStatics.h"
#include "MovieSceneSequencePlayer.h"
#include "LevelSequenceActor.h"
#include "LevelSequencePlayer.h"
#include "EngineUtils.h"

#include "HeroController.h"
#include "HeroInGameHUD.h"
#include "S1GameInstance.h"
#include "QuestManager.h"
#include "UIManager.h"
#include "DataManager.h"
#include "RewardData.h"
#include "ClearWidget.h"

AStageGameMode::AStageGameMode()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AStageGameMode::StartPlay()
{
	Super::StartPlay();

	//게임 시작했다고 패킷 보내기
	US1GameInstance* GameInstance = Cast<US1GameInstance>(GetGameInstance());
	if (GameInstance)
	{
		Protocol::C_GAME_INIT Pkt;
		auto SendBuffer = ClientPacketHandler::MakeSendBuffer(Pkt);
		GameInstance->SendPacket(SendBuffer);
	}

	if (APlayerController* PC = UGameplayStatics::GetPlayerController(GetWorld(), 0))
	{
		PC->SetInputMode(FInputModeGameOnly());
	}

	UQuestManager* QuestManager = GameInstance->GetSubsystem<UQuestManager>();
	QuestManager->SetQuestData(StepNumber);

	if (!QuestManager->GetPlayCine())
	{
		if (AHeroInGameHUD* HUD = Cast<AHeroInGameHUD>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHUD()))
		{
			HUD->SetQuestSummary(QuestManager->GetPlayTimer());
			HUD->PlayerHUDAnimation(EUIType::QUESTSUMMARY);
		}
	}

	else
	{
		StartCine(UGameplayStatics::GetCurrentLevelName(GetWorld()));
	}

}

void AStageGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

uint64 AStageGameMode::RegisterObject(AActor* Actor)
{
	uint64 ID = GenerateID();
	Objects.Add(ID, Actor);

	return ID;
}

bool AStageGameMode::UnRegisterObject(uint64 ID)
{
	AActor** Actor = Objects.Find(ID);
	if (Actor == nullptr)
	{
		return false;
	}

	Objects.Remove(ID);
	return true;
}

AActor* AStageGameMode::GetObject(uint64 ID)
{
	AActor** Actor = Objects.Find(ID);
	if (Actor == nullptr)
	{
		return nullptr;
	}

	return (*Actor);
}

void AStageGameMode::SetStep(uint64 Step)
{
	StepNumber = Step;

	//퀘스트 인터페이스, 목표 지정 위치 변경해주는 코드 
	UGameInstance* GameInstance = GetGameInstance();
	UQuestManager* QuestManager = GameInstance->GetSubsystem<UQuestManager>();
	QuestManager->SetQuestData(StepNumber);
	bIsClear = QuestManager->GetIsClearStep();

	if (QuestManager->GetPlayCine())
	{
		StartCine(QuestManager->GetNameOfCine());
	}

	else
	{
		if ("" != QuestManager->GetSummary())
		{
			if (AHeroInGameHUD* HUD = Cast<AHeroInGameHUD>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHUD()))
			{
				HUD->SetQuestSummary(QuestManager->GetPlayTimer());
				HUD->PlayerHUDAnimation(EUIType::QUESTSUMMARY);
			}
		}

		if (bIsClear)
		{
			StageClear();
		}
	}

	for (const auto& Object : Objects)
	{
		ABaseActor* Actor = Cast<ABaseActor>(Object.Value);
		if (Actor == nullptr || Actor->IsStepObject() == false)
		{
			continue;
		}

		if (Actor->GetStepID() == StepNumber)
		{
			Actor->Interact();
		}
	}
}

void AStageGameMode::NextStep()
{
	SetStep(StepNumber + 1);
}

void AStageGameMode::StageClear()
{
	US1GameInstance* S1GameInstance = Cast<US1GameInstance>(GetGameInstance());
	UUIManager* UIManager = S1GameInstance->GetUIManager();

	UQuestManager* QuestManager = S1GameInstance->GetSubsystem<UQuestManager>();
	UDataManager* DataManager = S1GameInstance->GetDataManager();

	FRewardData* Reward = DataManager->GetRewardData(UGameplayStatics::GetCurrentLevelName(GetWorld()));

	DataManager->SetCurrentGold(DataManager->GetCurrentGold() + Reward->Gold);
	DataManager->SetCurrentSp(DataManager->GetCurrentSp() + Reward->Sp);

	int NumOfClearStage = S1GameInstance->GetNumOfClearStage();
	S1GameInstance->SetNumOfClearStage(++NumOfClearStage);
	S1GameInstance->SetNumOfPlayer(0);

	if (QuestManager->GetIsGameClear())
	{
		UIManager->CreateUIWidget(EUIType::GAMECLEAR);
	}

	else
	{
		UIManager->CreateUIWidget(EUIType::CLEAR);
		Cast<UClearWidget>(UIManager->GetUI(EUIType::CLEAR))->SetReward(Reward->Gold, Reward->Sp);
	}

	if (AHeroController* HC = Cast<AHeroController>(UGameplayStatics::GetPlayerController(GetWorld(), 0)))
	{
		HC->StageClear();
	}
}

void AStageGameMode::TimerFunction()
{ 
}

void AStageGameMode::MissionFailed()
{
}

void AStageGameMode::StartCine(FString NameOfCine)
{	
	ALevelSequenceActor* SequenceActor = nullptr;
	for (TActorIterator<ALevelSequenceActor> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		const FString Name = ActorItr->GetActorNameOrLabel();
		if (Name.Contains(NameOfCine))  // 원하는 조건으로 변경 가능
		{
			SequenceActor = *ActorItr;
			break;
		}
	}

	if (SequenceActor)
	{
		// 시퀀스 플레이어 생성 및 구성

		FMovieSceneSequencePlaybackSettings Settings;
		Settings.bDisableLookAtInput = true;
		Settings.bDisableMovementInput = true;
		Settings.bHideHud = true;

		ULevelSequencePlayer* SequencePlayer = ULevelSequencePlayer::CreateLevelSequencePlayer(
			GetWorld(),
			SequenceActor->GetSequence(),
			Settings,
			SequenceActor
		);

		if (SequencePlayer)
		{
			// 시퀀스 재생
			SequencePlayer->Play();
			bPlayingCine = true;

			if (AHeroInGameHUD* HUD = Cast<AHeroInGameHUD>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHUD()))
			{
				HUD->PlayCine(bPlayingCine);
			}

			SequencePlayer->OnFinished.AddDynamic(this, &AStageGameMode::FinishCine);
		}
	}
}

void AStageGameMode::FinishCine()
{
	bPlayingCine = false;

	if (AHeroInGameHUD* HUD = Cast<AHeroInGameHUD>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHUD()))
	{
		HUD->PlayCine(bPlayingCine);
	}

	if (!bIsClear)
	{
		UGameInstance* GameInstance = GetGameInstance();
		UQuestManager* QuestManager = GameInstance->GetSubsystem<UQuestManager>();
		QuestManager->SetQuestData(StepNumber);

		if("" != QuestManager->GetSummary())
		{ 
			if (AHeroInGameHUD* HUD = Cast<AHeroInGameHUD>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHUD()))
			{
				HUD->SetQuestSummary(QuestManager->GetPlayTimer());
				HUD->PlayerHUDAnimation(EUIType::QUESTSUMMARY);
			}
		}

		if (QuestManager->GetPlayTimer())
		{
			FTimerHandle Playtimer;
			GetWorldTimerManager().SetTimer(Playtimer, this, &AStageGameMode::TimerFunction, QuestManager->GetTimerDelay());
		}
	}

	else if (bIsClear)
	{
		StageClear();
	}

}

uint64 AStageGameMode::GenerateID()
{
	return IDGenerator ++;
}
