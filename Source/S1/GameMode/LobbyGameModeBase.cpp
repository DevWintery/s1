// Fill out your copyright notice in the Description page of Project Settings.


#include "LobbyGameModeBase.h"
#include "Blueprint/UserWidget.h"
#include "Kismet/GameplayStatics.h"
#include "UI/Lobby/LobbyWidget.h"
#include "UI/Lobby/CreateRoomWidget.h"
#include "UI/Lobby/InRoomWidget.h"
#include "UI/Lobby/LobbyCommonWidget.h"
#include "LobbyMainWidget.h"
#include "InventoryWidget.h"
#include "SelectMapWidget.h"
#include "Components/WidgetComponent.h"
#include "LobbyCharacter.h"
#include "LobbyController.h"
#include "DataManager.h"
#include "UIManager.h"
#include "ErrorWidget.h"
#include "InventoryItemWidget.h"

ALobbyGameModeBase::ALobbyGameModeBase()
{
}

void ALobbyGameModeBase::StartPlay()
{
	Super::StartPlay();

	if (LobbyCommonInstance)
	{
		LobbyCommonWidget = CreateWidget(UGameplayStatics::GetPlayerController(GetWorld(), 0), LobbyCommonInstance);

		if (LobbyCommonWidget)
		{
			LobbyCommonWidget->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
			LobbyCommonWidget->AddToViewport();
		}

		if (ULobbyCommonWidget* LobbyCommon = Cast<ULobbyCommonWidget>(LobbyCommonWidget))
		{
			LobbyCommon->OnGoBackScene.AddDynamic(this, &ALobbyGameModeBase::GoBackScene);
			SetGoldAndSp();
		}

		LobbyUIList.Add(ELobbyScene::COMMON, LobbyCommonWidget);
	}

	if (LobbyWidgetInstance)
	{
		LobbyWidget = CreateWidget(UGameplayStatics::GetPlayerController(GetWorld(), 0), LobbyWidgetInstance);

		if (LobbyWidget)
		{
			LobbyWidget->SetVisibility(ESlateVisibility::Collapsed);
			LobbyWidget->AddToViewport();

			if (ULobbyWidget* Lobby = Cast<ULobbyWidget>(LobbyWidget))
			{
				Lobby->OnCreateRoom.AddDynamic(this, &ALobbyGameModeBase::ShowCreateRoomWidget);
			}

			LobbyUIList.Add(ELobbyScene::CREATEROOM, LobbyWidget);
		}
	}

	if (CreateRoomInstance)
	{
		CreateRoomWidget = CreateWidget(UGameplayStatics::GetPlayerController(GetWorld(), 0), CreateRoomInstance);

		if (CreateRoomWidget)
		{
			CreateRoomWidget->SetVisibility(ESlateVisibility::Collapsed);
			CreateRoomWidget->AddToViewport();
		}

		if (UCreateRoomWidget* CreateRoom = Cast<UCreateRoomWidget>(CreateRoomWidget))
		{
			CreateRoom->OnConfirm.AddDynamic(this, &ALobbyGameModeBase::HideLobbyWidget);
			CreateRoom->OnConfirm.AddDynamic(this, &ALobbyGameModeBase::HideCreateRoomWidget);
			CreateRoom->OnConfirm.AddDynamic(this, &ALobbyGameModeBase::ShowInRoomWidget);
			CreateRoom->OnCancel.AddDynamic(this, &ALobbyGameModeBase::HideCreateRoomWidget);
		}
	}

	if (InRoomInstance)
	{
		InRoomWidget = CreateWidget(UGameplayStatics::GetPlayerController(GetWorld(), 0), InRoomInstance);

		if (InRoomWidget)
		{
			InRoomWidget->SetVisibility(ESlateVisibility::Collapsed);
			InRoomWidget->AddToViewport();

			LobbyUIList.Add(ELobbyScene::INROOM, InRoomWidget);
		}

		if (UInRoomWidget* InRoom = Cast<UInRoomWidget>(InRoomWidget))
		{
			InRoom->OnSelectMap.AddUObject(this, &ALobbyGameModeBase::SetRefreshScene);
		}
	}

	if (SelectMapInstance)
	{
		SelectMapWidget = CreateWidget(UGameplayStatics::GetPlayerController(GetWorld(), 0), SelectMapInstance);

		if (SelectMapWidget)
		{
			SelectMapWidget->SetVisibility(ESlateVisibility::Collapsed);
			SelectMapWidget->AddToViewport();

			LobbyUIList.Add(ELobbyScene::SELECTSTAGE, SelectMapWidget);
		}

		if (USelectMapWidget* Select = Cast<USelectMapWidget>(SelectMapWidget))
		{
			Select->OnGoInRoomScene.AddUObject(this, &ALobbyGameModeBase::GoBackScene);
		}
	}

	if (LobbyMainInstance)
	{
		LobbyMainWidget = CreateWidget(UGameplayStatics::GetPlayerController(GetWorld(), 0), LobbyMainInstance);

		if (LobbyMainWidget)
		{
			LobbyMainWidget->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
			LobbyMainWidget->AddToViewport();
		}

		if (ULobbyMainWidget* LobbyMain = Cast<ULobbyMainWidget>(LobbyMainWidget))
		{
			LobbyMain->OnGoSceneCreateRoom.AddUObject(this, &ALobbyGameModeBase::SetRefreshScene);
			LobbyMain->OnGoSceneInventory.AddUObject(this, &ALobbyGameModeBase::SetRefreshScene);
			LobbyMain->OnGoSceneShop.AddUObject(this, &ALobbyGameModeBase::SetRefreshScene);

			LobbyUIList.Add(ELobbyScene::MAIN, LobbyMainWidget);
		}
	}

	if (InventoryInstance)
	{
		InventoryWidget = CreateWidget(UGameplayStatics::GetPlayerController(GetWorld(), 0), InventoryInstance);
		InventoryWidget->AddToViewport();

		if (InventoryWidget)
		{
			InventoryWidget->SetVisibility(ESlateVisibility::Collapsed);
			InventoryWidget->AddToViewport();
		}

		LobbyUIList.Add(ELobbyScene::INVENTORY, InventoryWidget);
	}

	if (ShopInstance)
	{
		ShopWidget = CreateWidget(UGameplayStatics::GetPlayerController(GetWorld(), 0), ShopInstance);
		ShopWidget->AddToViewport();

		if (ShopWidget)
		{
			ShopWidget->SetVisibility(ESlateVisibility::Collapsed);
			ShopWidget->AddToViewport();
		}

		LobbyUIList.Add(ELobbyScene::SHOP, ShopWidget);
	}

	if (APlayerController* PC = UGameplayStatics::GetPlayerController(GetWorld(), 0))
	{
		PC->SetShowMouseCursor(true);
		PC->SetInputMode(FInputModeUIOnly());

		ALobbyController* LC = Cast<ALobbyController>(PC);

		LC->OnBuyItem.AddUObject(this, &ALobbyGameModeBase::SetGoldAndSp);
	}

	//첫 Scene Main 화면으로
	SetRefreshScene(ELobbyScene::MAIN);

	if (US1GameInstance* GameInstance = Cast<US1GameInstance>(GetGameInstance()))
	{
		GameInstance->OnConnectDelegate.AddDynamic(this, &ALobbyGameModeBase::SendEnterPacket);
	}

	OnError.AddUObject(this, &ALobbyGameModeBase::SetErrorMsg);
}

void ALobbyGameModeBase::SendEnterPacket()
{
	US1GameInstance* GameInstance = Cast<US1GameInstance>(GetGameInstance());
	if (GameInstance == nullptr)
	{
		return;
	}

	Protocol::C_ENTER_ROOM Pkt;

	std::string Name = TCHAR_TO_UTF8(*GameInstance->GetPlayerName());
	Pkt.set_player_name(Name);

	Protocol::ClothesInfo* ClothesInfo = Pkt.mutable_clothes_info();

	if (ALobbyCharacter* LobbyCharacter = Cast<ALobbyCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)))
	{
		ClothesInfo->set_helmet_id(LobbyCharacter->GetItemID(ESkeletalMeshParts::HELMET));
		ClothesInfo->set_amor_id(LobbyCharacter->GetItemID(ESkeletalMeshParts::AMOR));
		ClothesInfo->set_belts_id(LobbyCharacter->GetItemID(ESkeletalMeshParts::BELTS));
		ClothesInfo->set_boots_id(LobbyCharacter->GetItemID(ESkeletalMeshParts::BOOTS));
		ClothesInfo->set_glasses_id(LobbyCharacter->GetItemID(ESkeletalMeshParts::GLASSES));
		ClothesInfo->set_gloves_id(LobbyCharacter->GetItemID(ESkeletalMeshParts::GLOVES));
		ClothesInfo->set_mask_id(LobbyCharacter->GetItemID(ESkeletalMeshParts::MASK));
		ClothesInfo->set_nvd_id(LobbyCharacter->GetItemID(ESkeletalMeshParts::NVD));
		ClothesInfo->set_radio_id(LobbyCharacter->GetItemID(ESkeletalMeshParts::RADIO));
		ClothesInfo->set_jacket_id(LobbyCharacter->GetItemID(ESkeletalMeshParts::JACKET));
		ClothesInfo->set_pants_id(LobbyCharacter->GetItemID(ESkeletalMeshParts::PANTS));
	}

	SEND_PACKET(Pkt);
}

void ALobbyGameModeBase::ShowInRoomWidget()
{
	if (InRoomWidget)
	{
		InRoomWidget->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
		SetRefreshScene(ELobbyScene::INROOM);
	}
}

void ALobbyGameModeBase::HideLobbyWidget()
{
	if (LobbyWidget)
	{
		LobbyWidget->SetVisibility(ESlateVisibility::Collapsed);
	}
}

void ALobbyGameModeBase::ShowCreateRoomWidget()
{
	if (CreateRoomWidget)
	{
		CreateRoomWidget->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
	}
}

void ALobbyGameModeBase::HideCreateRoomWidget()
{
	if (CreateRoomWidget)
	{
		CreateRoomWidget->SetVisibility(ESlateVisibility::Collapsed);
	}
}

void ALobbyGameModeBase::HideInRoomWidget()
{
	if (InRoomWidget)
	{
		InRoomWidget->SetVisibility(ESlateVisibility::Collapsed);
	}
}

void ALobbyGameModeBase::GoBackScene()
{
	if (ELobbyScene::MAIN == SceneStack.Last())
	{
		if (US1GameInstance* GameInstance = Cast<US1GameInstance>(GetGameInstance()))
		{
			GameInstance->OpenLevel("Login");
		}
	}

	else
	{
		SceneStack.Pop();
		ManageVisible(SceneStack.Last());
	}
}

void ALobbyGameModeBase::SetRefreshScene(ELobbyScene LastScene)
{
	ManageVisible(LastScene);

	SceneStack.Emplace(LastScene);
}

void ALobbyGameModeBase::ManageVisible(ELobbyScene KeyScene)
{
	for (auto& UI : LobbyUIList)
	{
		if (KeyScene == UI.Key)
		{
			UI.Value->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
		}

		else if (ELobbyScene::COMMON == UI.Key)
		{
			continue;
		}

		else
		{
			UI.Value->SetVisibility(ESlateVisibility::Collapsed);
		}
	}

	FString NameOfScene;

	switch (KeyScene)
	{
	case ELobbyScene::MAIN:
	{
		NameOfScene = TEXT("Lobby");
	}
	break;
	case ELobbyScene::COMMON:
		break;
	case ELobbyScene::CREATEROOM:
	{
		NameOfScene = TEXT("CreateRoom");
	}
	break;
	case ELobbyScene::INVENTORY:
	{
		NameOfScene = TEXT("Inventory");
	}
	break;
	case ELobbyScene::SHOP:
	{
		NameOfScene = TEXT("Shop");
	}
	break;
	case ELobbyScene::OPTION:
	{
		NameOfScene = TEXT("Option");
	}
	break;
	case ELobbyScene::INROOM:
	{
		NameOfScene = TEXT("Room");
	}
	break;
	case ELobbyScene::SELECTSTAGE:
		break;
	default:
		break;
	}

	Cast<ULobbyCommonWidget>(LobbyCommonWidget)->SetTextOfScene(NameOfScene);
}

void ALobbyGameModeBase::SetGoldAndSp()
{
	if (US1GameInstance* GameInstance = Cast<US1GameInstance>(GetGameInstance()))
	{
		if (UDataManager* DM = GameInstance->GetDataManager())
		{
			Cast<ULobbyCommonWidget>(LobbyCommonWidget)->SetGoldAndSp(DM->GetCurrentGold(), DM->GetCurrentSp());
		}
	}
}

void ALobbyGameModeBase::SetErrorMsg(int ErrorIdx)
{
	if (US1GameInstance* GameInstance = Cast<US1GameInstance>(GetGameInstance()))
	{
		if (UUIManager* UM = GameInstance->GetUIManager())
		{
			if (nullptr == UM->GetUI(EUIType::ERROR))
			{
				UM->CreateUIWidget(EUIType::ERROR);
			}

			else
			{
				UM->GetUI(EUIType::ERROR)->AddToViewport(10);
			}

			if (UErrorWidget* Error = Cast<UErrorWidget>(UM->GetUI(EUIType::ERROR)))
			{
				if (UDataManager* DM = GameInstance->GetDataManager())
				{
					Error->SetErrorMessage(DM->GetErrorMessage(ErrorIdx));
					UGameplayStatics::PlaySoundAtLocation(GetWorld(), MsgSound, FVector::ZeroVector);
				}
			}
		}
	}
}

void ALobbyGameModeBase::RefreshUserList(const TArray<FString>& Players)
{
	if (UInRoomWidget* InRoom = Cast<UInRoomWidget>(InRoomWidget))
	{
		HideLobbyWidget();
		//ShowInRoomWidget();
		ManageVisible(ELobbyScene::INROOM);

		InRoom->UpdatePlayer(Players);
	}
}

void ALobbyGameModeBase::RefreshUserList(const FString& Player)
{
	if (UInRoomWidget* InRoom = Cast<UInRoomWidget>(InRoomWidget))
	{
		HideLobbyWidget();
		//ShowInRoomWidget();
		ManageVisible(ELobbyScene::INROOM);

		InRoom->UpdatePlayer(Player);
	}
}

void ALobbyGameModeBase::SetRoomID(int64 ID)
{
	RoomID = ID;

	//GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Green, FString::Printf(TEXT("Set RoomID : %d"), RoomID));
}

void ALobbyGameModeBase::SetCurrentStage(const char* NameOfStage)
{
	NameOfCurrentStage = NameOfStage;
}
