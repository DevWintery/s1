// Fill out your copyright notice in the Description page of Project Settings.


#include "S1GameInstance.h"
#include "Sockets.h"
#include "SocketSubsystem.h"
#include "Common/TcpSocketBuilder.h"
#include "PacketSession.h"
#include "SendBuffer.h"
#include "ClientPacketHandler.h"
#include "S1Defines.h"
#include "BasePlayer.h"
#include "BaseEnemy.h"
#include "GameFramework/PawnMovementComponent.h"
#include "StageGameMode.h"
#include "Kismet/GameplayStatics.h"
#include "BaseActor.h"
#include "HTTPSubsystem.h"
#include "HeroPlayer.h"
#include "DataManager.h"
#include "UIManager.h"
#include "LobbyCharacter.h"

US1GameInstance::US1GameInstance(const FObjectInitializer& ObjectInitializer):
	Super(ObjectInitializer)
{
	GetSubsystem<UHTTPSubsystem>();
}

void US1GameInstance::ConnectToGameServer()
{
	//초기화
	{
		MyPlayer = nullptr;
		Creatures.Empty();
		Players.Empty();
		Monsters.Empty();
	}

	Socket = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateSocket(TEXT("Stream"), TEXT("Client Socket"));

	FIPv4Address Ip;
	FIPv4Address::Parse(IpAddress, Ip);

	TSharedRef<FInternetAddr> InternetAddr = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateInternetAddr();
	InternetAddr->SetIp(Ip.Value);
	InternetAddr->SetPort(Port);

	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, FString::Printf(TEXT("Connecting To Server ... ")));

	bool Connected = Socket->Connect(*InternetAddr);

	if (Connected)
	{
		IsConnected = true;
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT("Connection Success")));

		//Session
		GameServerSession = MakeShared<PacketSession>(Socket);
		GameServerSession->Run();

		OnConnectDelegate.Broadcast();
	}
	else
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Connection Failed")));
	}
}

void US1GameInstance::DisconnectToGameServer()
{
	if (Socket == nullptr || GameServerSession == nullptr || IsConnected == false)
	{
		return;
	}

	GameServerSession->Disconnect();
	IsConnected = false;
}

void US1GameInstance::OpenLevel(const FString& Name)
{
	UWorld* World = GetWorld();
	if (World == nullptr)
	{
		return;
	}

	UGameplayStatics::OpenLevel(World, *Name);
}

UDataManager* US1GameInstance::GetDataManager()
{
	if (UDataManager* DataManager = GetSubsystem<UDataManager>())
	{
		return DataManager;
	}

	return nullptr;
}

UUIManager* US1GameInstance::GetUIManager()
{
	if (UUIManager* UIManager = GetSubsystem<UUIManager>())
	{
		return UIManager;
	}

	return nullptr;
}

void US1GameInstance::HandleRecvPackets()
{
	if (Socket == nullptr || GameServerSession == nullptr || IsConnected == false)
	{
		return;
	}

	GameServerSession->HandleRecvPackets();
}

void US1GameInstance::HandleSpawn(const Protocol::ObjectInfo& ObjectInfo, bool IsMine)
{
	if (Socket == nullptr || GameServerSession == nullptr)
	{
		return;
	}

	// 중복 처리 체크
	if (CheckID(ObjectInfo))
	{
		return;
	}

	if (ObjectInfo.creature_type() == Protocol::CREATURE_TYPE_PLAYER)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("Spawn Player"));

		SpawnPlayer(ObjectInfo, IsMine);
	}
	else if (ObjectInfo.creature_type() == Protocol::CREATURE_TYPE_MONSTER)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("Spawn Monster"));

		SpawnMonster(ObjectInfo);
	}
}

void US1GameInstance::HandleSpawn(const Protocol::S_GAME_INIT& EnterGamePkt)
{
	HandleSpawn(EnterGamePkt.player(), true);
}

void US1GameInstance::HandleSpawn(const Protocol::S_SPAWN& SpawnPkt)
{
	for (auto& Objects : SpawnPkt.objects())
	{
		HandleSpawn(Objects, false);
	}
}

void US1GameInstance::HandleDespawn(uint64 ObjectId)
{
	if (Socket == nullptr || GameServerSession == nullptr)
		return;

	auto* World = GetWorld();
	if (World == nullptr)
		return;

	ABasePlayer** FindActor = Players.Find(ObjectId);
	if (FindActor == nullptr)
		return;

	World->DestroyActor(*FindActor);
}

void US1GameInstance::HandleDespawn(const Protocol::S_DESPAWN& DespawnPkt)
{
	for (auto& ObjectId : DespawnPkt.object_ids())
	{
		HandleDespawn(ObjectId);
	}
}

void US1GameInstance::HandleMove(const Protocol::S_MOVE& MovePkt)
{
	if (Socket == nullptr || GameServerSession == nullptr)
		return;

	auto* World = GetWorld();
	if (World == nullptr)
		return;

	const uint64 ObjectId = MovePkt.info().object_id();

	ABaseCharacter** FindActor = Creatures.Find(ObjectId);
	if (FindActor == nullptr) { return; }

	ABaseCharacter* Actor = (*FindActor);
	/*if (Actor->IsValidLowLevel() == false)
	{
		return;
	}*/

	if (Actor->GetCreatureType() == Protocol::CREATURE_TYPE_PLAYER)
	{
		ABasePlayer* Player = Cast<ABasePlayer>(Actor);
		if (Player->IsMyPlayer())
			return;

		const Protocol::PosInfo& Info = MovePkt.info();
		Player->SetDestInfo(Info);
	}
	else if (Actor->GetCreatureType() == Protocol::CREATURE_TYPE_MONSTER)
	{
		ABaseEnemy* Monster = Cast<ABaseEnemy>(*FindActor);

		Monster->ProcessMovePacket(MovePkt);

		//Protocol::PosInfo* DestInfo = new Protocol::PosInfo();
		//DestInfo->CopyFrom(MovePkt.info());

		//if (FVector(DestInfo->x(), DestInfo->y(), DestInfo->z()) == FVector::ZeroVector)
		//{
		//	GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Blue, TEXT("DestLocation is Zero"));
		//}

		//else
		//{

		//}
	}
}

void US1GameInstance::HandleMonsterState(const Protocol::S_MONSTER_STATE& StatePkt)
{
	const uint64 ObjectID = StatePkt.object_id();
	ABaseEnemy** FindActor = Monsters.Find(ObjectID);
	if (FindActor == nullptr)
	{
		return;
	}

	ABaseEnemy* Monster = (*FindActor);

	const int64 TargetID = StatePkt.target_object_id();
	if (TargetID >= 0)
	{
		ABasePlayer** FindTarget = Players.Find(TargetID);
		if (FindTarget == nullptr)
		{
			return;
		}

		ABasePlayer* Player = (*FindTarget);
		Monster->SetTarget(Player);
	}

	Monster->SetState(StatePkt.state());
}

void US1GameInstance::HandleAnimation(const Protocol::S_ANIMATION_STATE& AnimationPkt)
{
	const uint64 ObjectId = AnimationPkt.object_id();
	ABasePlayer** FindActor = Players.Find(ObjectId);
	if (FindActor == nullptr)
	{
		return;
	}

	ABasePlayer* Player = (*FindActor);

	if (AnimationPkt.animation_state() == Protocol::ANIMATION_STATE_NONE)
	{
	
			Player->SetIsAim(false, false);
			Player->SetIsCrouch(false, false);
			Player->SetIsReload(false, false);
	}

	else if (AnimationPkt.animation_state() == Protocol::ANIMATION_STATE_AIM)
	{
		Player->SetIsAim(true, false);
	}
	else if (AnimationPkt.animation_state() == Protocol::ANIMATION_STATE_RELOAD)
	{
		Player->SetIsReload(true, false);
	}
	else if (AnimationPkt.animation_state() == Protocol::ANIMATION_STATE_CROUCH)
	{
		Player->SetIsCrouch(true, false);
	}
}

void US1GameInstance::HandleAttack(const Protocol::S_ATTACK& AttackPkt)
{
	const uint64 ObjectId = AttackPkt.object_id();
	ABaseCharacter** FindActor = Creatures.Find(ObjectId);
	if (FindActor == nullptr)
	{
		return;
	}

	FVector StartLocation = FVector(AttackPkt.start_x(), AttackPkt.start_y(), AttackPkt.start_z());
	FVector EndLocation = FVector(AttackPkt.end_x(), AttackPkt.end_y(), AttackPkt.end_z());

	switch ((*FindActor)->GetCreatureType())
	{
	case Protocol::CREATURE_TYPE_PLAYER:
	{
		if (ABasePlayer* Player = Cast<ABasePlayer>(*FindActor))
		{
			Player->Attack(StartLocation, EndLocation);
		}
	}
	break;

	case Protocol::CREATURE_TYPE_MONSTER:
	{
		ABaseEnemy* Enemy = Cast<ABaseEnemy>(*FindActor);

		if (nullptr != Enemy && Protocol::MONSTER_STATE_DIE != Enemy->GetState())
		{
			{
				Enemy->Attack(StartLocation, EndLocation);
			}
		}
	}
	break;

	default:
		break;
	}
}

void US1GameInstance::HandleHit(const Protocol::S_HIT& HitPkt)
{
	const uint64 ObjectId = HitPkt.object_id();
	ABaseCharacter** FindActor = Creatures.Find(ObjectId);

	if (FindActor == nullptr)
	{
		return;
	}

	switch ((*FindActor)->GetCreatureType())
	{
	case Protocol::CREATURE_TYPE_PLAYER:
	{
		ABasePlayer* Player = Cast<ABasePlayer>(*FindActor);
		Player->TakeDamaged(HitPkt.damage());
	}
	break;

	case Protocol::CREATURE_TYPE_MONSTER:
	{
		ABaseEnemy* Enemy = Cast<ABaseEnemy>(*FindActor);
		Enemy->TakeDamaged(HitPkt.damage());
	}
	break;

	default:
		break;
	}
}

void US1GameInstance::HandleInteract(const Protocol::S_INTERACT& pkt)
{
	UWorld* World = GetWorld();
	if (World == nullptr)
	{
		return;
	}


	AStageGameMode* GameMode = Cast<AStageGameMode>(UGameplayStatics::GetGameMode(World));
	if (GameMode == nullptr)
	{
		return;
	}

	uint64 ObjectID = pkt.object_id();
	ABaseActor* Actor = Cast<ABaseActor>(GameMode->GetObject(ObjectID));
	if (Actor == nullptr)
	{
		return;
	}

	GameMode->SetStep(pkt.step_id());
	Actor->Interact();
}

void US1GameInstance::HandleChangeWeapon(const Protocol::S_CHANGE_WEAPON& pkt)
{
	const uint64 ObjectId = pkt.object_id();
	ABasePlayer** FindActor = Players.Find(ObjectId);

	if (FindActor == nullptr)
	{
		return;
	}

	ABasePlayer* OtherHero = (*FindActor);
	OtherHero->SetEquipWeapon(pkt.weapon_id());
}

bool US1GameInstance::CheckID(const Protocol::ObjectInfo& Info)
{
	uint64 ObjectID = Info.object_id();

	if (Info.creature_type() == Protocol::CREATURE_TYPE_PLAYER)
	{
		return Players.Find(ObjectID) != nullptr;
	}
	else if (Info.creature_type() == Protocol::CREATURE_TYPE_MONSTER)
	{
		return Monsters.Find(ObjectID) != nullptr;
	}

	return false;
}

void US1GameInstance::SpawnPlayer(const Protocol::ObjectInfo& Info, bool IsMine)
{
	UWorld* World = GetWorld();
	if (World == nullptr)
	{
		return;
	}

	FVector SpawnLocation(Info.pos_info().x(), Info.pos_info().y(), Info.pos_info().z());

	if (IsMine)
	{
		APlayerController* PC = UGameplayStatics::GetPlayerController(this, 0);

		if (PC == nullptr)
		{
			return;
		}

		ABasePlayer* Player = Cast<ABasePlayer>(PC->GetPawn());

		if (Player == nullptr)
		{
			return;
		}

		Player->SetActorLocation(SpawnLocation);
		Player->SetPlayerInfo(Info.pos_info());
		Player->SetCreatureType(Info.creature_type());
		Player->SetClothesInfo(Info.clothes_info());

		MyPlayer = Player;
		AddPlayer(Info.object_id(), Player);
	}
	else
	{
		ABasePlayer* Player = Cast<ABasePlayer>(World->SpawnActor(OtherPlayerClass, &SpawnLocation));
		Player->SetPlayerInfo(Info.pos_info());
		Player->SetCreatureType(Info.creature_type());
		Player->SetClothesInfo(Info.clothes_info());

		AddPlayer(Info.object_id(), Player);
	}
}

void US1GameInstance::SpawnMonster(const Protocol::ObjectInfo& Info)
{
	UWorld* World = GetWorld();
	if (World == nullptr)
	{
		return;
	}

	FVector SpawnLocation(Info.pos_info().x(), Info.pos_info().y(), Info.pos_info().z());
	ABaseEnemy* Monster = Cast<ABaseEnemy>(World->SpawnActor(MonsterClass, &SpawnLocation));

	if (Monster == nullptr)
	{
		return;
	}

	Monster->SetCreatureType(Info.creature_type());
	Monster->SetMonsterAttackType(Info.monster_attack_type());
	AddMonster(Info.object_id(), Monster);
}

ABasePlayer* US1GameInstance::FindPlayer(uint64 ObjectID)
{
	Players.KeySort([](uint64 A, uint64 B) {
		return A < B;
		});

	for (auto& iter : Players)
	{
		if (iter.Key == ObjectID)
		{
			continue;
		}

		else
		{
			return iter.Value;
		}
	}

	return nullptr;
}

void US1GameInstance::AddCreature(uint64 ID, ABaseCharacter* Creature)
{
	Creatures.Add(ID, Creature);
}

void US1GameInstance::AddPlayer(uint64 ID, ABasePlayer* Player)
{
	Players.Add(ID, Player);
	AddCreature(ID, Player);
	++NumOfPlayer;
}

void US1GameInstance::AddMonster(uint64 ID, ABaseEnemy* Monster)
{
	Monsters.Add(ID, Monster);
	AddCreature(ID, Monster);
}

void US1GameInstance::SendPacket(TSharedPtr<class SendBuffer> SendBuffer)
{
	if (Socket == nullptr || GameServerSession == nullptr)
	{
		return;
	}

	GameServerSession->SendPacket(SendBuffer);
}
