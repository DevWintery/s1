// Fill out your copyright notice in the Description page of Project Settings.


#include "GameInstance/HTTPSubsystem.h"

void UHTTPSubsystem::SendHttpPostRequest(const FString& Url, const FString& ContentJson, TFunction<void(bool, const FString&, int32)> CallbackFunction)
{
	TSharedRef<IHttpRequest, ESPMode::ThreadSafe> HttpRequest = FHttpModule::Get().CreateRequest();
	HttpRequest->SetURL(Url);
	HttpRequest->SetVerb(TEXT("POST"));
	HttpRequest->SetHeader(TEXT("Content-Type"), TEXT("application/json"));
	HttpRequest->SetContentAsString(ContentJson);
	HttpRequest->OnProcessRequestComplete().BindLambda([this, CallbackFunction](FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
	{
		int32 StatusCode = Response.IsValid() ? Response->GetResponseCode() : 0;
		FString ResponseContent = Response.IsValid() ? Response->GetContentAsString() : TEXT("");
		CallbackFunction(bWasSuccessful, ResponseContent, StatusCode);
	});
	HttpRequest->ProcessRequest();
}

void UHTTPSubsystem::SendHttpGetRequest(const FString& Url, TFunction<void(bool, const FString&, int32)> CallbackFunction)
{
	TSharedRef<IHttpRequest, ESPMode::ThreadSafe> HttpRequest = FHttpModule::Get().CreateRequest();
	HttpRequest->SetURL(Url);
	HttpRequest->SetVerb(TEXT("GET"));
	HttpRequest->OnProcessRequestComplete().BindLambda([this, CallbackFunction](FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
	{
		int32 StatusCode = Response.IsValid() ? Response->GetResponseCode() : 0;
		FString ResponseContent = Response.IsValid() ? Response->GetContentAsString() : TEXT("");
		CallbackFunction(bWasSuccessful, ResponseContent, StatusCode);
	});
	HttpRequest->ProcessRequest();
}