// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Protocol.pb.h"
#include "S1GameInstance.generated.h"

class ABasePlayer;
class ABaseEnemy;
class ABaseCharacter;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FConnectDelegate);

struct FSpawnPacket
{
	bool IsMine;
	Protocol::ObjectInfo Info;
};

/**
 *
 */
UCLASS()
class S1_API US1GameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	US1GameInstance(const FObjectInitializer& ObjectInitializer);

public:
	UPROPERTY(BlueprintAssignable)
	FConnectDelegate OnConnectDelegate;

	UFUNCTION(BlueprintCallable)
	void ConnectToGameServer();

	UFUNCTION(BlueprintCallable)
	void DisconnectToGameServer();

	void SendPacket(TSharedPtr<class SendBuffer> SendBuffer);

	UFUNCTION(BlueprintCallable)
	void SetIP(const FString& IP) { IpAddress = IP; }
	const FString& GetIP() { return IpAddress; }

	void OpenLevel(const FString& Name);

	void SetNumOfClearStage(int Num) { NumOfClearStage = Num; }
	int GetNumOfClearStage() const { return NumOfClearStage; }

	int GetNumOfPlayer() const { return NumOfPlayer; }
	void SetNumOfPlayer(int Num) { NumOfPlayer = Num;}

public: /* return Manager */
	class UDataManager* GetDataManager();
	class UUIManager* GetUIManager();

private:
	void SendEnterPacket();

	/* 추후 분리 */
public:
	UFUNCTION(BlueprintCallable)
	void HandleRecvPackets();

	void HandleSpawn(const Protocol::ObjectInfo& ObjectInfo, bool IsMine);
	void HandleSpawn(const Protocol::S_GAME_INIT& EnterGamePkt);
	void HandleSpawn(const Protocol::S_SPAWN& SpawnPkt);
	//void HandleSpawn(const Protocol::S_CREATURE_SPAWN& CreatureInfo);

	void HandleDespawn(uint64 ObjectId);
	void HandleDespawn(const Protocol::S_DESPAWN& DespawnPkt);

	void HandleMove(const Protocol::S_MOVE& MovePkt);

	void HandleMonsterState(const Protocol::S_MONSTER_STATE& StatePkt);

	void HandleAnimation(const Protocol::S_ANIMATION_STATE& AnimationPkt);

	void HandleAttack(const Protocol::S_ATTACK& AttackPkt);
	void HandleHit(const Protocol::S_HIT& HitPkt);

	void HandleInteract(const Protocol::S_INTERACT& pkt);
	void HandleChangeWeapon(const Protocol::S_CHANGE_WEAPON& pkt);

public:
	ABasePlayer* FindPlayer(uint64 ObjectID);

private:
	bool CheckID(const Protocol::ObjectInfo& Info);
	void SpawnPlayer(const Protocol::ObjectInfo& Info, bool IsMine);
	void SpawnMonster(const Protocol::ObjectInfo& Info);

private:
	void AddCreature(uint64 ID, ABaseCharacter* Creature);
	void AddPlayer(uint64 ID, ABasePlayer* Player);
	void AddMonster(uint64 ID, ABaseEnemy* Monster);

private:
	class FSocket* Socket;
	FString IpAddress = TEXT("127.0.0.1");
	int16 Port = 7777;
	TSharedPtr<class PacketSession> GameServerSession;
	bool IsConnected = false;
	int NumOfClearStage = 0;
	int NumOfPlayer = 0;

private:
	UPROPERTY(EditAnywhere)
	TSubclassOf<class ABasePlayer> OtherPlayerClass;

	UPROPERTY(EditAnywhere)
	TSubclassOf<class ABaseEnemy> MonsterClass;

	ABasePlayer* MyPlayer; // 기본형으로 정의해야하나
	TMap<uint64, ABaseCharacter*> Creatures;
	TMap<uint64, ABasePlayer*> Players;
	TMap<uint64, ABaseEnemy*> Monsters;

public:
	void SetPlayerName(const FString& Name) { PlayerName = Name; }
	const FString& GetPlayerName() { return PlayerName; }

private:
	FString PlayerName = "";
};
