// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "HttpModule.h"
#include "Interfaces/IHttpRequest.h"
#include "Interfaces/IHttpResponse.h"
#include "HTTPSubsystem.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnHttpResponse, bool, bWasSuccessful, FString, ResponseContent, int32, StatusCode);

UCLASS()
class S1_API UHTTPSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()
	
public:
	void SendHttpPostRequest(const FString& Url, const FString& ContentJson, TFunction<void(bool, const FString&, int32)> CallbackFunction);
	void SendHttpGetRequest(const FString& Url, TFunction<void(bool, const FString&, int32)> CallbackFunction);
};
