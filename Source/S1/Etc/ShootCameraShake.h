// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "LegacyCameraShake.h"
#include "ShootCameraShake.generated.h"

/**
 * 
 */
UCLASS()
class S1_API UShootCameraShake : public ULegacyCameraShake
{
	GENERATED_BODY()

public:
	UShootCameraShake();
	
};
