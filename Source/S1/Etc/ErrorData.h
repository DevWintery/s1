#pragma once
#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "ErrorData.generated.h"

USTRUCT(BlueprintType)
struct FErrorData : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (AllowPrivateAccess = true))
	FString ErrorMessage = "";

};
