#pragma once
#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "InventoryData.generated.h"

USTRUCT(BlueprintType)
struct FInventoryData : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (AllowPrivateAccess = true))
	FString ItemID = "";


};
