#pragma once
#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "RewardData.generated.h"

USTRUCT(BlueprintType)
struct FRewardData : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (AllowPrivateAccess = true))
	int Gold = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (AllowPrivateAccess = true))
	int Sp = 0;

};
