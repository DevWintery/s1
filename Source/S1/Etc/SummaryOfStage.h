#pragma once
#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "SummaryOfStage.generated.h"

USTRUCT(BlueprintType)
struct FSummaryOfStage : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (AllowPrivateAccess = true))
	FString SummaryOfStage = "";


};
