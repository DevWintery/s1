// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Enemy/BaseEnemy.h"
#include "TutorialEnemy.generated.h"

/**
 * 
 */
UCLASS()
class S1_API ATutorialEnemy : public ABaseEnemy
{
	GENERATED_BODY()
	
};
