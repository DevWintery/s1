// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseEnemy.h"
#include "Components/SphereComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/CapsuleComponent.h"
#include "PaperSpriteComponent.h"
#include "Components/WidgetComponent.h"

#include "EnemyInfoViewModel.h"
#include "MVVMSubsystem.h"
#include "View/MVVMView.h"
#include "EnemyHpBarWidget.h"
#include "LineTraceGun.h"
#include "Punch.h"
#include "StageGameMode.h"
#include "AnimInstance/EnemyAnimInstance.h"
#include "HeroPlayer.h"
#include "UIManager.h"


ABaseEnemy::ABaseEnemy()
{
	DestInfo = new Protocol::PosInfo();

	HpBar = CreateDefaultSubobject<UWidgetComponent>(TEXT("HpBarWidget"));
	HpBar->SetupAttachment(GetMesh());

	static ConstructorHelpers::FObjectFinder<UMaterial> DISSOLVE(TEXT("/Game/S1/Effects/Material/M_Dissolve.M_Dissolve"));

	if (DISSOLVE.Succeeded())
	{
		Dissolve = DISSOLVE.Object;
	}
}

void ABaseEnemy::BeginPlay()
{
	Super::BeginPlay();

	EnemyState = Protocol::MONSTER_STATE_IDLE;

	if (nullptr != HpBar)
	{
		HpBar->SetWidgetClass(LoadClass<UUserWidget>(nullptr, TEXT("/Game/S1/UI/InGame/Enemy/WBP_EnemyHpBar.WBP_EnemyHpBar_C")));
		HpBar->SetRelativeLocation(FVector(0.f, 0.f, 200.f));
		HpBar->SetWidgetSpace(EWidgetSpace::Screen);
		HpBar->SetDrawSize(FVector2D(100.f, 5.f));
		HpBar->GetWidget()->SetVisibility(ESlateVisibility::Collapsed);

		InfoVM = Cast<UEnemyHpBarWidget>(HpBar->GetWidget())->GetInfoViewModel();
	}

	if (InfoVM)
	{
		InfoVM->SetMaxHealth(GetMaxHp());
		InfoVM->SetCurrentHealth(GetMaxHp());
	}
}

void ABaseEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (AStageGameMode* GM = Cast<AStageGameMode>(UGameplayStatics::GetGameMode(GetWorld())))
	{
		if (GM->GetIsClear())
		{
			Protocol::C_HIT HitPkt;
			HitPkt.set_object_id(DestInfo->object_id());
			HitPkt.set_damage(GetMaxHp());
			SEND_PACKET(HitPkt);
			return;
		}

		Speed = DestInfo->speed();

		switch (EnemyState)
		{
		case Protocol::MONSTER_STATE_IDLE:
		{
			Speed = 0.f;
		}
		break;

		case Protocol::MONSTER_STATE_MOVE:
		{
			EnemyMove();
		}
		break;

		case Protocol::MONSTER_STATE_ATTACK:
		{

			if (nullptr != Target && !GM->GetPlayingCine())
			{
				FVector StartLocation = GetActorLocation();
				FVector TargetLocation = Target->GetActorLocation();
				FRotator LookAtRotation = UKismetMathLibrary::FindLookAtRotation(StartLocation, TargetLocation);
				CurrentPitch = LookAtRotation.Pitch;
				LookAtRotation.Pitch = 0.f;
				SetActorRotation(LookAtRotation);
			}

			EnemyMove();
		}
		break;
		case Protocol::MONSTER_STATE_DIE:
			Target = nullptr;
			break;
		default:
			break;
		}
	}
}

void ABaseEnemy::Attack(FVector StartLocation, FVector EndLocation)
{
	if (AStageGameMode* GameMode = Cast<AStageGameMode>(UGameplayStatics::GetGameMode(GetWorld())))
	{
		if (GameMode->GetPlayingCine())
		{
			return;
		}
	}

	TimeAcc = 0.f;

	ABaseWeapon* CurrentWeapon = GetEquipWeapon();

	if (nullptr != CurrentWeapon)
	{
		CurrentWeapon->Attack(StartLocation, EndLocation);

		if (UEnemyAnimInstance* CurrentAnimIns = Cast<UEnemyAnimInstance>(GetMesh()->GetAnimInstance()))
		{
			CurrentAnimIns->SetIsAttackStart();
		}
	}
}

void ABaseEnemy::ProcessMovePacket(const Protocol::S_MOVE& MovePkt)
{
	GetCharacterMovement()->MaxWalkSpeed = MovePkt.info().speed();

	DestInfo->CopyFrom(MovePkt.info());

	if (FirstDestInfo == FVector::ZeroVector)
	{
		FirstDestInfo = FVector(DestInfo->x(), DestInfo->y(), DestInfo->z());
	}

	SetActorRotation(UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), FVector(DestInfo->x(), DestInfo->y(), GetActorLocation().Z)));
}

void ABaseEnemy::SetMonsterAttackType(Protocol::MonsterAttackType _AttackType)
{
	EnemyType = _AttackType;

	switch (EnemyType)
	{
	case Protocol::MonsterAttackType::MONSTER_ATTACK_TYPE_NONE:
		break;

	case Protocol::MonsterAttackType::MONSTER_ATTACK_TYPE_RIFLE:
	{
		if (ALineTraceGun* Gun = Cast<ALineTraceGun>(GetGunActor()))
		{
			SetEquipWeapon(Gun);
		}
	}
	break;

	case Protocol::MonsterAttackType::MONSTER_ATTACK_TYPE_PUNCH:
	{
		if (APunch* Punch = Cast<APunch>(GetPunchActor()))
		{
			SetEquipWeapon(Punch);
		}
	}
	break;

	default:
		break;
	}

	ABaseWeapon* CurrnetWeapon = GetEquipWeapon();

	if (UEnemyAnimInstance* AnimIns = Cast<UEnemyAnimInstance>(GetMesh()->GetAnimInstance()))
	{
		AnimIns->SetEnemyAttackType();
	}

}

void ABaseEnemy::SetAttackable(bool bCanAttack)
{

}

void ABaseEnemy::EnemyMove()
{
	//이동은 기본적으로 계속
	FVector DestLocation = FVector(DestInfo->x(), DestInfo->y(), DestInfo->z());

	if (FVector(DestInfo->x(), DestInfo->y(), DestInfo->z()) == FVector::ZeroVector)
	{
		DestLocation = FirstDestInfo;
	}

	if (FVector(DestLocation) == FVector::ZeroVector)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Blue, TEXT("DestLocation is Zero"));
		return;
	}


	FVector Current = GetActorLocation();

	SetActorLocation(FMath::VInterpTo(Current, DestLocation, GetWorld()->GetDeltaSeconds(), Speed));

	DestLocation.Z = 0.f;
	Current.Z = 0.f;

	float Length = (DestLocation - Current).Size();
	if (Length > 70.f)
	{
		FVector DestVector = DestLocation - Current;
		DestVector.Normalize();
		CurrentAngle = FMath::RadiansToDegrees(FMath::Acos(FVector::DotProduct(GetActorForwardVector(), DestVector)));

		float DotProduct = FVector::DotProduct(FVector(0.f, 0.f, 1.f), FVector::CrossProduct(GetActorForwardVector(), DestVector));

		if (0.f > DotProduct)
		{
			CurrentAngle *= -1.f;
		}
	}

	else
	{
		CurrentAngle = 0.f;
	}
}

void ABaseEnemy::TakeDamaged(int Damage)
{
	Super::TakeDamaged(Damage);

	if (Protocol::MONSTER_STATE_DIE == EnemyState)
	{
		return;
	}

	int CurrentDamage = FMath::Min(GetCurrentHp(), Damage);

	SetCurrnetHp(GetCurrentHp() - CurrentDamage);

	if (nullptr != HpBar)
	{
		HpBar->GetWidget()->SetVisibility(ESlateVisibility::HitTestInvisible);
	}

	if (nullptr != InfoVM)
	{
		InfoVM->SetCurrentHealth(GetCurrentHp());
	}

	if (0 >= GetCurrentHp())
	{
		DetachFromControllerPendingDestroy();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		//GetMesh()->SetMaterial(0, Dissolve);
		EnemyState = Protocol::MONSTER_STATE_DIE;
		GetMinimapIcon()->SetHiddenInGame(true);
		HpBar->DestroyComponent();

	}

}
