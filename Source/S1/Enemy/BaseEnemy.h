// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Character/BaseCharacter.h"
#include "S1Defines.h"
#include "BaseEnemy.generated.h"

/**
 *
 */
UCLASS()
class S1_API ABaseEnemy : public ABaseCharacter
{
	GENERATED_BODY()

public:
	ABaseEnemy();

protected:
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Attack(FVector StartLocation, FVector EndLocation) override;

	virtual void TakeDamaged(int Damage) override;

public:
	void ProcessMovePacket(const Protocol::S_MOVE& MovePkt);

	const Protocol::MonsterState& GetState() const { return EnemyState; }
	void SetState(Protocol::MonsterState _State) { EnemyState = _State; }
	void SetMonsterAttackType(Protocol::MonsterAttackType _AttackType);

	void SetTarget(AActor* _Target) { Target = _Target; }

	void SetAttackable(bool bCanAttack);

	void EnemyMove();

	const Protocol::PosInfo* GetPosInfo() const { return DestInfo; }

	FORCEINLINE float GetSpeed() { return Speed; }
	FORCEINLINE float GetAngle() { return CurrentAngle; }
	FORCEINLINE float GetPitch() { return CurrentPitch; }
	FORCEINLINE Protocol::MonsterAttackType GetEnemyType() { return EnemyType; }

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Widget, meta = (AllowPrivateAccess = true))
	class UWidgetComponent* HpBar;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ViewModel, meta = (AllowPrivateAccess = true))
	class UEnemyInfoViewModel* InfoVM;

private:
	const float DISTANCE_MAX = 80.f;
	Protocol::PosInfo* DestInfo;
	FVector FirstDestInfo = FVector::ZeroVector;
	Protocol::MonsterState EnemyState;
	Protocol::MonsterAttackType EnemyType;
	float Speed;

	AActor* Target;
	float TimeAcc = 0.f;
	int NumOfAttack = 3;
	float CurrentAngle;
	float CurrentPitch;

	class UMaterial* Dissolve;



};
