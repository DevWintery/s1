// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Actor/BaseActor.h"
#include "FinishActor.generated.h"

/**
 * 
 */
UCLASS()
class S1_API AFinishActor : public ABaseActor
{
	GENERATED_BODY()
public:
	AFinishActor();

protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	virtual void Interact() override;

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	class UBoxComponent* BoxCollisionComp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true));
	class UInteractComponent* InteractComp;

private:
	bool bCanInteract;
	int NumOfInteractPlayer;
	
};
