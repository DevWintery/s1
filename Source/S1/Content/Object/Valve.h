// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Actor/BaseActor.h"
#include "Valve.generated.h"

/**
 * 
 */
UCLASS()
class S1_API AValve : public ABaseActor
{
	GENERATED_BODY()

public:
	AValve();

protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	virtual void Interact() override;

public:
	bool GetIsInteract() const {return bIsInteract;}

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	class USphereComponent* Collision;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	class UStaticMeshComponent* Mesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true));
	class UInteractComponent* InteractComp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true));
	class URotatingMovementComponent* RotatingComp;

	UPROPERTY(EditAnywhere)
	class UParticleSystem* SmokeEffect;

	UPROPERTY(EditAnywhere)
	class USoundBase* ValveSound;


private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	bool bIsInteract = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	float RotateSpeed = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	float MaxRotateSpeed = 3.f;

};
