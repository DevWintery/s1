// Fill out your copyright notice in the Description page of Project Settings.


#include "Content/Object/Valve.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/RotatingMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "InteractComponent.h"

AValve::AValve()
{
	PrimaryActorTick.bCanEverTick = true;

	Collision = CreateDefaultSubobject<USphereComponent>(TEXT("Collision"));
	SetRootComponent(Collision);

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(Collision);

	InteractComp = CreateDefaultSubobject<UInteractComponent>(TEXT("InteractComp"));
	InteractComp->SetupAttachment(Collision);

	RotatingComp = CreateDefaultSubobject<URotatingMovementComponent>(TEXT("RotatingComp"));
}

void AValve::BeginPlay()
{
	Super::BeginPlay();

	if (InteractComp == nullptr)
	{
		return;
	}

	bool Result = InteractComp->Initialize(GetObjectID(), Collision);

	if (Result == false)
	{
		UE_LOG(LogTemp, Error, TEXT("Failed Intiailze InteractComp"));
	}

	RotatingComp->RotationRate = FRotator(0.f, 0.f, 0.f);
}

void AValve::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if(!bIsInteract)
	{
		return;
	}

	if (MaxRotateSpeed > RotateSpeed)
	{
		RotateSpeed += DeltaSeconds;
	}

	else
	{
		RotateSpeed = MaxRotateSpeed;
		/*bIsInteract = false;*/
	}

	RotatingComp->RotationRate = FRotator(RotateSpeed, 0.f, 0.f);

	AddActorLocalRotation(RotatingComp->RotationRate);

}

void AValve::Interact()
{
	if (bIsInteract)
	{
		return;
	}

	bIsInteract = true;

	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), SmokeEffect, GetActorLocation());
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), ValveSound, GetActorLocation());

	InteractComp->DeInitialize(GetObjectID(), Collision);

}
