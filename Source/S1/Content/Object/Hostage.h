// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Actor/BaseActor.h"
#include "Hostage.generated.h"

/**
 * 
 */
UCLASS()
class S1_API AHostage : public ABaseActor
{
	GENERATED_BODY()

public:
	AHostage();

protected:
	virtual void BeginPlay() override;
	virtual void Interact() override;

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	class USceneComponent* Root;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	class USkeletalMeshComponent* Mesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	class UBoxComponent* BoxCollisionComp;

	UPROPERTY();
	class UInteractComponent* InteractComp;
};
