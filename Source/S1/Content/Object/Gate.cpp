// Fill out your copyright notice in the Description page of Project Settings.


#include "Content/Object/Gate.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "InteractComponent.h"

AGate::AGate()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	SetRootComponent(Root);

	BoxCollisionComp = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollisionComp"));
	BoxCollisionComp->SetupAttachment(Root);

	GateMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("GateMesh"));
	GateMesh->SetupAttachment(Root);

	GateLeftDoor = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("GateLeftDoor"));
	GateLeftDoor->SetupAttachment(GateMesh);

	GateRightDoor = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("GateRightDoor"));
	GateRightDoor->SetupAttachment(GateMesh);

	InteractComp = CreateDefaultSubobject<UInteractComponent>(TEXT("InteractComp"));
	InteractComp->SetupAttachment(Root);
}

void AGate::BeginPlay()
{
	Super::BeginPlay();

	if (InteractComp == nullptr)
	{
		return;
	}

	bool Result = InteractComp->Initialize(GetObjectID(), BoxCollisionComp);
	if (Result == false)
	{
		UE_LOG(LogTemp, Error, TEXT("Failed Intiailze InteractComp"));
	}

	TargetLeft = GateLeftDoor->GetRelativeLocation();
	TargetLeft.X -= 160;

	TargetRight = GateRightDoor->GetRelativeLocation();
	TargetRight.X += 160;
}

void AGate::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (bIsComplete)
	{
		return;
	}

	if (bIsOpen == false)
	{
		return;
	}

	FVector ResultLeft = FMath::VInterpConstantTo(GateLeftDoor->GetRelativeLocation(), TargetLeft, DeltaSeconds, InterpSpeed);
	FVector ResultRight = FMath::VInterpConstantTo(GateRightDoor->GetRelativeLocation(), TargetRight, DeltaSeconds, InterpSpeed);

	GateLeftDoor->SetRelativeLocation(ResultLeft);
	GateRightDoor->SetRelativeLocation(ResultRight);

	if (1.f > FVector::Distance(GateLeftDoor->GetRelativeLocation(), TargetLeft) && 1.f > FVector::Distance(GateRightDoor->GetRelativeLocation(), TargetRight))
	{
		InteractComp->DeInitialize(GetObjectID(), BoxCollisionComp);
		bIsComplete = true;
	}
}

void AGate::Interact()
{
	if (bIsOpen)
	{
		return;
	}

	bIsOpen = true;

	BoxCollisionComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GateMesh->SetCollisionProfileName("NoCollision");
}
