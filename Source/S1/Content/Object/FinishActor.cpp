// Fill out your copyright notice in the Description page of Project Settings.


#include "Content/Object/FinishActor.h"
#include "Components/BoxComponent.h"
#include "InteractComponent.h"
#include "S1GameInstance.h"
#include "StageGameMode.h"
#include "Kismet/GameplayStatics.h"

AFinishActor::AFinishActor()
	: bCanInteract(false)
{
	PrimaryActorTick.bCanEverTick = true;

	BoxCollisionComp = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollisionComp"));
	SetRootComponent(BoxCollisionComp);

	InteractComp = CreateDefaultSubobject<UInteractComponent>(TEXT("InteractComp"));
	InteractComp->SetupAttachment(RootComponent);
}

void AFinishActor::BeginPlay()
{
	Super::BeginPlay();

	if (InteractComp == nullptr)
	{
		return;
	}

	//bool Result = InteractComp->Initialize(GetObjectID(), BoxCollisionComp);
	//if (Result == false)
	//{
	//	UE_LOG(LogTemp, Error, TEXT("Failed Intiailze InteractComp"));
	//}

}

void AFinishActor::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (AStageGameMode* GM = Cast<AStageGameMode>(UGameplayStatics::GetGameMode(GetWorld())))
	{
		int NumOfStep = GM->GetNumOfStep();
		if (8 > NumOfStep)
		{
			return;
		}

		else if (8 <= NumOfStep && !bCanInteract)
		{
			bool Result = InteractComp->Initialize(GetObjectID(), BoxCollisionComp);
			if (Result == false)
			{
				UE_LOG(LogTemp, Error, TEXT("Failed Intiailze InteractComp"));
			}

			bCanInteract = true;
		} 
	}
}

void AFinishActor::Interact()
{
	if (!bCanInteract)
	{
		return;
	}

	BoxCollisionComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	InteractComp->DeInitialize(GetObjectID(), BoxCollisionComp);
}
