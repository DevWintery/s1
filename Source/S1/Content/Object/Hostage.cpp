// Fill out your copyright notice in the Description page of Project Settings.


#include "Content/Object/Hostage.h"
#include "Components/BoxComponent.h"
#include "InteractComponent.h"
#include "UIManager.h"
#include "Components/SkeletalMeshComponent.h"

AHostage::AHostage()
{
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	SetRootComponent(Root);

	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(Root);

	BoxCollisionComp = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollisionComp"));
	BoxCollisionComp->SetupAttachment(Mesh);

	InteractComp = CreateDefaultSubobject<UInteractComponent>(TEXT("InteractComp"));
	InteractComp->SetupAttachment(Root);
}

void AHostage::BeginPlay()
{
	Super::BeginPlay();

	if (InteractComp == nullptr)
	{
		return;
	}

	bool Result = InteractComp->Initialize(GetObjectID(), BoxCollisionComp);
	if (Result == false)
	{
		UE_LOG(LogTemp, Error, TEXT("Failed Intiailze InteractComp"));
	}
}

void AHostage::Interact()
{
	InteractComp->DeInitialize(GetObjectID(), BoxCollisionComp);
	BoxCollisionComp->SetCollisionProfileName("NoCollision");

}
