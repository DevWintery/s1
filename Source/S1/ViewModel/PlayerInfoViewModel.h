// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MVVMViewModelBase.h"
#include "PlayerInfoViewModel.generated.h"

/**
 * 
 */
UCLASS(BlueprintType, Blueprintable)
class S1_API UPlayerInfoViewModel : public UMVVMViewModelBase
{
	GENERATED_BODY()

public:
	void SetCurrentHealth(int32 NewCurrentHealth)
	{
		if (UE_MVVM_SET_PROPERTY_VALUE(CurrentHealth, NewCurrentHealth))
		{
			UE_MVVM_BROADCAST_FIELD_VALUE_CHANGED(GetHealthPercent);
		}
	}

	void SetMaxHealth(int32 NewMaxHealth)
	{
		if (UE_MVVM_SET_PROPERTY_VALUE(MaxHealth, NewMaxHealth))
		{
			UE_MVVM_BROADCAST_FIELD_VALUE_CHANGED(GetHealthPercent);
		}
	}

	void SetEquipAmmo(FText NewEquipAmmo)
	{
		if (UE_MVVM_SET_PROPERTY_VALUE(EquipAmmo, NewEquipAmmo))
		{
			UE_MVVM_BROADCAST_FIELD_VALUE_CHANGED(GetEquipAmmoText);
		}
	}

	void SetTotalAmmo(FText NewTotalAmmo)
	{
		if (UE_MVVM_SET_PROPERTY_VALUE(TotalAmmo, NewTotalAmmo))
		{
			UE_MVVM_BROADCAST_FIELD_VALUE_CHANGED(GetTotalAmmoText);
		}
	}

	int32 GetCurrentHealth() const
	{
		return CurrentHealth;
	}

	int32 GetMaxHealth() const
	{
		return MaxHealth;
	}

	FText GetEquipAmmo() const
	{
		return EquipAmmo;
	}

	FText GetTotalAmmo() const
	{
		return TotalAmmo;
	}



public:
	UFUNCTION(BlueprintPure, FieldNotify)
	float GetHealthPercent() const
	{
		if (MaxHealth != 0)
		{
			return static_cast<float>(CurrentHealth) / static_cast<float>(MaxHealth);
		}
		else
		{
			return 0;
		}
	}

	UFUNCTION(BlueprintPure, FieldNotify)
	FText GetEquipAmmoText() const
	{
		return EquipAmmo;
	}


	UFUNCTION(BlueprintPure, FieldNotify)
	FText GetTotalAmmoText() const
	{
		return TotalAmmo;
	}

private:
	UPROPERTY(BlueprintReadWrite, FieldNotify, Setter, Getter, meta = (AllowPrivateAccess = true))
	int32 CurrentHealth;

	UPROPERTY(BlueprintReadWrite, FieldNotify, Setter, Getter, meta = (AllowPrivateAccess = true))
	int32 MaxHealth;

	UPROPERTY(BlueprintReadWrite, FieldNotify, Setter, Getter,  meta = (AllowPrivateAccess = true))
	FText EquipAmmo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, FieldNotify, Setter, Getter,  meta = (AllowPrivateAccess = true))
	FText TotalAmmo;

private:
	class AHeroPlayer* Player;


};
