// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MVVMViewModelBase.h"
#include "EnemyInfoViewModel.generated.h"

/**
 *
 */
UCLASS(BlueprintType, Blueprintable)
class S1_API UEnemyInfoViewModel : public UMVVMViewModelBase
{
	GENERATED_BODY()

public:
	void SetCurrentHealth(int32 NewCurrentHealth)
	{
		if (UE_MVVM_SET_PROPERTY_VALUE(CurrentHealth, NewCurrentHealth))
		{
			UE_MVVM_BROADCAST_FIELD_VALUE_CHANGED(GetHealthPercent);
		}
	}

	void SetMaxHealth(int32 NewMaxHealth)
	{
		if (UE_MVVM_SET_PROPERTY_VALUE(MaxHealth, NewMaxHealth))
		{
			UE_MVVM_BROADCAST_FIELD_VALUE_CHANGED(GetHealthPercent);
		}
	}

public:
	int32 GetCurrentHealth() const
	{
		return CurrentHealth;
	}

	int32 GetMaxHealth() const
	{
		return MaxHealth;
	}

public:
	UFUNCTION(BlueprintPure, FieldNotify)
	float GetHealthPercent() const
	{
		if (MaxHealth != 0)
		{
			return (float)CurrentHealth / (float)MaxHealth;
		}
		else
		{
			return 0;
		}
	}

private:
	UPROPERTY(BlueprintReadWrite, FieldNotify, Setter, Getter, meta = (AllowPrivateAccess = true))
	int32 CurrentHealth;

	UPROPERTY(BlueprintReadWrite, FieldNotify, Setter, Getter, meta = (AllowPrivateAccess = true))
	int32 MaxHealth;

};
