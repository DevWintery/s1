// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseActor.generated.h"

UCLASS()
class S1_API ABaseActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseActor();

protected:
	virtual void Init();
	virtual void BeginPlay() override;

public:
	uint64 GetObjectID() { return ObjectID; }
	void SetObjectID(uint64 id) { ObjectID = id; }

	uint64 GetStepID() { return StepID; }
	void SetStepID(uint64 id) { StepID = id; }

	bool IsStepObject() { return bStepObject; }

	virtual void Interact() { }

private:
	UPROPERTY(VisibleAnywhere, meta = (AllowPrivateAccess = true))
	uint64 ObjectID;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	bool bStepObject = false;

	/* 해당 스텝에 활성화되어야할때 사용하는 ID */
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true))
	uint64 StepID;
};
