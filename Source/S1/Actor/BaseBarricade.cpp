// Fill out your copyright notice in the Description page of Project Settings.


#include "Actor/BaseBarricade.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "CoverComponent.h"

ABaseBarricade::ABaseBarricade()
{
	Root= CreateDefaultSubobject<USceneComponent>(TEXT("Root"));

	SetRootComponent(Root);

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));

	Mesh->SetupAttachment(Root);

	Collision = CreateDefaultSubobject<USphereComponent>(TEXT("Collision"));

	Collision->SetupAttachment(Mesh);
}

void ABaseBarricade::BeginPlay()
{
	Super::BeginPlay();


}
