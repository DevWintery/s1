// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Actor/BaseActor.h"
#include "GoalActor.generated.h"

/**
 * 
 */
UCLASS()
class S1_API AGoalActor : public ABaseActor
{
	GENERATED_BODY()

public:
	AGoalActor();

public:
	virtual void BeginPlay() override;
	virtual void Tick( float DeltaSeconds ) override;

public:
	void SetLocation(FVector CurrentLocation);

	bool IsInScreenViewport();

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	class USceneComponent* Root;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	class UWidgetComponent* GoalIconWidget;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Icon, meta = (AllowPrivateAccess = true))
	class UPaperSpriteComponent* MinimapIcon;

	UPROPERTY()
	class ABasePlayer* Target;
};
