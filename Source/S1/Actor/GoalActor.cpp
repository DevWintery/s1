// Fill out your copyright notice in the Description page of Project Settings.


#include "Actor/GoalActor.h"
#include "Components/SceneComponent.h"
#include "Components/WidgetComponent.h"
#include "Kismet/GameplayStatics.h"
#include "UIManager.h"
#include "PaperSpriteComponent.h"
#include "GoalIconWidget.h"
#include "HeroPlayer.h"
#include "StageGameMode.h"

AGoalActor::AGoalActor()
{
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(Root);

	GoalIconWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("GoalIconWidget"));
	GoalIconWidget->SetupAttachment(Root);

	static ConstructorHelpers::FClassFinder<UUserWidget> QUEST_WIDGET(TEXT("/Game/S1/UI/InGame/Etc/WBP_GoalIcon.WBP_GoalIcon_C"));
	if (QUEST_WIDGET.Succeeded())
	{
		GoalIconWidget->SetWidgetSpace(EWidgetSpace::Screen);
		GoalIconWidget->SetWidgetClass(QUEST_WIDGET.Class);
		GoalIconWidget->SetDrawSize(FVector2D(47, 54));
		GoalIconWidget->SetRelativeLocation(FVector(0.f, 0.f, 60.f));
	}

	MinimapIcon = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("MinimapIcon"));
	MinimapIcon->SetupAttachment(Root);
	MinimapIcon->SetVisibleInSceneCaptureOnly(true);
}

void AGoalActor::BeginPlay()
{
	Super::BeginPlay();

	Cast<UGoalIconWidget>(GoalIconWidget->GetWidget())->SetIsWidgetComponent(true);
}

void AGoalActor::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (FVector::ZeroVector == GetActorLocation())
	{
		Cast<UGoalIconWidget>(GoalIconWidget->GetWidget())->SetGoalLocation(FVector::ZeroVector);

		MinimapIcon->SetHiddenInGame(true);
		return;
	}

	else if (AStageGameMode* GameMode = Cast<AStageGameMode>(UGameplayStatics::GetGameMode(GetWorld())))
	{
		if (GameMode->GetPlayingCine())
		{
			Cast<UGoalIconWidget>(GoalIconWidget->GetWidget())->SetVisibility(ESlateVisibility::Collapsed);
			return;
		}
		else
		{
			Cast<UGoalIconWidget>(GoalIconWidget->GetWidget())->SetVisibility(ESlateVisibility::HitTestInvisible);
		}
	}


	UGameInstance* GameInstance = GetGameInstance();
	UUIManager* UIManager = GameInstance->GetSubsystem<UUIManager>();
	UIManager->SetGoalIconState(IsInScreenViewport());
}

void AGoalActor::SetLocation(FVector CurrentLocation)
{
	SetActorLocation(CurrentLocation);
	if (GoalIconWidget)
	{
		Cast<UGoalIconWidget>(GoalIconWidget->GetWidget())->SetGoalLocation(CurrentLocation);

		if (FVector::ZeroVector == CurrentLocation)
		{
			Cast<UGoalIconWidget>(GoalIconWidget->GetWidget())->SetVisibility(ESlateVisibility::Collapsed);
		}
	}
}

bool AGoalActor::IsInScreenViewport()
{
	APlayerController* Player = UGameplayStatics::GetPlayerController(this, 0);

	ULocalPlayer* const LP = Player ? Player->GetLocalPlayer() : nullptr;

	if (LP && LP->ViewportClient)
	{
		// get the projection data
		FSceneViewProjectionData ProjectionData;

		if (LP->GetProjectionData(LP->ViewportClient->Viewport, /*out*/ ProjectionData, (int)EStereoscopicPass::eSSP_FULL))
		{
			FMatrix const ViewProjectionMatrix = ProjectionData.ComputeViewProjectionMatrix();

			FVector2D ScreenPosition;

			bool bResult = UGameplayStatics::ProjectWorldToScreen(Player, GetActorLocation(), ScreenPosition);

			if (bResult && ScreenPosition.X > ProjectionData.GetViewRect().Min.X			&&
				ScreenPosition.X < ProjectionData.GetViewRect().Max.X						&& 
				ScreenPosition.Y > ProjectionData.GetViewRect().Min.Y						&& 
				ProjectionData.GetViewRect().Max.Y > ScreenPosition.Y)
			{

				return true;
			}
		}
	}

	return false;
}
