// Fill out your copyright notice in the Description page of Project Settings.


#include "Actor/DestructibleActor.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "BaseEnemy.h"
#include "BasePlayer.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ADestructibleActor::ADestructibleActor()
	: Hp(5)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Collision = CreateDefaultSubobject<USphereComponent>(TEXT("Collision"));
	SetRootComponent(Collision);

	Collision->SetRelativeLocation(GetActorLocation());

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(RootComponent);
	
	RangeCollision = CreateDefaultSubobject<USphereComponent>(TEXT("RangeCollision"));
	RangeCollision->SetupAttachment(RootComponent);

}

void ADestructibleActor::Init()
{
}

// Called when the game starts or when spawned
void ADestructibleActor::BeginPlay()
{
	Super::BeginPlay();
	
	RangeCollision->OnComponentBeginOverlap.AddDynamic(this, &ADestructibleActor::PushListActor);
	RangeCollision->OnComponentEndOverlap.AddDynamic(this, &ADestructibleActor::PopListActor);
}

// Called every frame
void ADestructibleActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ADestructibleActor::TakeDamaged()
{
	if (0 == Hp)
	{
		return;
	}

	--Hp;

	if (0 >= Hp)
	{
		Hp = 0;
		DestroyActor();
	}
}

void ADestructibleActor::DestroyActor()
{
	Protocol::C_HIT Hitpkt;
	
	for (auto& Enemy : EnemyList)
	{
		Hitpkt.set_damage(Enemy->GetMaxHp());
		Hitpkt.set_object_id(Enemy->GetPosInfo()->object_id());
		SEND_PACKET(Hitpkt);
	}

	for (auto& Player : PlayerList)
	{
		if (Player->GetCanDamaged())
		{
			Hitpkt.set_damage(Player->GetMaxHp() / 4.f);
			Hitpkt.set_object_id(Player->GetPlayerInfo()->object_id());
			SEND_PACKET(Hitpkt);
		}
	}

	Collision->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), DestroyEffect, Collision->GetComponentLocation(), FRotator::ZeroRotator, EffectScale);
	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), DestroySound, Collision->GetComponentLocation());
}

void ADestructibleActor::PushListActor(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (ABaseEnemy* Enemy = Cast<ABaseEnemy>(OtherActor))
	{
		EnemyList.Emplace(Enemy);
	}

	else if (ABasePlayer* Player = Cast<ABasePlayer>(OtherActor))
	{
		PlayerList.Emplace(Player);
	}
}

void ADestructibleActor::PopListActor(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (ABaseEnemy* Enemy = Cast<ABaseEnemy>(OtherActor))
	{
		EnemyList.Remove(Enemy);
	}

	else if (ABasePlayer* Player = Cast<ABasePlayer>(OtherActor))
	{
		PlayerList.Remove(Player);
	}
}

