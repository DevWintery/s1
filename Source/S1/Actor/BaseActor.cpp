// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseActor.h"
#include "Kismet/GameplayStatics.h"
#include "StageGameMode.h"

// Sets default values
ABaseActor::ABaseActor()
{
	PrimaryActorTick.bCanEverTick = false;
}

void ABaseActor::Init()
{
	UWorld* World = GetWorld();
	if (World == nullptr)
	{
		return;
	}

	AStageGameMode* GameMode = Cast<AStageGameMode>(UGameplayStatics::GetGameMode(World));
	if (GameMode == nullptr)
	{
		return;
	}

	uint64 ID = GameMode->RegisterObject(this);
	SetObjectID(ID);
}

void ABaseActor::BeginPlay()
{
	Super::BeginPlay();

	Init();
}
