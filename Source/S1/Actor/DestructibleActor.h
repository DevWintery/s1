// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseActor.h"
#include "DestructibleActor.generated.h"

UCLASS()
class S1_API ADestructibleActor : public ABaseActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADestructibleActor();

protected:
	// Called when the game starts or when spawned
	virtual void Init() override;
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	void TakeDamaged();
	void DestroyActor();

	class USphereComponent* GetRange() {return RangeCollision;};

private:
	UFUNCTION()
	void PushListActor(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void PopListActor(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

private:
	UPROPERTY(EditAnywhere)
	class USceneComponent* Root;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	class UStaticMeshComponent* Mesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	class USphereComponent* Collision;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	class USphereComponent* RangeCollision;

	UPROPERTY(EditAnywhere)
	int Hp;

	UPROPERTY(EditAnywhere)
	class UParticleSystem* DestroyEffect;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	FVector EffectScale = FVector::ZeroVector;

	UPROPERTY(EditAnywhere)
	class USoundBase* DestroySound;

private:
	TArray<class ABaseEnemy*> EnemyList;
	TArray<class ABasePlayer*> PlayerList;
};
