// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Actor/BaseActor.h"
#include "BaseBarricade.generated.h"

/**
 * 
 */
UCLASS()
class S1_API ABaseBarricade : public ABaseActor
{
	GENERATED_BODY()

public:
	ABaseBarricade();

protected:
	virtual void BeginPlay() override;

private:
	UPROPERTY(VisibleAnywhere, meta = (AllowPrivateAccess = true))
	class USceneComponent* Root;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true))
	class UStaticMeshComponent* Mesh;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true))
	class UShapeComponent* Collision;

	UPROPERTY()
	class UCoverComponent* Cover;
};
