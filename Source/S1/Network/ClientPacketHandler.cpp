#include "ClientPacketHandler.h"
#include "S1Defines.h"
#include "LobbyGameModeBase.h"

bool Handle_INVALID(SharedPacketSession& session, BYTE* buffer, int32 len)
{
	return true;
}

bool Handle_S_ENTER_ROOM(SharedPacketSession& session, Protocol::S_ENTER_ROOM& pkt)
{
	if (auto* GameMode = Cast<ALobbyGameModeBase>(GWorld->GetAuthGameMode()))
	{
		TArray<FString> Players;
		for (const auto& player : pkt.players())
		{
			Players.Add(*FString(player.player_name().c_str()));
		}
		GameMode->RefreshUserList(Players);
	}

	return true;
}

bool Handle_S_ENTER_GAME(SharedPacketSession& session, Protocol::S_ENTER_GAME& pkt)
{
	if (auto* GameInstance = Cast<US1GameInstance>(GWorld->GetGameInstance()))
	{
		FString MapName = TEXT("/Game/Maps/Levels/");
		MapName += pkt.map_name().c_str();

		GameInstance->OpenLevel(MapName);
	}

	return true;
}

bool Handle_S_GAME_INIT(SharedPacketSession& session, Protocol::S_GAME_INIT& pkt)
{
	if (auto* GameInstance = Cast<US1GameInstance>(GWorld->GetGameInstance()))
	{
		GameInstance->HandleSpawn(pkt);
	}

	return true;
}

bool Handle_S_LEAVE_GAME(SharedPacketSession& session, Protocol::S_LEAVE_GAME& pkt)
{
	if (auto* GameInstance = Cast<US1GameInstance>(GWorld->GetGameInstance()))
	{
		GameInstance->OpenLevel("Lobby");
		GameInstance->DisconnectToGameServer();
	}

	return true;
}

bool Handle_S_SPAWN(SharedPacketSession& session, Protocol::S_SPAWN& pkt)
{
	if (auto* GameInstance = Cast<US1GameInstance>(GWorld->GetGameInstance()))
	{
		GameInstance->HandleSpawn(pkt);
	}

	return true;
}

bool Handle_S_DESPAWN(SharedPacketSession& session, Protocol::S_DESPAWN& pkt)
{
	if (auto* GameInstance = Cast<US1GameInstance>(GWorld->GetGameInstance()))
	{
		GameInstance->HandleDespawn(pkt);
	}

	return true;
}

bool Handle_S_MOVE(SharedPacketSession& session, Protocol::S_MOVE& pkt)
{
	if (auto* GameInstance = Cast<US1GameInstance>(GWorld->GetGameInstance()))
	{
		GameInstance->HandleMove(pkt);
	}

	return true;
}

bool Handle_S_MONSTER_STATE(SharedPacketSession& session, Protocol::S_MONSTER_STATE& pkt)
{
	if (auto* GameInstance = Cast<US1GameInstance>(GWorld->GetGameInstance()))
	{
		GameInstance->HandleMonsterState(pkt);
	}

	return true;
}

bool Handle_S_ANIMATION_STATE(SharedPacketSession& session, Protocol::S_ANIMATION_STATE& pkt)
{
	if (auto* GameInstance = Cast<US1GameInstance>(GWorld->GetGameInstance()))
	{
		GameInstance->HandleAnimation(pkt);
	}
	return true;
}

bool Handle_S_ATTACK(SharedPacketSession& session, Protocol::S_ATTACK& pkt)
{
	if (auto* GameInstance = Cast<US1GameInstance>(GWorld->GetGameInstance()))
	{
		GameInstance->HandleAttack(pkt);
	}
	return true;
}

bool Handle_S_HIT(SharedPacketSession& session, Protocol::S_HIT& pkt)
{
	if (auto* GameInstance = Cast<US1GameInstance>(GWorld->GetGameInstance()))
	{
		GameInstance->HandleHit(pkt);
	}
	return true;
}

bool Handle_S_INTERACT(SharedPacketSession& session, Protocol::S_INTERACT& pkt)
{
	if (auto* GameInstance = Cast<US1GameInstance>(GWorld->GetGameInstance()))
	{
		GameInstance->HandleInteract(pkt);
	}
	return true;
}

bool Handle_S_CHANGE_WEAPON(SharedPacketSession& session, Protocol::S_CHANGE_WEAPON& pkt)
{
	if (auto* GameInstance = Cast<US1GameInstance>(GWorld->GetGameInstance()))
	{
		GameInstance->HandleChangeWeapon(pkt);
	}
	return true;
}
