// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

struct PacketHeader
{
	uint16 size;
	uint16 id;
};

/**
 * Cursor를 이용한 Buffer 유틸 클래스
 */
class S1_API SendBuffer : public TSharedFromThis<SendBuffer>
{
public:
	SendBuffer(int32 BufferSize);
	~SendBuffer();

public:
	void CopyData(void* Data, int32 Length);
	void Close(uint32 WriteSize);

public:
	FORCEINLINE BYTE* Buffer() { return _Buffer.GetData(); }
	FORCEINLINE int32 WriteSize() { return _WriteSize; }
	FORCEINLINE int32 Capacity() { return static_cast<int32>(_Buffer.Num()); }

private:
	TArray<BYTE> _Buffer;
	int32 _WriteSize;
};
