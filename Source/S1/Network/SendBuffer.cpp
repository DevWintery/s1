// Fill out your copyright notice in the Description page of Project Settings.


#include "Network/SendBuffer.h"

SendBuffer::SendBuffer(int32 BufferSize)
{
	_Buffer.SetNum(BufferSize);
}

SendBuffer::~SendBuffer()
{
}

void SendBuffer::CopyData(void* Data, int32 Length)
{
	::memcpy(_Buffer.GetData(), Data, Length);
	_WriteSize = Length;
}

void SendBuffer::Close(uint32 WriteSize)
{
	_WriteSize = WriteSize;
}
