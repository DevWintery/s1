// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

struct S1_API FPacketHeader
{
	FPacketHeader() : PacketSize(0), PacketID(0)
	{
	}

	FPacketHeader(uint16 PacketSize, uint16 PacketID) : PacketSize(PacketSize), PacketID(PacketID)
	{
	}

	friend FArchive& operator<<(FArchive& Ar, FPacketHeader& Header)
	{
		Ar << Header.PacketSize;
		Ar << Header.PacketID;

		return Ar;
	}

	uint16 PacketSize;
	uint16 PacketID;
};


class RecvWorker : public FRunnable
{
public:
	RecvWorker(class FSocket* Socket, TSharedPtr<class PacketSession> Session);
	~RecvWorker();

	virtual bool Init() override;
	virtual uint32 Run() override;
	virtual void Exit() override;

	void Destroy();

private:
	bool ReceivePacket(TArray<uint8>& OutPacket);

	/* Size 만큼의 데이터를 받는 함수 */
	bool ReceiveDesiredBytes(uint8* Results, int32 Size);

protected:
	FRunnableThread* Thread = nullptr;
	bool Running = true;
	class FSocket* Socket;

	TWeakPtr<class PacketSession> SessionRef;
};

class SendWorker : public FRunnable
{
public:
	SendWorker(class FSocket* Socket, TSharedPtr<class PacketSession> Session);
	~SendWorker();

	virtual bool Init() override;
	virtual uint32 Run() override;
	virtual void Exit() override;

	void Destroy();

private:
	bool SendPacket(TSharedPtr<class SendBuffer> SendBuffer);

	/* Size 만큼의 데이터를 받는 함수 */
	bool SendDesiredBytes(const uint8* Buffer, int32 Size);

protected:
	FRunnableThread* Thread = nullptr;
	bool Running = true;
	class FSocket* Socket;

	TWeakPtr<class PacketSession> SessionRef;
};