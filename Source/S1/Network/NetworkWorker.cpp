// Fill out your copyright notice in the Description page of Project Settings.


#include "Network/NetworkWorker.h"
#include "Sockets.h"
#include "Serialization/ArrayWriter.h"
#include "PacketSession.h"
#include "SendBuffer.h"

RecvWorker::RecvWorker(FSocket* Socket, TSharedPtr<PacketSession> Session) :
	Socket(Socket), SessionRef(Session)
{
	Thread = FRunnableThread::Create(this, TEXT("RecvWorker Thread"));
}

RecvWorker::~RecvWorker()
{

}

bool RecvWorker::Init()
{
	return true;
}

uint32 RecvWorker::Run()
{
	while (Running)
	{
		TArray<uint8> Packet;
		
		if (ReceivePacket(OUT Packet))
		{
			if (TSharedPtr<PacketSession> Session = SessionRef.Pin())
			{
				Session->RecvPacketQueue.Enqueue(Packet);
			}
		}
	}

	return 0;
}

void RecvWorker::Exit()
{

}


void RecvWorker::Destroy()
{
	Running = false;
	Thread->Kill(true);
	Thread->WaitForCompletion();
	Thread = nullptr;
}

bool RecvWorker::ReceivePacket(TArray<uint8>& OutPacket)
{
	/* ��� �Ľ� */
	const int32 HeaderSize = sizeof(FPacketHeader);
	TArray<uint8> HeaderBufer;
	HeaderBufer.AddZeroed(HeaderSize);

	if (ReceiveDesiredBytes(HeaderBufer.GetData(), HeaderSize) == false)
	{
		return false;
	}

	/* ��� ���� */
	FPacketHeader Header;
	{
		FMemoryReader Reader(HeaderBufer);
		Reader << Header;
		//UE_LOG(LogTemp, Log, TEXT("Recv PacketID : %d, PacketSize : %d"), Header.PacketID, Header.PacketSize);
	}

	/* ��Ŷ ���� �Ľ� */
	OutPacket = HeaderBufer;

	TArray<uint8> PacketBuffer;
	const int32 PacketSize = Header.PacketSize - HeaderSize;
	if (PacketSize == 0)
	{
		return true;
	}
	OutPacket.AddZeroed(PacketSize);

	if (ReceiveDesiredBytes(&OutPacket[HeaderSize], PacketSize) == false)
	{
		return false;
	}

	return true;
}

bool RecvWorker::ReceiveDesiredBytes(uint8* Results, int32 Size)
{
	uint32 PendingDataSize; //�����ִ� ������ ������
	if (Socket->HasPendingData(PendingDataSize) == false || PendingDataSize <= 0)
	{
		return false;
	}

	/* �ش� �����ŭ �����͸� ���������� ��ȸ */
	int32 Offset = 0;
	while (Size > 0)
	{
		int32 NumRead = 0;
		Socket->Recv(Results + Offset, Size, OUT NumRead);

		check(NumRead <= Size);

		if (NumRead <= 0)
		{
			return false;
		}

		Offset += NumRead;
		Size -= NumRead;
	}

	return true;
}

SendWorker::SendWorker(class FSocket* Socket, TSharedPtr<class PacketSession> Session):
	Socket(Socket), SessionRef(Session)
{
	Thread = FRunnableThread::Create(this, TEXT("SendWorker Thread"));
}

SendWorker::~SendWorker()
{

}

bool SendWorker::Init()
{
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("SendWorker Initialize"));

	return true;
}

uint32 SendWorker::Run()
{
	while (Running)
	{
		TSharedPtr<SendBuffer> SendBuffer;

		if (TSharedPtr<PacketSession> Session = SessionRef.Pin())
		{
			if (Session->SendPacketQueue.Dequeue(OUT SendBuffer))
			{
				SendPacket(SendBuffer);
			}
		}
	}

	return 0;
}

void SendWorker::Exit()
{

}

void SendWorker::Destroy()
{
	Running = false;
	Thread->Kill(true);
	Thread->WaitForCompletion();
	Thread = nullptr;
}

bool SendWorker::SendPacket(TSharedPtr<class SendBuffer> SendBuffer)
{
	if (SendDesiredBytes(SendBuffer->Buffer(), SendBuffer->WriteSize()) == false)
	{
		return false;
	}

	return true;
}

bool SendWorker::SendDesiredBytes(const uint8* Buffer, int32 Size)
{
	while (Size > 0)
	{
		int32 BytesSent = 0;
		if (Socket->Send(Buffer, Size, BytesSent) == false)
		{
			return false;
		}
		
		Size -= BytesSent;
		Buffer += BytesSent;
	}

	return true;
}
