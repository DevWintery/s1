// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "S1Defines.h"
#include "HeroController.generated.h"

/**
 *
 */
UCLASS()
class S1_API AHeroController : public APlayerController
{
	GENERATED_BODY()

public:
	AHeroController();

public:
	void PawnKilled();
	void ChangeModeSpectator();
	void FinishSpectator();
	void StageClear();
	void Interactable(bool bCanInteract);

protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	virtual void SetupInputComponent() override;

private:
	void Move(const struct FInputActionValue& Value);
	void Look(const struct FInputActionValue& Value);
	void NightVision();

private:
	/** MappingContext */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = true))
	class UInputMappingContext* DefaultMappingContext;

	/** Move Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = true))
	class UInputAction* MoveAction;

	/** Look Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = true))
	class UInputAction* LookAction;

	/**	Night Vision Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = true))
	class UInputAction* NightVisionAction;


private:
	bool bIsNightVision = false;
	float TimeAccOfNV = 0.f;
	FPostProcessSettings DefaultPP; // 기본 카메라 세팅

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PostProcessSetting, meta = (AllowPrivateAccess = true))
	FPostProcessSettings NightVisionPP; // 야간 투시경 세팅

	ABasePlayer* CurrentPawn = nullptr;

};
