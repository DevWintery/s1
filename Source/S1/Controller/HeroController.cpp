//Fill out your copyright notice in the Description page of Project Settings.


#include "HeroController.h"
#include "HeroPlayer.h"
#include "S1Defines.h"
#include "PlayerInfoViewModel.h"
#include "StageGameMode.h"
#include "UIManager.h"
#include "HeroInGameHUD.h"
#include "Kismet/GameplayStatics.h"
#include "EnhancedInputSubsystems.h"
#include "EnhancedInputComponent.h"
#include "Camera/CameraComponent.h"

AHeroController::AHeroController()
{

}

void AHeroController::BeginPlay()
{
	Super::BeginPlay();

	if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer()))
	{
		Subsystem->AddMappingContext(DefaultMappingContext, 0);
	}

	CurrentPawn = Cast<ABasePlayer>(GetPawn());
}

void AHeroController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (bIsNightVision)
	{
		if (5.f <= TimeAccOfNV)
		{
			NightVision();
			TimeAccOfNV = 0.f;
		}
		else
		{
			TimeAccOfNV += DeltaSeconds;
		}
	}
}

void AHeroController::SetupInputComponent()
{
	Super::SetupInputComponent();
	//Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(InputComponent))
	{
		//Moving
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &AHeroController::Move);
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Completed, this, &AHeroController::Move);

		//Looking
		EnhancedInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, this, &AHeroController::Look);

		//NightVision
		EnhancedInputComponent->BindAction(NightVisionAction, ETriggerEvent::Started, this, &AHeroController::NightVision);
	}
}

void AHeroController::Move(const FInputActionValue& Value)
{
	if (ABaseCharacter* CurrentPlayer = Cast<ABaseCharacter>(GetPawn()))
	{
		CurrentPlayer->Move(Value);
	}
}

void AHeroController::Look(const FInputActionValue& Value)
{
	if (ABaseCharacter* CurrentPlayer = Cast<ABaseCharacter>(GetPawn()))
	{
		CurrentPlayer->Look(Value);
	}
}

void AHeroController::NightVision()
{
	if (AHeroPlayer* CurrentPlayer = Cast<AHeroPlayer>(GetPawn()))
	{
		if (NVD_MIN > CurrentPlayer->GetItemID(ESkeletalMeshParts::NVD) || NVD_MAX < CurrentPlayer->GetItemID(ESkeletalMeshParts::NVD))
		{
			return;
		}
	}

	else if (bIsNightVision && 5.f > TimeAccOfNV)
	{
		return;
	}

	AHeroInGameHUD* CurrnetHUD = Cast<AHeroInGameHUD>(GetHUD());
	CurrnetHUD->PlayerHUDAnimation(EUIType::BLUREFFECT);

	FTimerHandle VisionChangeTimerHandle;
	float VisionChangeTime = 0.5f;

	GetWorld()->GetTimerManager().SetTimer(VisionChangeTimerHandle, FTimerDelegate::CreateLambda([&]()
		{
			if (AHeroPlayer* CurrnetPlayer = Cast<AHeroPlayer>(GetPawn()))
			{
				if (UCameraComponent* CurrentCamera = CurrnetPlayer->GetFollowCamera())
				{
					if (!bIsNightVision)
					{
						CurrentCamera->PostProcessSettings = NightVisionPP;
						bIsNightVision = true;
					}
					else
					{
						CurrentCamera->PostProcessSettings = DefaultPP;
						bIsNightVision = false;
					}
				}
			}
		
			GetWorld()->GetTimerManager().ClearTimer(VisionChangeTimerHandle);
		}), VisionChangeTime, false);
}

void AHeroController::PawnKilled()
{ 
	if(AHeroInGameHUD* CurrentHUD = Cast<AHeroInGameHUD>(GetHUD()))
	{
		CurrentHUD->PawnKilled();
	}
	SetShowMouseCursor(true);
}

void AHeroController::ChangeModeSpectator()
{
	US1GameInstance* GameInstance = Cast<US1GameInstance>(GetGameInstance());

	if (ABasePlayer* OtherPlayer = GameInstance->FindPlayer(CurrentPawn->GetPlayerInfo()->object_id()))
	{
		SetViewTarget(OtherPlayer);
		UUIManager* UIManager = GameInstance->GetSubsystem<UUIManager>();

		if (nullptr == UIManager->GetUI(EUIType::SPECTATOR))
		{
			UIManager->CreateUIWidget(EUIType::SPECTATOR);
		}
		UIManager->SetGoalIconTarget(OtherPlayer);
		UIManager->ShowUI(EUIType::SPECTATOR);
		UIManager->HideUI(EUIType::GAMEOVER);
	}

}

void AHeroController::FinishSpectator()
{
	US1GameInstance* GameInstance = Cast<US1GameInstance>(GetGameInstance());

	if(CurrentPawn)
	{ 
		SetViewTarget(CurrentPawn);
		UUIManager* UIManager = GameInstance->GetSubsystem<UUIManager>();

		if (nullptr == UIManager->GetUI(EUIType::GAMEOVER))
		{
			UIManager->CreateUIWidget(EUIType::GAMEOVER);
		}

		UIManager->ShowUI(EUIType::GAMEOVER);
		UIManager->HideUI(EUIType::SPECTATOR);
	}
}

void AHeroController::StageClear()
{
	Cast<AHeroInGameHUD>(GetHUD())->StageClear();
	SetShowMouseCursor(true);
	SetInputMode(FInputModeUIOnly());
	UnPossess();
}

void AHeroController::Interactable(bool bCanInteract)
{
	Cast<AHeroInGameHUD>(GetHUD())->Interactable(bCanInteract);
}
