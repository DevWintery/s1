// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "LobbyController.generated.h"

DECLARE_MULTICAST_DELEGATE(FOnBuyItemDelegate);

/**
 *
 */
UCLASS()
class S1_API ALobbyController : public APlayerController
{
	GENERATED_BODY()

public:
	ALobbyController();

public:
	FORCEINLINE class UInventoryComponent* GetInventoryComponent() const { return Inventory; }

public:
	FOnBuyItemDelegate OnBuyItem;

protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;

private:
	class UInventoryComponent* Inventory;

};
