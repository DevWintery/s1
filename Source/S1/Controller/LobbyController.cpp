// Fill out your copyright notice in the Description page of Project Settings.


#include "Controller/LobbyController.h"
#include "InventoryComponent.h"
#include "Engine/DataTable.h"

ALobbyController::ALobbyController()
{
	Inventory = CreateDefaultSubobject<UInventoryComponent>(TEXT("Inventory"));
}

void ALobbyController::BeginPlay()
{
	Super::BeginPlay();
}

void ALobbyController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}
