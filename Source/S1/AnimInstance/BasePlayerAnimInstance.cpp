// Fill out your copyright notice in the Description page of Project Settings.


#include "AnimInstance/BasePlayerAnimInstance.h"
#include "HeroPlayer.h"
#include "Kismet/KismetMathLibrary.h"

void UBasePlayerAnimInstance::NativeInitializeAnimation()
{
	Super::NativeInitializeAnimation();

	if (OwnerPawn == nullptr)
	{
		OwnerPawn = Cast<ABasePlayer>(TryGetPawnOwner());
	}
}

void UBasePlayerAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);

	if (OwnerPawn == nullptr)
	{
		return;
	}

	if (OwnerPawn->IsMyPlayer())
	{
		CurrentSpeed = OwnerPawn->GetVelocity().Size();

		CurrentAngle = FRotationMatrix::MakeFromX(OwnerPawn->GetActorTransform().InverseTransformVectorNoScale(OwnerPawn->GetVelocity())).Rotator().Yaw;
		FRotator Rot = (OwnerPawn->GetControlRotation() - OwnerPawn->GetActorRotation());
		Rot.Normalize();

		CurrentAimPitch = Rot.Pitch;

	}
	else
	{
		CurrentSpeed = OwnerPawn->GetPlayerInfo()->speed();

		CurrentAngle = OwnerPawn->GetAngle();
	}

	if (ABasePlayer* Player = Cast<ABasePlayer>(OwnerPawn))
	{
		bIsCrouch = Player->GetIsCrouch();
		bIsAim = Player->GetIsAim();
		bIsReload = Player->GetIsReload();
		bIsAnumMirror = Player->GetIsAnimMirror();
		bIsAttack = Player->GetIsAttack();
		bIsDie = Player->GetIsDie();
		PlayerState = Player->GetPlayerState();
	}

	bShouldMove = CurrentSpeed > 3.f;

	//TurnInPlace();
}

void UBasePlayerAnimInstance::AnimNotify_SetReload()
{
	if (OwnerPawn->IsMyPlayer())
	{
		AHeroPlayer* Player = Cast<AHeroPlayer>(OwnerPawn);

		Player->FinishReload();
	}

	else
	{
		ABasePlayer* Player = Cast<ABasePlayer>(OwnerPawn);
		bIsReload = false;
		Player->SetIsReload(false);
	}
}

void UBasePlayerAnimInstance::AnimNotify_SetAttack()
{
	ABasePlayer* Player = Cast<ABasePlayer>(OwnerPawn);
	bIsAttack = false;
	Player->SetIsAttack(false);
}

void UBasePlayerAnimInstance::TurnInPlace()
{
	if (!bIsAim)
	{
		RootBornYaw = FMath::FInterpTo(RootBornYaw, 0.f, GetWorld()->GetDeltaSeconds(), 20.f);
		LastMovingRotation = MovingRotation;
		MovingRotation = GetOwningActor()->GetActorRotation();
		return;
	}

	LastMovingRotation = MovingRotation;
	MovingRotation = GetOwningActor()->GetActorRotation();
	RootBornYaw = RootBornYaw - (MovingRotation - LastMovingRotation).Yaw;

	if (GetCurveValue(TEXT("Turning")))
	{
		if (GetCurveValue(TEXT("Turning")))
		{ 
			LastDistanceCurve = DistanceCurveValue;
			DistanceCurveValue = GetCurveValue(TEXT("DistanceCurve"));

			DeltaDistanceCurve = DistanceCurveValue - LastDistanceCurve;

			if (0 < RootBornYaw)
			{
				///Turning left
				RootBornYaw -=  DeltaDistanceCurve;
			}

			else
			{
				RootBornYaw += DeltaDistanceCurve;
			}

			AbsRootBornYaw = FMath::Abs(RootBornYaw);

			if (90.f < AbsRootBornYaw)
			{
				YawExcess = AbsRootBornYaw -  90.f;
			}

			if (0 < RootBornYaw)
			{
				RootBornYaw -= YawExcess;
			}

			else
			{
				RootBornYaw += YawExcess;
			}

		}
	}

	if (GEngine)
	{ 
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("%f"), GetCurveValue(TEXT("Turning"))));
	}
}
