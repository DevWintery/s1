// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "S1Defines.h"
#include "EnemyAnimInstance.generated.h"

/**
 * 
 */
UCLASS()
class S1_API UEnemyAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:
	 virtual void NativeInitializeAnimation() override;
	 virtual void NativeBeginPlay() override;
	 virtual void NativeUpdateAnimation(float DeltaSeconds) override;

public:
	void SetEnemyAttackType();
 public:
	UFUNCTION()
	void AnimNotify_UndoState();

	UFUNCTION()
	void AnimNotify_StartAttack();

	UFUNCTION()
	void AnimNotify_FinHitAttack();

	UFUNCTION()
	void AnimNotify_FinAttack();


public:
	void SetIsAttackStart();

 private:
	 UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = true))
	 float CurrentSpeed;

	 UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = true))
	 float CurrentAngle;

	 UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = true))
	 float CurrentAimPitch;

	 UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = State, meta = (AllowPrivateAccess = true))
	 bool bShouldMove = false;

	 UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = State, meta = (AllowPrivateAccess = true))
	 bool bIsCover = false;

	 UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = State, meta = (AllowPrivateAccess = true))
	 bool bIsAim = false;

	 UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = State, meta = (AllowPrivateAccess = true))
	 bool bIsReload = false;

	 UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = State, meta = (AllowPrivateAccess = true))
	 bool bIsAttack = false;

	 UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = State, meta = (AllowPrivateAccess = true))
	 bool bIsAnumMirror = false;

	 UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = State, meta = (AllowPrivateAccess = true))
	 EEnemyType EnemyType = EEnemyType::Punch;

	 UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = State, meta = (AllowPrivateAccess = true))
	 EEnemyState EnemyState = EEnemyState::IDLE;

	 UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = State, meta = (AllowPrivateAccess = true))
	 float  SpeedMultiple = 100.f;

 private:
	class ABaseEnemy* OwnerEnemy;

	
};
