// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "S1Defines.h"
#include "BasePlayerAnimInstance.generated.h"

/** -------------------
 * BaseClass이지만 Animation을 사용하는 클래스가 현재 플레이어 밖에 없기때문에
 * 해당 클래스에 기능 구현 후 추후에 분리하는 과정을 거칠 예정
 * 우선은 Player용 AnimInstance라고 가정하에 제작
  ------------------------ */

class ABasePlayer;

UCLASS()
class S1_API UBasePlayerAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
	
public:
	void NativeInitializeAnimation() override;
	void NativeUpdateAnimation(float DeltaSeconds) override;

public:
	UFUNCTION()
	void AnimNotify_SetReload();

	UFUNCTION()
	void AnimNotify_SetAttack();

	UFUNCTION()
	void TurnInPlace();

private:
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = true))
	ABasePlayer* OwnerPawn;

	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = true))
	float CurrentSpeed;

	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = true))
	float CurrentAngle;

	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = true))
	float CurrentAimPitch;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = true))
	bool bShouldMove = false;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = State, meta = (AllowPrivateAccess = true))
	bool bIsCrouch = false;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = State, meta = (AllowPrivateAccess = true))
	bool bIsAim = false;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = State, meta = (AllowPrivateAccess = true))
	bool bIsReload = false;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = State, meta = (AllowPrivateAccess = true))
	bool bIsAnumMirror = false;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = State, meta = (AllowPrivateAccess = true))
	bool bIsAttack = false;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = State, meta = (AllowPrivateAccess = true))
	bool bIsDie = false;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = State, meta = (AllowPrivateAccess = true))
	EPlayerState PlayerState = EPlayerState::IDLE;

	/// Turn In Place 관련
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Turn, meta = (AllowPrivateAccess = true))
	float RootBornYaw;

	FRotator MovingRotation = FRotator::ZeroRotator;
	FRotator LastMovingRotation = FRotator::ZeroRotator;
	float DistanceCurveValue = 0.f;
	float LastDistanceCurve = 0.f;
	float DeltaDistanceCurve = 0.f;
	float AbsRootBornYaw = 0.f;
	float YawExcess = 0.f;
};
