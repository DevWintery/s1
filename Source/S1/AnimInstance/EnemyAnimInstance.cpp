// Fill out your copyright notice in the Description page of Project Settings.


#include "AnimInstance/EnemyAnimInstance.h"
#include "BaseEnemy.h"
#include "Punch.h"

void UEnemyAnimInstance::NativeInitializeAnimation()
{
	Super::NativeInitializeAnimation();

	OwnerEnemy = Cast<ABaseEnemy>(TryGetPawnOwner());
}

void UEnemyAnimInstance::NativeBeginPlay()
{
	Super::NativeBeginPlay();
}

void UEnemyAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);

	if (nullptr == OwnerEnemy)
	{
		return;
	}

	CurrentSpeed = OwnerEnemy->GetSpeed();
	CurrentAngle = OwnerEnemy->GetAngle();
	CurrentAimPitch = OwnerEnemy->GetPitch();

	if (0 == CurrentAngle)
	{
		SpeedMultiple = 0;
	}

	else
	{
		SpeedMultiple = 150;
	}

	switch (OwnerEnemy->GetState())
	{
	case Protocol::MONSTER_STATE_IDLE:
		EnemyState = EEnemyState::IDLE;
		break;
	case Protocol::MONSTER_STATE_MOVE:
		EnemyState = EEnemyState::MOVE;
		break;
	case Protocol::MONSTER_STATE_ATTACK:
		EnemyState = EEnemyState::ATTACK;
		break;
		//죽는 애니메이션 처리
	case Protocol::MONSTER_STATE_DIE:
		EnemyState = EEnemyState::DIE;
		break;
	default:
		break;
	}
}

void UEnemyAnimInstance::SetEnemyAttackType()
{
	if (OwnerEnemy)
	{
		if (OwnerEnemy->GetEnemyType() == Protocol::MONSTER_ATTACK_TYPE_PUNCH)
		{
			EnemyType = EEnemyType::Punch;
		}

		else if (OwnerEnemy->GetEnemyType() == Protocol::MONSTER_ATTACK_TYPE_RIFLE)
		{
			EnemyType = EEnemyType::Rifle;
		}
	}
}

void UEnemyAnimInstance::AnimNotify_UndoState()
{
	if (nullptr == OwnerEnemy)
	{
		return;
	}
}

void UEnemyAnimInstance::AnimNotify_StartAttack()
{
	if (OwnerEnemy)
	{
		if (APunch* Punch = Cast<APunch>(OwnerEnemy->GetEquipWeapon()))
		{
			Punch->SetAttackAble(true);
		}
	}
}

void UEnemyAnimInstance::AnimNotify_FinHitAttack()
{
	//if (OwnerEnemy)
	//{
	//	if (APunch* Punch = Cast<APunch>(OwnerEnemy->GetEquipWeapon()))
	//	{
	//		Punch->SetAttackAble(false);
	//	}
	//}
}

void UEnemyAnimInstance::AnimNotify_FinAttack()
{
	bIsAttack = false;

	if (OwnerEnemy)
	{
		if (APunch* Punch = Cast<APunch>(OwnerEnemy->GetEquipWeapon()))
		{
			Punch->SetAttackAble(false);
		}
	}

}

void UEnemyAnimInstance::SetIsAttackStart()
{
	bIsAttack = true;
}
