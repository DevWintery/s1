#pragma once
#include "CoreMinimal.h"
#include "UObject/Class.h"
#include "ClientPacketHandler.h"
#include "S1GameInstance.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"

#define SEND_PACKET(Pkt)																\
	TSharedPtr<SendBuffer> SendBuffer = ClientPacketHandler::MakeSendBuffer(Pkt);		\
	Cast<US1GameInstance>(GWorld->GetGameInstance())->SendPacket(SendBuffer);

#define GET_UIMANAGER(UIManager)														\
	US1GameInstance* GameInstance = Cast<US1GameInstance>(GWorld->GetGameInstance());	\
	UUIManager* UIManager = GameInstance->GetSubsystem<UUIManager>();					

UENUM(BlueprintType)
enum class EPlayerState : uint8
{
	IDLE,
	MOVE,
	AIM,
	ATTACK,
	RELOAD,
	DIE,
};

UENUM(BlueprintType)
enum class EPlayerMoveState : uint8 
{
	STOP,
	MOVE,
};

UENUM(BlueprintType)
enum class EEnemyType : uint8
{
	Rifle,
	Punch,
};

UENUM(BlueprintType)
enum class EEnemyState : uint8 
{
	IDLE,
	MOVE,
	CHASE,
	ATTACK,
	DAMAGED,
	DIE,
};

UENUM(BlueprintType)
enum class EUIType : uint8
{
	NONE,
	HUD,
	CROSSHAIR,
	BLOODOVERLAY,
	MINIMAP,
	QUESTSUMMARY,
	TIMER,
	GAMEOVER,
	SPECTATOR,
	BLUREFFECT,
	DAMAGEFLOTER,
	CLEAR,
	GAMECLEAR,
	GOALICON,
	ERROR,
	INTERACT,
	ITEMSUMMARY,
};

UENUM(BlueprintType)
enum class ESkeletalMeshParts : uint8
{
	NONE, 
	HELMET,
	AMOR,
	BELTS,
	BOOTS,
	GLASSES,
	GLOVES,
	JACKET,
	PANTS,
	MASK,
	NVD,
	RADIO,
};

UENUM(BlueprintType)
enum class EItemType : uint8
{
	NONE,
	EQUIPMENT,
	ETC,
};

UENUM(BlueprintType)
enum class ETypeOfStat : uint8
{
	NONE,
	OFFENCT,
	DEFENCE,
	AMMO,
	HP,
};

UENUM(BlueprintType)
enum class ELobbyScene : uint8
{
	MAIN,
	COMMON,
	CREATEROOM,
	INVENTORY,
	SHOP,
	OPTION,
	INROOM,
	SELECTSTAGE,

};


const float PLAYER_RUN_SPEED = 360.f;
const float PLAYER_WALK_SPEED = 96.5f;
const float PLAYER_CROUCH_WALK_SPEED = 274.f;

const int JACKET_MIN = 1001;
const int JACKET_MAX = 1999;
const int PANTS_MIN = 2001;
const int PANTS_MAX = 2999;
const int BOOTS_MIN = 3001;
const int BOOTS_MAX = 3999;
const int NVD_MIN = 4001;
const int NVD_MAX = 4999;
const int AMOR_MIN = 5001;
const int AMOR_MAX = 5999;
const int HELMET_MIN = 6001;
const int HELMET_MAX = 6999;
const int GLASSES_MIN = 7001;
const int GLASSES_MAX = 7999;
const int MASK_MIN = 8001;
const int MASK_MAX = 8999;
const int GLOVES_MIN = 9001;
const int GLOVES_MAX = 9999;
const int BELTS_MIN = 10001;
const int BELTS_MAX = 10999;
const int RADIO_MIN = 11000;
const int RADIO_MAX = 11999;
