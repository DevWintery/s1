// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon/Bullet.h"
#include "Rocket.generated.h"

/**
 * 
 */
UCLASS()
class S1_API ARocket : public ABaseActor
{
	GENERATED_BODY()
	
public:
	ARocket();

protected:
	// Called when the game starts or when spawned
	virtual void Init() override;
	virtual void BeginPlay() override;
public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	void SetDir(FVector Start, FVector End);

private:	
	UFUNCTION()
	void PushListActor(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void PopListActor(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

private:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = true))
	class UStaticMeshComponent* Mesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = true))
	class USphereComponent* Collision;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = true))
	class UProjectileMovementComponent* Movement;

	UPROPERTY(EditAnywhere)
	class USoundBase* ImpactSound;

	UPROPERTY(EditAnywhere)
	class UParticleSystem* ImpactEffect;

	UPROPERTY(EditAnywhere)
	class UParticleSystem* TrailEffect;

	UPROPERTY(EditAnywhere)
	class USoundBase* FlySound;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true))
	class USphereComponent* RangeOfExplosion;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	FVector EffectScale = FVector::ZeroVector;

private:
	FVector StartLocation;
	FVector EndLocation;
	FVector ShootDirection;

	TArray<class ABaseEnemy*> EnemyList;

};
