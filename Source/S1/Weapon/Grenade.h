// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon/BaseWeapon.h"
#include "Grenade.generated.h"

/**
 *
 */
UCLASS()
class S1_API AGrenade : public ABaseWeapon
{
	GENERATED_BODY()

public:
	AGrenade();

protected:
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;

public:
	virtual void SetEquip(bool _bIsResult, class USkeletalMeshComponent* _Mesh) override;
	virtual void SendAtackPKT() override;
	virtual void Attack(FVector SpawnLocation, FVector EndLocation) override;
	virtual void SetAttackPacket(FVector StartLocation, FVector EndLocation) override;

private:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess))
	class UProjectileMovementComponent* Movement;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Collision, meta = (AllowPrivateAccess))
	class USphereComponent* Collision;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Value, meta = (AllowPrivateAccess))
	FVector Velocity;

};
