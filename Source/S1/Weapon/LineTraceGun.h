// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseWeapon.h"
#include "S1Defines.h"
#include "LineTraceGun.generated.h"

/**
 *
 */
UCLASS()
class S1_API ALineTraceGun : public ABaseWeapon
{
	GENERATED_BODY()

public:
	ALineTraceGun();

protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;

public:
	virtual void SetEquip(bool _bIsResult, class USkeletalMeshComponent* _Mesh) override;

	virtual void PlayEffect(FVector StartLocation, FVector EndLocation) override;

	virtual void SendAtackPKT() override;

	virtual void Attack(FVector SpawnLocation, FVector EndLocation) override;

	virtual void SetAttackPacket(FVector StartLocation, FVector EndLocation) override;

public:/* Set Function */
	void SetIsFusillade(bool _bIsFusillade);

//public: /*Get Function*/
//	FORCEINLINE int GetCurrentAmmo() { return CurrentBullet; }
//	FORCEINLINE int GetToTalAmmo() { return TotalBullet; }

private:
	// �˻� ���� 
	UPROPERTY(EditAnywhere)
	float TraceMaxRange;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Bullet, meta = (AllowPrivateAccess = true))
	TSubclassOf<class ABullet> BulletClass;

	UPROPERTY(EditAnywhere)
	class UParticleSystem* MuzzleFlash;

	UPROPERTY(EditAnywhere)
	class USoundBase* MuzzleSound;
	
	UPROPERTY(EditAnywhere)
	class USoundBase* DrySound;

private:
	bool bIsFusillade;
	float ShootDelayAcc;
	const float SHOOT_DELAY_MAX = 0.15f;
	class ABullet* BulletActor;
	class ABaseActor* Target = nullptr;
};
