// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon/Rocket.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "BaseCharacter.h"
#include "Particles/ParticleSystem.h"
#include "Kismet/GameplayStatics.h"
#include "DestructibleActor.h"
#include "BaseEnemy.h"


ARocket::ARocket()
{
	PrimaryActorTick.bCanEverTick = true;

	Collision = CreateDefaultSubobject<USphereComponent>(TEXT("Collision"));
	SetRootComponent(Collision);

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(Collision);

	RangeOfExplosion = CreateDefaultSubobject<USphereComponent>(TEXT("RangeOfExplosion"));
	RangeOfExplosion->SetupAttachment(Mesh);

	Movement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Movement"));
	Movement->SetUpdatedComponent(Collision);
	Movement->InitialSpeed = 3000.0f;
	Movement->MaxSpeed = 3000.0f;
	Movement->bRotationFollowsVelocity = true;
	Movement->bShouldBounce = false;
	Movement->ProjectileGravityScale = 0.f;

	EffectScale = FVector(2.f, 2.f, 2.f);
}

void ARocket::Init()
{
}

// Called when the game starts or when spawned
void ARocket::BeginPlay()
{
	Super::BeginPlay();
	RangeOfExplosion->OnComponentBeginOverlap.AddDynamic(this, &ARocket::PushListActor);
	RangeOfExplosion->OnComponentEndOverlap.AddDynamic(this, &ARocket::PopListActor);

	UGameplayStatics::SpawnEmitterAttached(TrailEffect, Mesh, TEXT("Trail"));
	UGameplayStatics::SpawnSoundAttached(FlySound, Mesh, TEXT("Trail"));
}


// Called every frame
void ARocket::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector CurrentLocation = GetActorLocation();

	if ((EndLocation - CurrentLocation).Length() < 100.f)
	{	

		Protocol::C_HIT Hitpkt;
		Hitpkt.set_damage(300);

		for (auto& Enemy : EnemyList)
		{
			Hitpkt.set_object_id(Enemy->GetPosInfo()->object_id());
			SEND_PACKET(Hitpkt);
		}

		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactEffect, EndLocation, (ShootDirection.Rotation() * -1), EffectScale);
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ImpactSound, EndLocation);

		Destroy();

	}
}

void ARocket::SetDir(FVector Start, FVector End)
{
	EndLocation = End;
	ShootDirection = (End - Start);
	ShootDirection.Normalize();
	Movement->Velocity = ShootDirection * Movement->InitialSpeed;
}

void ARocket::PushListActor(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (ABaseEnemy* Enemy = Cast<ABaseEnemy>(OtherActor))
	{
		EnemyList.Emplace(Enemy);
	}
}


void ARocket::PopListActor(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (ABaseEnemy* Enemy = Cast<ABaseEnemy>(OtherActor))
	{
		//EnemyList.Remove(Enemy);
	}
}


