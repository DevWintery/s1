// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon/BaseWeapon.h"
#include "RocketLauncher.generated.h"

/**
 * 
 */
UCLASS()
class S1_API ARocketLauncher : public ABaseWeapon
{
	GENERATED_BODY()

public:
	ARocketLauncher();

public:
	virtual void BeginPlay() override;

public:
	virtual void SetEquip(bool _bIsResult, class USkeletalMeshComponent* _Mesh) override;
	virtual void PlayEffect(FVector StartLocation = FVector::ZeroVector, FVector EndLocation = FVector::ZeroVector) override;
	virtual void SendAtackPKT() override;
	virtual void Attack(FVector SpawnLocation, FVector EndLocation) override;
	virtual void SetAttackPacket(FVector StartLocation, FVector EndLocation) override;

private:
	// �˻� ���� 
	UPROPERTY(EditAnywhere)
	float TraceMaxRange;

	UPROPERTY(EditAnywhere)
	TSubclassOf<class ARocket> BulletClass;

	UPROPERTY(EditAnywhere)
	class UParticleSystem* MuzzleFlash;

	UPROPERTY(EditAnywhere)
	class USoundBase* MuzzleSound;

	UPROPERTY(EditAnywhere)
	class USoundBase* DrySound;

private:
	bool bIsFusillade;
	float ShootDelayAcc;
	const float SHOOT_DELAY_MAX = 0.15f;

	class ARocket* BulletActor;
	
};
