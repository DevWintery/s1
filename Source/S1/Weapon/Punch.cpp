// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon/Punch.h"
#include "Components/SphereComponent.h"
#include "S1Defines.h"
#include "BaseEnemy.h"
#include "BasePlayer.h"
#include "DestructibleActor.h"
#include "Components/CapsuleComponent.h"
#include "HeroPlayer.h"

APunch::APunch()
{
	Range = CreateDefaultSubobject<USphereComponent>(TEXT("Range"));
	Range->SetupAttachment(Root);
}

void APunch::BeginPlay()
{
	Super::BeginPlay();

	Range->SetSphereRadius(0.f);
	Range->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Range->OnComponentBeginOverlap.AddDynamic(this, &APunch::OnTarget);

}

void APunch::SetEquip(bool _bIsResult, USkeletalMeshComponent* _Mesh)
{
}

void APunch::PlayEffect(FVector StartLocation, FVector EndLocation)
{
}

void APunch::SendAtackPKT()
{
	SetAttackPacket(GetActorLocation(), GetActorLocation());
}

void APunch::Attack(FVector StartLocation, FVector EndLocation)
{
	bCanOverlapCheck = true;
}

void APunch::SetAttackPacket(FVector StartLocation, FVector EndLocation)
{
	Protocol::C_ATTACK AttackPkt;

	if (ABasePlayer* Player = Cast<ABasePlayer>(GetOwner()))
	{
		AttackPkt.set_object_id(Player->GetPlayerInfo()->object_id());

	}

	else if (ABaseEnemy* Enemy = Cast<ABaseEnemy>(GetOwner()))
	{
		AttackPkt.set_object_id(Cast<ABaseEnemy>(GetOwner())->GetPosInfo()->object_id());
	}

	AttackPkt.set_start_x(StartLocation.X);
	AttackPkt.set_start_y(StartLocation.Y);
	AttackPkt.set_start_z(StartLocation.Z);

	AttackPkt.set_end_x(EndLocation.X);
	AttackPkt.set_end_y(EndLocation.Y);
	AttackPkt.set_end_z(EndLocation.Z);

	SEND_PACKET(AttackPkt);
}

void APunch::SetAttackAble(bool bCanAttack)
{
	if (bCanAttack)
	{
		Range->SetSphereRadius(30.0f);

		Range->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	}

	else
	{
		Range->SetSphereRadius(0.f);
		Range->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
	
}

void APunch::OnTarget(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Protocol::C_HIT Hitpkt;

	AHeroPlayer* CurrentHero = Cast<AHeroPlayer>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));

	if (CurrentHero == OtherActor)
	{
		Hitpkt.set_damage(10.f);
		Hitpkt.set_object_id(CurrentHero->GetPlayerInfo()->object_id());

		SEND_PACKET(Hitpkt);

		Range->SetSphereRadius(0.0f);
		Range->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}