// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseWeapon.h"
#include "Components/SkeletalMeshComponent.h"

// Sets default values
ABaseWeapon::ABaseWeapon()
	: Damage(10.f)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(Root);

	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ABaseWeapon::BeginPlay()
{
	Super::BeginPlay();
}

void ABaseWeapon::SetEquip(bool _bIsResult, USkeletalMeshComponent* _Mesh)
{
}

void ABaseWeapon::PlayEffect(FVector StartLocation, FVector EndLocation)
{
}

void ABaseWeapon::SendAtackPKT()
{
}

void ABaseWeapon::Attack(FVector StartLocation, FVector EndLocation)
{
}

void ABaseWeapon::SetAttackPacket(FVector StartLocation, FVector EndLocation)
{
}

USkeletalMeshComponent* ABaseWeapon::GetMesh() const
{
	return Mesh;
}


AController* ABaseWeapon::GetOwnerController()
{
	APawn* OwnerPawn = Cast<APawn>(GetOwner());

	if (!OwnerPawn)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OwnerPawn is null")));
		return nullptr;
	}

	AController* OwnerController = OwnerPawn->GetController();

	if (!OwnerController)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("OwnerController is null")));
		return nullptr;
	}


	return OwnerController;
	return nullptr;
}

void ABaseWeapon::Reload()
{
	if (MaxBullet == CurrentBullet || 0 == TotalBullet)
	{
		return;
	}

	else if (MaxBullet > TotalBullet + CurrentBullet)
	{
		CurrentBullet += TotalBullet;
		TotalBullet = 0;
	}

	else
	{
		TotalBullet -= MaxBullet - CurrentBullet;
		CurrentBullet = MaxBullet;
	}
}
