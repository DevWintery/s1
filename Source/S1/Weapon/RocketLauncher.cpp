// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon/RocketLauncher.h"
#include "Engine/SkeletalMeshSocket.h"
#include "BasePlayer.h"
#include "BaseEnemy.h"
#include "Rocket.h"
#include "Bullet.h"

ARocketLauncher::ARocketLauncher()
{
}

void ARocketLauncher::BeginPlay()
{
	Super::BeginPlay();

	MaxBullet = 1;
	CurrentBullet = MaxBullet;
	TotalBullet = 2;
}

void ARocketLauncher::SetEquip(bool _bIsResult, USkeletalMeshComponent* _Mesh)
{
	if (true == _bIsResult)
	{
		const USkeletalMeshSocket* WeaponSocket = _Mesh->GetSocketByName("Skt_Rocket01");

		if (nullptr != WeaponSocket)
		{
			WeaponSocket->AttachActor(this, _Mesh);
		}
	}

	else
	{
		const USkeletalMeshSocket* WeaponSocket = _Mesh->GetSocketByName("WeaponEquipRocketALauncher");

		if (nullptr != WeaponSocket)
		{
			WeaponSocket->AttachActor(this, _Mesh);
		}
	}
}

void ARocketLauncher::PlayEffect(FVector StartLocation, FVector EndLocation)
{
}

void ARocketLauncher::SendAtackPKT()
{
	APawn* OwnerPawn = Cast<APawn>(GetOwner());

	FVector ShotDirection = FVector::ZeroVector;
	FVector StartLocation = FVector::ZeroVector;
	FVector EndLocation = FVector::ZeroVector;

	FRotator Rotation;

	if (Cast<ABasePlayer>(OwnerPawn)->IsMyPlayer())
	{
		AController* OwnerController = GetOwnerController();
		OwnerController->GetPlayerViewPoint(StartLocation, Rotation);
	}

	else
	{
		OwnerPawn->GetActorEyesViewPoint(StartLocation, Rotation);
	}

	ShotDirection = -Rotation.Vector();

	EndLocation = StartLocation + Rotation.Vector() * TraceMaxRange;

	FHitResult Hit;
	FCollisionQueryParams Params;

	Params.AddIgnoredActor(GetOwner());
	Params.AddIgnoredActor(this);

	while (GetWorld()->LineTraceSingleByChannel(Hit, StartLocation, EndLocation, ECollisionChannel::ECC_GameTraceChannel1, Params))
	{
		if (Hit.GetActor())
		{
			if (ABaseEnemy* Enemy = Cast<ABaseEnemy>(Hit.GetActor()))
			{
				if (Enemy->GetState() == Protocol::MONSTER_STATE_DIE)
				{
					Params.AddIgnoredActor(Enemy);

					continue;
				}
			}

			if (ABasePlayer* CurrnetPlayer = Cast<ABasePlayer>(Hit.GetActor()))
			{
				if (CurrnetPlayer->GetPlayerState() == EPlayerState::DIE)
				{
					Params.AddIgnoredActor(CurrnetPlayer);
					continue;
				}
			}

			if ((StartLocation - Hit.Location).Length() < (StartLocation - OwnerPawn->GetActorLocation()).Length())
			{
				Params.AddIgnoredActor(Hit.GetActor());
				continue;
			}

			/*if (ADestructibleActor* DA = Cast<ADestructibleActor>(Hit.GetActor()))
			{
				DA->TakeDamaged();
			}*/

			EndLocation = Hit.Location;
			break;
		}

		else
		{
		}
	}

	SetAttackPacket(GetMesh()->GetSocketLocation("Rocket"), EndLocation);
}

void ARocketLauncher::Attack(FVector SpawnLocation, FVector EndLocation)
{
	if (0 == GetCurrentAmmo())
	{
		UGameplayStatics::SpawnSoundAttached(DrySound, Mesh, TEXT("Rocket"));

		return;
	}

	else
	{
		ABasePlayer* OwnerPlayer = Cast<ABasePlayer>(GetOwner());

		ShootDelayAcc = 0.f;

		if (OwnerPlayer->IsMyPlayer())
		{
			if (0 == CurrentBullet)
			{
				return;
			}

			--CurrentBullet;
			CurrentBullet = FMath::Max(0, CurrentBullet);
		}
	}

	if (BulletClass)
	{
		BulletActor = Cast<ARocket>(GetWorld()->SpawnActor(BulletClass));

		if (BulletActor)
		{
			BulletActor->SetOwner(GetOwner());
			BulletActor->SetActorLocation(SpawnLocation);
			BulletActor->SetDir(SpawnLocation, EndLocation);
			UGameplayStatics::SpawnEmitterAttached(MuzzleFlash, Mesh, TEXT("Rocket"));
			UGameplayStatics::SpawnSoundAttached(MuzzleSound, Mesh, TEXT("Rocket"));
		}
	}
}

void ARocketLauncher::SetAttackPacket(FVector StartLocation, FVector EndLocation)
{
	Protocol::C_ATTACK AttackPkt;

	if (ABasePlayer* Player = Cast<ABasePlayer>(GetOwner()))
	{
		AttackPkt.set_object_id(Player->GetPlayerInfo()->object_id());

	}

	else if (ABaseEnemy* Enemy = Cast<ABaseEnemy>(GetOwner()))
	{
		AttackPkt.set_object_id(Cast<ABaseEnemy>(GetOwner())->GetPosInfo()->object_id());
	}

	AttackPkt.set_start_x(StartLocation.X);
	AttackPkt.set_start_y(StartLocation.Y);
	AttackPkt.set_start_z(StartLocation.Z);

	AttackPkt.set_end_x(EndLocation.X);
	AttackPkt.set_end_y(EndLocation.Y);
	AttackPkt.set_end_z(EndLocation.Z);

	SEND_PACKET(AttackPkt);
}
