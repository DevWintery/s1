// Fill out your copyright notice in Description page of Project Settings.


#include "LineTraceGun.h"
#include "Engine/DamageEvents.h"
#include "Engine/SkeletalMeshSocket.h"
#include "Components/SkeletalMeshComponent.h"
#include "Protocol.pb.h"
#include "BasePlayer.h"
#include "S1Defines.h"
#include "BaseEnemy.h"
#include "UIManager.h"
#include "Particles/ParticleSystem.h"
#include "HeroPlayer.h"
#include "DestructibleActor.h"
#include "Bullet.h"
#include "Rocket.h"

ALineTraceGun::ALineTraceGun()
	: TraceMaxRange(5000.f), bIsFusillade(false), ShootDelayAcc(0.f)
{

}

void ALineTraceGun::BeginPlay()
{
	Super::BeginPlay();

	MaxBullet = 30;
	CurrentBullet = MaxBullet;
	TotalBullet = 200;
}

void ALineTraceGun::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

void ALineTraceGun::SetEquip(bool _bIsResult, USkeletalMeshComponent* _Mesh)
{
	if (true == _bIsResult)
	{
		const USkeletalMeshSocket* WeaponSocket = _Mesh->GetSocketByName("Skt_Rifle01");

		if (nullptr != WeaponSocket)
		{
			WeaponSocket->AttachActor(this, _Mesh);
		}
	}

	else
	{
		const USkeletalMeshSocket* WeaponSocket = _Mesh->GetSocketByName("WeaponEquipRifle");

		if (nullptr != WeaponSocket)
		{
			WeaponSocket->AttachActor(this, _Mesh);
		}
	}
}

void ALineTraceGun::PlayEffect(FVector StartLocation, FVector EndLocation)
{
	//DrawDebugPoint(GetWorld(), EndLocation, 10, FColor::Red, true);

	UGameplayStatics::SpawnEmitterAttached(MuzzleFlash, Mesh, TEXT("Muzzle"));
	UGameplayStatics::SpawnSoundAttached(MuzzleSound, Mesh, TEXT("Muzzle"));

}

void ALineTraceGun::SendAtackPKT()
{
	APawn* OwnerPawn = Cast<APawn>(GetOwner());

	FVector ShotDirection = FVector::ZeroVector;
	FVector StartLocation = FVector::ZeroVector;
	FVector EndLocation = FVector::ZeroVector;

	FRotator Rotation;

	if (Cast<ABasePlayer>(OwnerPawn)->IsMyPlayer())
	{
		AController* OwnerController = GetOwnerController();
		OwnerController->GetPlayerViewPoint(StartLocation, Rotation);
	}

	else
	{
		OwnerPawn->GetActorEyesViewPoint(StartLocation, Rotation);
	}

	ShotDirection = -Rotation.Vector();

	EndLocation = StartLocation + Rotation.Vector() * TraceMaxRange;

	FHitResult Hit;
	FCollisionQueryParams Params;

	Params.AddIgnoredActor(GetOwner());
	Params.AddIgnoredActor(this);

	while (GetWorld()->LineTraceSingleByChannel(Hit, StartLocation, EndLocation, ECollisionChannel::ECC_GameTraceChannel1, Params))
	{
		if (Hit.GetActor())
		{
			if (ABaseEnemy* Enemy = Cast<ABaseEnemy>(Hit.GetActor()))
			{
				if (Enemy->GetState() == Protocol::MONSTER_STATE_DIE)
				{
					Params.AddIgnoredActor(Enemy);
					continue;
				}
			}

			if (ABasePlayer* Player = Cast<ABasePlayer>(Hit.GetActor()))
			{
				if (Player->GetPlayerState() ==  EPlayerState::DIE)
				{
					Params.AddIgnoredActor(Player);
					continue;
				}
			}

			if ((StartLocation - Hit.Location).Length() < (StartLocation - OwnerPawn->GetActorLocation()).Length())
			{
				Params.AddIgnoredActor(Hit.GetActor());
				continue;
			}

			if (ADestructibleActor* DA = Cast<ADestructibleActor>(Hit.GetActor()))
			{
				Target = DA;
			}

			EndLocation = Hit.Location;
			break;
		}

		else
		{
		}
	}

	//UGameplayStatics::SpawnEmitterAttached(MuzzleFlash, Mesh, TEXT("Muzzle"));
	//UGameplayStatics::SpawnSoundAttached(MuzzleSound, Mesh, TEXT("Muzzle"));
	SetAttackPacket(GetMesh()->GetSocketLocation("Muzzle"), EndLocation);
}

void ALineTraceGun::Attack(FVector SpawnLocation, FVector EndLocation)
{
	if (0 == GetCurrentAmmo())
	{
		UGameplayStatics::SpawnSoundAttached(DrySound, Mesh, TEXT("Muzzle"));

		return;
	}

	else
	{
		ABasePlayer* OwnerPlayer = Cast<ABasePlayer>(GetOwner());

		ShootDelayAcc = 0.f;

		if (OwnerPlayer->IsMyPlayer())
		{
			if (0 == CurrentBullet)
			{
				return;
			}

			--CurrentBullet;
			CurrentBullet = FMath::Max(0, CurrentBullet);
		}
	}

	if (nullptr != BulletClass)
	{

		FActorSpawnParameters SpawnParams;
		SpawnParams.Owner = GetOwner();
		SpawnParams.Instigator = Cast<APawn>(GetOwner());
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		BulletActor = GetWorld()->SpawnActor<ABullet>(BulletClass, SpawnLocation, GetActorRotation(), SpawnParams);

		if (BulletActor)
		{
			BulletActor->SetShootOwner(Cast<ABaseCharacter>(GetOwner()));
			BulletActor->SetData(SpawnLocation, EndLocation);
			BulletActor->SetCompleteSpawn();

			UGameplayStatics::SpawnEmitterAttached(MuzzleFlash, Mesh, TEXT("Muzzle"));
			UGameplayStatics::SpawnSoundAttached(MuzzleSound, Mesh, TEXT("Muzzle"));
		}
	}
}

void ALineTraceGun::SetIsFusillade(bool _bIsFusillade)
{
	bIsFusillade = _bIsFusillade;
}


void ALineTraceGun::SetAttackPacket(FVector StartLocation, FVector EndLocation)
{
	Protocol::C_ATTACK AttackPkt;

	if (ABasePlayer* Player = Cast<ABasePlayer>(GetOwner()))
	{
		AttackPkt.set_object_id(Player->GetPlayerInfo()->object_id());

	}

	else if (ABaseEnemy* Enemy = Cast<ABaseEnemy>(GetOwner()))
	{
		AttackPkt.set_object_id(Cast<ABaseEnemy>(GetOwner())->GetPosInfo()->object_id());
	}

	AttackPkt.set_start_x(StartLocation.X);
	AttackPkt.set_start_y(StartLocation.Y);
	AttackPkt.set_start_z(StartLocation.Z);

	AttackPkt.set_end_x(EndLocation.X);
	AttackPkt.set_end_y(EndLocation.Y);
	AttackPkt.set_end_z(EndLocation.Z);

	SEND_PACKET(AttackPkt);
}
