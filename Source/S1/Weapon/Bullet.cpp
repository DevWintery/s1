// Fill out your copyright notice in the Description page of Project Settings.


#include "Bullet.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "BaseCharacter.h"
#include "BaseEnemy.h"
#include "S1Defines.h"
#include "Particles/ParticleSystem.h"
#include "Kismet/GameplayStatics.h"
#include "BasePlayer.h"
#include "DestructibleActor.h"
#include "UIManager.h"
#include "Math/UnrealMathUtility.h"
#include "DamageFloaterWidget.h"

// Sets default values
ABullet::ABullet()
	: Damage(10.f)
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Collision = CreateDefaultSubobject<USphereComponent>(TEXT("Collision"));
	SetRootComponent(Collision);

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(Collision);

	Movement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Movement"));
	Movement->SetUpdatedComponent(Collision);
	Movement->InitialSpeed = 3000.0f;
	Movement->MaxSpeed = 3000.0f;
	Movement->bRotationFollowsVelocity = true;
	Movement->bShouldBounce = false;
	Movement->ProjectileGravityScale = 0.f;

	BulletTrail = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("TrailParticle"));
	BulletTrail->SetupAttachment(Mesh);
}

void ABullet::Init()
{
	//��� ����ϸ� �ȵ�
	//�ƹ����� ��������
}

// Called when the game starts or when spawned
void ABullet::BeginPlay()
{
	Super::BeginPlay();

	Collision->OnComponentBeginOverlap.AddDynamic(this, &ABullet::OnOverlapBegin);
}


// Called every frame
void ABullet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector CurrentLocation = GetActorLocation();

	if ((EndLocation - CurrentLocation).Length() < 100.f && ECollisionEnabled::NoCollision != Collision->GetCollisionEnabled())
	{
		if (ADestructibleActor* DA = Cast<ADestructibleActor>(Target))
		{
			DA->TakeDamaged();
		}

		Destroy();

		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactEffect, EndLocation, (ShootDirection.Rotation() * -1));
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ImpactSound, EndLocation);
	}
}

void ABullet::SetData(FVector Start, FVector End)
{
	EndLocation = End;
	ShootDirection = (End - Start);
	ShootDirection.Normalize();
	Movement->Velocity = ShootDirection * Movement->InitialSpeed;

	BulletTrail = UGameplayStatics::SpawnEmitterAttached(
	BulletTrailEffect, 
	Collision);

	BulletTrail->SetRelativeScale3D(FVector(0.5f, 0.5f, 0.5f));
}

void ABullet::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (!bIsCompleteSpawn)
	{
		return;
	}

	if (!IsActorInitialized())
	{
		return;
	}

	if (nullptr != Target)
	{
		return;
	}

	else if (nullptr != GetOwner() && (OtherActor == GetOwner() || Cast<ABaseCharacter>(OtherActor) == ShootOwner || GetOwner()->GetClass() == OtherActor->GetClass()))
	{
		return;
	}

	else
	{
		if (ABaseEnemy* Enemy = Cast<ABaseEnemy>(OtherActor))
		{
			if (Enemy->GetState() != Protocol::MONSTER_STATE_DIE)
			{
				SendHitPkt(Damage, Enemy->GetPosInfo()->object_id(), true);
				Target = nullptr;
			}
		}

		else if (ABasePlayer* Player = Cast<ABasePlayer>(OtherActor))
		{
			if (Player == GetOwner())
			{
				return;
			}

			if (Player->GetPlayerState() != EPlayerState::DIE && Player->GetCanDamaged() == true)
			{
				SendHitPkt(Damage, Player->GetPlayerInfo()->object_id(), false);
				Target = nullptr;
			}
		}

		else
		{
			if (ABasePlayer* OwnerCharacter = Cast<ABasePlayer>(GetOwner()))
			{
				if (ADestructibleActor* DA = Cast<ADestructibleActor>(OtherActor))
				{
					if (DA->GetRange() == OtherComp)
					{
						return;
					}
					else
					{
						//GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Blue, TEXT("Target is Da"));
						Target = DA;
					}
				}
			}
		}
	}
}

void ABullet::SendHitPkt(int CurrentDamage, int ObjectId, bool bSpawnableDamageFloater)
{
	if (bSpawnableDamageFloater)
	{
		if (ABasePlayer* BasePlayer = Cast<ABasePlayer>(ShootOwner))
		{
			if (BasePlayer->IsMyPlayer())
			{
				UUIManager* UIManager = GetGameInstance()->GetSubsystem<UUIManager>();
				int64 RandNum = FMath::RandRange(100000, 999999);
				UIManager->CreateUIWidget(EUIType::DAMAGEFLOTER);

				if (UDamageFloaterWidget* DF = Cast<UDamageFloaterWidget>(UIManager->GetUI(EUIType::DAMAGEFLOTER)))
				{
					DF->SetDamageFloater(RandNum, RandNum > 500000);
				}
			}
		}
	}

	Protocol::C_HIT Hitpkt;

	Hitpkt.set_damage(CurrentDamage);
	Hitpkt.set_object_id(ObjectId);
	SEND_PACKET(Hitpkt);
	Collision->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	if (BulletTrail)
	{
		BulletTrail->DestroyComponent();
		BulletTrail = nullptr;
	}

	Destroy();

}
