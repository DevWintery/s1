// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon/BaseWeapon.h"
#include "Punch.generated.h"

/**
 *
 */
UCLASS()
class S1_API APunch : public ABaseWeapon
{
	GENERATED_BODY()

public:
	APunch();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	virtual void SetEquip(bool _bIsResult, class USkeletalMeshComponent* _Mesh);

	virtual void PlayEffect(FVector StartLocation = FVector::ZeroVector, FVector EndLocation = FVector::ZeroVector);

	virtual void SendAtackPKT();

	virtual void Attack(FVector StartLocation = FVector::ZeroVector, FVector EndLocation = FVector::ZeroVector);

	virtual void SetAttackPacket(FVector StartLocation, FVector EndLocation) override;

public:
	void SetAttackAble(bool bCanAttack);

private:
	UFUNCTION()
	void OnTarget(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

private:
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true))
	class USphereComponent* Range;

	class ABaseCharacter* Target;

private:
	bool bCanOverlapCheck = false;

};
