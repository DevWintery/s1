// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseActor.h"
#include "Bullet.generated.h"

UCLASS()
class S1_API ABullet : public ABaseActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABullet();

protected:
	virtual void Init() override;
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	void SetData(FVector Start, FVector End);
	void SetShootOwner(class ABaseCharacter* Character) { ShootOwner = Character; }
	void SetCompleteSpawn() { bIsCompleteSpawn = true;}
	void SetTarget(ABaseActor* Actor);
	void SetDamage();

	UFUNCTION()
	void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

private:
	void SendHitPkt(int CurrentDamage, int ObjectId, bool bSpawnableDamageFloater);
	
private:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = true))
	class UStaticMeshComponent* Mesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = true))
	class USphereComponent* Collision;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = true))
	class UProjectileMovementComponent* Movement;


	UPROPERTY(EditAnywhere)
	class USoundBase* ImpactSound;

	UPROPERTY(EditAnywhere)
	class UParticleSystem* ImpactEffect;

	UPROPERTY(EditAnywhere)
	class UParticleSystem* BulletTrailEffect;

	UPROPERTY(EditAnywhere)
	class UParticleSystemComponent* BulletTrail;

private:
	FVector StartLocation;
	FVector EndLocation;
	FVector ShootDirection;

private:
	class ABaseCharacter* ShootOwner;
	float Damage;
	AActor* Target;
	bool bIsCompleteSpawn = false;
};
