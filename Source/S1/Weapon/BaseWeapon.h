// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseWeapon.generated.h"

UCLASS()
class S1_API ABaseWeapon : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABaseWeapon();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	virtual void SetEquip(bool _bIsResult, class USkeletalMeshComponent* _Mesh);
	virtual void PlayEffect(FVector StartLocation = FVector::ZeroVector, FVector EndLocation = FVector::ZeroVector);
	virtual void SendAtackPKT();
	virtual void Attack(FVector StartLocation = FVector::ZeroVector, FVector EndLocation = FVector::ZeroVector);
	virtual void SetAttackPacket(FVector StartLocation = FVector::ZeroVector, FVector EndLocation = FVector::ZeroVector);

public: /*Get Function*/
	FORCEINLINE virtual int GetCurrentAmmo() { return CurrentBullet; }
	void SetCurrentAmmo(int Num) { CurrentBullet = Num; }

	FORCEINLINE virtual int GetToTalAmmo() { return TotalBullet; }
	void SetTotalAmmo(int Num) { TotalBullet = Num; }

	FORCEINLINE virtual int GetMaxAmmo() { return MaxBullet; }

	virtual class AController* GetOwnerController();
	void Reload();

public:
	class USkeletalMeshComponent* GetMesh() const;

protected:
	UPROPERTY(VisibleAnywhere)
	class USceneComponent* Root;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Mesh, meta = (AllowPrivateAccess = true))
	class USkeletalMeshComponent* Mesh;

protected:
	UPROPERTY(EditAnywhere)
	float Damage;

protected:
	// ���� �ִ� ź��
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Info, meta = (AllowPrivateAccess = true))
	int MaxBullet;

	// ���� ź��
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Info, meta = (AllowPrivateAccess = true))
	int CurrentBullet;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Info, meta = (AllowPrivateAccess = true))
	int TotalBullet;
};
