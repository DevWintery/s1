// Fill out your copyright notice in the Description page of Project Settings.


#include "Grenade.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"
#include "HeroPlayer.h"
#include "Engine/SkeletalMeshSocket.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/PrimitiveComponent.h"
#include "Kismet/KismetMathLibrary.h"

AGrenade::AGrenade()
{
	Movement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));

	Collision = CreateDefaultSubobject<USphereComponent>(TEXT("Collision"));

	Collision->SetupAttachment(RootComponent);

	Mesh->SetupAttachment(Collision);

}

void AGrenade::BeginPlay()
{
	Super::BeginPlay();

	//Movement->Velocity = Velocity;
	//Movement->bShouldBounce = true;
}

void AGrenade::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	// 액터의 변환 얻기
	FTransform ownerTransform = GetOwner()->GetActorTransform();

	// 소켓의 상대 위치 얻기
	FVector socketWorldLocation = Cast<AHeroPlayer>(GetOwner())->GetMesh()->GetSocketLocation("weapon_r"); // "SocketName"은 사용하려는 소켓의 이름입니다.

	// 월드 위치 계산
	//FVector SocketWorldLocation = ActorTransform.TransformPosition(SocketRelativeLocation);

	RootComponent->SetWorldLocation(socketWorldLocation);
}


void AGrenade::SetEquip(bool _bIsResult, USkeletalMeshComponent* _Mesh)
{
	if (_bIsResult)
	{
		const USkeletalMeshSocket* weaponSocket = _Mesh->GetSocketByName("weapon_r");

		if (nullptr != weaponSocket)
		{
			weaponSocket->AttachActor(this, _Mesh);
		}
	}

	else
	{
		//const USkeletalMeshSocket* weaponSocket = _Mesh->GetSocketByName("WeaponEquipSocket");

		//if (nullptr != weaponSocket)
		//{
		//	weaponSocket->AttachActor(this, _Mesh);
		//}
	}
}

void AGrenade::SendAtackPKT()
{
}


void AGrenade::SetAttackPacket(FVector StartLocation, FVector EndLocation)
{
}

void AGrenade::Attack(FVector SpawnLocation, FVector EndLocation)
{
}
