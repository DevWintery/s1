// Fill out your copyright notice in the Description page of Project Settings.


#include "Managers/DataManager.h"
#include "Engine/DataTable.h"
#include "ItemData.h"
#include "SummaryOfStage.h"
#include "ErrorData.h"
#include "UserData.h"
#include "CurrentEquipmentData.h"
#include "RewardData.h"

UDataManager::UDataManager()
{
	static ConstructorHelpers::FObjectFinder<UDataTable> ITEMDATA(TEXT("/Game/S1/DataTables/DT_ItemData.DT_ItemData"));

	if (ITEMDATA.Succeeded())
	{
		ItemDT = ITEMDATA.Object;
	}

	static ConstructorHelpers::FObjectFinder<UDataTable> INVENTORYDT(TEXT("/Game/S1/DataTables/DT_InventoryData.DT_InventoryData"));

	if (INVENTORYDT.Succeeded())
	{
		InventoryDT = INVENTORYDT.Object;
	}

	static ConstructorHelpers::FObjectFinder<UDataTable> SUMMARY(TEXT("/Game/S1/DataTables/DT_SummaryOfStage.DT_SummaryOfStage"));

	if (SUMMARY.Succeeded())
	{
		SummaryOfStageDT = SUMMARY.Object;
	}

	static ConstructorHelpers::FObjectFinder<UDataTable> ERROR(TEXT("/Game/S1/DataTables/DT_Error.DT_Error"));

	if (ERROR.Succeeded())
	{
		ErrorDT = ERROR.Object;
	}

	static ConstructorHelpers::FObjectFinder<UDataTable> PLAYER(TEXT("/Game/S1/DataTables/DT_PlayerData.DT_PlayerData"));

	if (PLAYER.Succeeded())
	{
		PlayerDT = PLAYER.Object;
	}

	static ConstructorHelpers::FObjectFinder<UDataTable> EQUIP(TEXT("/Game/S1/DataTables/DT_CurrnetEquipment.DT_CurrnetEquipment"));

	if (EQUIP.Succeeded())
	{
		CurrentEquipmentDT = EQUIP.Object;
	}

	static ConstructorHelpers::FObjectFinder<UDataTable> REWARD(TEXT("/Game/S1/DataTables/DT_Reward.DT_Reward"));

	if (REWARD.Succeeded())
	{
		RewardDT = REWARD.Object;
	}
}

void UDataManager::Initialize(FSubsystemCollectionBase& _Collection)
{
	Super::Initialize(_Collection);
}

void UDataManager::Deinitialize()
{
	Super::Deinitialize();
}

FItemData* UDataManager::GetItemData(int ItemID)
{
	FString ItemStrID = FString::FromInt(ItemID);

	FItemData* ItemData = ItemDT->FindRow<FItemData>(FName(*ItemStrID), FString(""));

	if (ItemData)
	{
		return ItemData;
	}

	else
	{
		return nullptr;
	}
}

TArray<FName> UDataManager::GetAllItemData()
{
	TArray<FName> AllItemID = ItemDT->GetRowNames();

	return AllItemID;
}

FString UDataManager::GetSummaryOfStage(FString Stage)
{
	FSummaryOfStage* SummaryData = SummaryOfStageDT->FindRow<FSummaryOfStage>(FName(*Stage), FString(""));

	FText MultiCheck = FText::FromString(SummaryData->SummaryOfStage);

	if (SummaryData)
	{
		return MultiCheck.ToString();
	}

	else
	{
		return "";
	}
}

FString UDataManager::GetErrorMessage(int ErrorID)
{
	FString Error = FString::FromInt(ErrorID);

	FErrorData* ErrorData = ErrorDT->FindRow<FErrorData>(FName(*Error), FString(""));

	if (ErrorData)
	{
		return ErrorData->ErrorMessage;
	}

	else
	{
		return "";
	}
}

FPlayerData* UDataManager::GetPlayerData()
{
	FString PlayerID = FString::FromInt(0);

	FPlayerData* PlayerData = PlayerDT->FindRow<FPlayerData>(FName(*PlayerID), FString(""));

	if (PlayerData)
	{
		return PlayerData;
	}
	
	return nullptr;
}

int UDataManager::GetCurrentGold()
{
	FString PlayerID = FString::FromInt(0);

	FPlayerData* PlayerData = PlayerDT->FindRow<FPlayerData>(FName(*PlayerID), FString(""));

	if (PlayerData)
	{
		return PlayerData->Gold;
	}

	else
	{
		return 0;
	}
}

int UDataManager::GetCurrentSp()
{
	FString PlayerID = FString::FromInt(0);

	FPlayerData* PlayerData = PlayerDT->FindRow<FPlayerData>(FName(*PlayerID), FString(""));

	if (PlayerData)
	{
		return PlayerData->Sp;
	}

	else
	{
		return 0;
	}
}

FCurrentEquipmentData* UDataManager::GetCurrentEquipItem()
{
	if (FCurrentEquipmentData* EquipmentData = CurrentEquipmentDT->FindRow<FCurrentEquipmentData>(FName(*FString::FromInt(0)), FString("")))
	{
		return EquipmentData;
	}

	else
	{
		return nullptr;
	}
}

FRewardData* UDataManager::GetRewardData(FString RowName)
{
	if (FRewardData* RewardData = RewardDT->FindRow<FRewardData>(FName(*RowName), FString("")))
	{
		return RewardData;
	}

	else
	{
		return nullptr;
	}

}


void UDataManager::SetCurrentGold(int Gold)
{
	FString PlayerID = FString::FromInt(0);

	FPlayerData* PlayerData = PlayerDT->FindRow<FPlayerData>(FName(*PlayerID), FString(""));

	FPlayerData NewData;
	NewData.Gold = Gold;
	NewData.Sp = PlayerData->Sp;

	PlayerDT->AddRow(FName(*PlayerID), NewData);
}

void UDataManager::SetCurrentSp(int Sp)
{
	FString PlayerID = FString::FromInt(0);

	FPlayerData* PlayerData = PlayerDT->FindRow<FPlayerData>(FName(*PlayerID), FString(""));

	FPlayerData NewData;
	NewData.Gold = PlayerData->Gold;
	NewData.Sp =  Sp;

	PlayerDT->AddRow(FName(*PlayerID), NewData);
}

void UDataManager::SetEquipment(ESkeletalMeshParts Parts, int Id)
{
	if (FCurrentEquipmentData* EquipmentData = CurrentEquipmentDT->FindRow<FCurrentEquipmentData>(FName(*FString::FromInt(0)), FString("")))
	{
		FCurrentEquipmentData NewData = (*EquipmentData);

		switch (Parts)
		{
		case ESkeletalMeshParts::NONE:
			break;
		case ESkeletalMeshParts::HELMET:
		{
			NewData.HelmetID = Id;
		}
		break;
		case ESkeletalMeshParts::AMOR:
		{
			NewData.AmorID = Id;
		}
		break;
		case ESkeletalMeshParts::BELTS:
		{
			NewData.BeltsID = Id;
		}
		break;
		case ESkeletalMeshParts::BOOTS:
		{
			NewData.BootsID = Id;
		}
		break;
		case ESkeletalMeshParts::GLASSES:
		{
			NewData.GlassesID = Id;
		}
		break;
		case ESkeletalMeshParts::GLOVES:
		{
			NewData.GlovesID = Id;
		}
		break;
		case ESkeletalMeshParts::JACKET:
		{
			NewData.JacketID = Id;
		}
		break;
		case ESkeletalMeshParts::PANTS:
		{
			NewData.PantsID = Id;
		}
		break;
		case ESkeletalMeshParts::MASK:
		{
			NewData.MaskID = Id;
		}
		break;
		case ESkeletalMeshParts::NVD:
		{
			NewData.NvdID = Id;
		}
		break;
		case ESkeletalMeshParts::RADIO:
		{
			NewData.RadioID = Id;
		}
		break;
		default:
			break;
		}

		CurrentEquipmentDT->AddRow(FName(*FString::FromInt(0)), NewData);
	}
}
