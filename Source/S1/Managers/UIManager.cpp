// Fill out your copyright notice in the Description page of Project Settings.


#include "UIManager.h"
#include "DamageFloaterWidget.h"
#include "Blueprint/UserWidget.h"
#include "Components/WidgetComponent.h"
#include "Kismet/GameplayStatics.h"

#include "BaseCharacter.h"
#include "GoalIconWidget.h"
#include "QuestManager.h"
#include "HeroHUDWidget.h"
#include "SpectatorWidget.h"
#include "BasePlayer.h"
#include "ClearWidget.h"
#include "GameOverWidget.h"
#include "GameEndWidget.h"
#include "ErrorWidget.h"
#include "ItemSummaryWidget.h"

UUIManager::UUIManager()
{
	static ConstructorHelpers::FClassFinder<UDamageFloaterWidget> DF_WIDGET(TEXT("/Game/S1/UI/InGame/Player/WBP_DamageFloater.WBP_DamageFloater_C"));

	if (DF_WIDGET.Succeeded())
	{
		DamageFloaterClass = DF_WIDGET.Class;
	}

	static ConstructorHelpers::FClassFinder<UGoalIconWidget> GI_WIDGET(TEXT("/Game/S1/UI/InGame/Etc/WBP_GoalIcon.WBP_GoalIcon_C"));

	if (GI_WIDGET.Succeeded())
	{
		GoalIconClass = GI_WIDGET.Class;
	}

	static ConstructorHelpers::FClassFinder<USpectatorWidget> SPECTATOR(TEXT("/Game/S1/UI/InGame/Player/WBP_Spetator.WBP_Spetator_C"));

	if (SPECTATOR.Succeeded())
	{
		SpectatorClass = SPECTATOR.Class;
	}

	static ConstructorHelpers::FClassFinder<UClearWidget> CLEAR(TEXT("/Game/S1/UI/InGame/Player/WBP_Clear.WBP_Clear_C"));

	if (CLEAR.Succeeded())
	{
		ClearClass = CLEAR.Class;
	}

	static ConstructorHelpers::FClassFinder<UGameOverWidget> GAMEOVER(TEXT("/Game/S1/UI/InGame/Player/WBP_GameOver.WBP_GameOver_C"));

	if (GAMEOVER.Succeeded())
	{
		GameOverClass = GAMEOVER.Class;
	}

	static ConstructorHelpers::FClassFinder<UGameEndWidget> GAMEEND(TEXT("/Game/S1/UI/InGame/Etc/WBP_GameEnd.WBP_GameEnd_C"));

	if (GAMEEND.Succeeded())
	{
		GameEndClass = GAMEEND.Class;
	}

	static ConstructorHelpers::FClassFinder<UErrorWidget> ERROR(TEXT("/Game/S1/UI/Lobby/PopUP/WBP_Error.WBP_Error_C"));

	if (ERROR.Succeeded())
	{
		ErrorMsgClass = ERROR.Class;
	}

	static ConstructorHelpers::FClassFinder<UItemSummaryWidget> SUMMARYOFITEM(TEXT("/Game/S1/UI/Lobby/PopUP/BP_ItemSummaryWidget.BP_ItemSummaryWidget_C"));

	if (SUMMARYOFITEM.Succeeded())
	{
		ItemSummaryClass = SUMMARYOFITEM.Class;
	}
}

void UUIManager::Initialize(FSubsystemCollectionBase& _Collection)
{
	Super::Initialize(_Collection);
}

void UUIManager::Deinitialize()
{
	Super::Deinitialize();
}

void UUIManager::ShowUI(EUIType UIType)
{	
	for (const auto UI : UIList)
	{
		if (UIType == UI.Key)
		{
			UI.Value->AddToViewport();
		}
	}
}

void UUIManager::HideUI(EUIType UIType)
{
	for (const auto UI : UIList)
	{
		if (UIType == UI.Key)
		{
			UI.Value->RemoveFromParent();
		}
	}
}

UUserWidget* UUIManager::GetUI(EUIType UIType)
{
	for (const auto UI : UIList)
	{
		if (UIType == UI.Key)
		{
			return UI.Value;
		}
	}

	return nullptr;
}

void UUIManager::CreateUIWidget(EUIType UIType)
{
	switch (UIType)
	{
	case EUIType::CLEAR:
	{
		if (ClearClass)
		{
			ClearWidget = CreateWidget(UGameplayStatics::GetPlayerController(GetWorld(), 0), ClearClass);

			UIList.Emplace(UIType, ClearWidget);
		}

		if (ClearWidget)
		{
			ClearWidget->AddToViewport();
		}
	}
		break;

	case EUIType::ERROR:
	{
		if (ErrorMsgClass)
		{
			ErrorMsgWidget = CreateWidget(UGameplayStatics::GetPlayerController(GetWorld(), 0), ErrorMsgClass);

			UIList.Emplace(UIType, ErrorMsgWidget);
		}

		if (ErrorMsgWidget)
		{
			ErrorMsgWidget->AddToViewport();
		}
	}
	break;

	case EUIType::GAMECLEAR:
	{
		if (GameEndClass)
		{
			GameEndWidget = CreateWidget(UGameplayStatics::GetPlayerController(GetWorld(), 0), GameEndClass);

			UIList.Emplace(UIType, GameEndWidget);
		}

		if (GameEndWidget)
		{
			GameEndWidget->AddToViewport();
		}
	}
	break;

	case EUIType::DAMAGEFLOTER:
	{
		if (DamageFloaterClass)
		{
			DamageFloaterWidget = CreateWidget(UGameplayStatics::GetPlayerController(GetWorld(), 0), DamageFloaterClass);

			UIList.Emplace(UIType, DamageFloaterWidget);
		}

		if (DamageFloaterWidget)
		{
			DamageFloaterWidget->AddToViewport();
		}
	}
		break;
	case EUIType::GAMEOVER:
	{
		if (GameOverClass)
		{
			GameOverWidget = CreateWidget(UGameplayStatics::GetPlayerController(GetWorld(), 0), GameOverClass);
			UIList.Emplace(UIType, GameOverWidget);
		}

		if (GameOverWidget)
		{
			GameOverWidget->AddToViewport();
			//Cast<UGameOverWidget>(GameEndWidget)->SetScreen();
		}
	}
		break;

	case EUIType::SPECTATOR:
	{
		if (SpectatorClass)
		{
			SpectatorWidget = CreateWidget(UGameplayStatics::GetPlayerController(GetWorld(), 0), SpectatorClass);
			UIList.Emplace(UIType, SpectatorWidget);
		}

		if (SpectatorWidget)
		{
			SpectatorWidget->AddToViewport();
		}
	}
		break;

	case EUIType::GOALICON:
	{
		if (GoalIconClass)
		{
			GoalIconWidget = CreateWidget(UGameplayStatics::GetPlayerController(GetWorld(), 0), GoalIconClass);
			UIList.Emplace(UIType, GoalIconWidget);
		}

		if (GoalIconWidget)
		{
			GoalIconWidget->AddToViewport();
		}
	}
		break;

	case EUIType::ITEMSUMMARY:
	{
		if (ItemSummaryClass)
		{
			ItemSummaryWidget = CreateWidget(UGameplayStatics::GetPlayerController(GetWorld(), 0), ItemSummaryClass);
			UIList.Emplace(UIType, ItemSummaryWidget);
		}

		if (ItemSummaryWidget)
		{
			ItemSummaryWidget->AddToViewport(8);
		}
	}
	break;
	
	default:
		break;
	}
}

void UUIManager::RemoveGoalIcon()
{
	if (GoalIconWidget && GoalIconWidget->IsInViewport())
	{
		GoalIconWidget->RemoveFromParent();
	}
}

void UUIManager::SetGoalIconState(bool bIsInViewport)
{
	if (GoalIconWidget)
	{
		if (bIsInViewport)
		{
			GoalIconWidget->SetVisibility(ESlateVisibility::Collapsed);

		}

		else
		{
			GoalIconWidget->SetVisibility(ESlateVisibility::HitTestInvisible);
		}
	}
}

void UUIManager::SetGoalIconTarget(ABasePlayer* Target)
{
	if (nullptr != GoalIconWidget)
	{
		Cast<UGoalIconWidget>(GoalIconWidget)->SetTarget(Target);
	}
}

void UUIManager::SetItemSummary(FString ItemSummary, FName ItemName, ETypeOfStat TypeOfStat, int Stat, FVector2D Location)
{
	if (nullptr != ItemSummaryWidget)
	{
		FString Type;

		switch (TypeOfStat)
		{
		case ETypeOfStat::NONE:
			break;
		case ETypeOfStat::OFFENCT:
		{
			Type = TEXT("Att");
		}
			break;
		case ETypeOfStat::DEFENCE:
		{
			Type = TEXT("Def");
		}
			break;
		case ETypeOfStat::AMMO:
		{
			Type = TEXT("Ammo");
		}
			break;
		case ETypeOfStat::HP:
		{
			Type = TEXT("Hp");
		}
			break;
		default:
			break;
		}

		Cast<UItemSummaryWidget>(ItemSummaryWidget)->SetSummaryText(ItemSummary, ItemName, Type, Stat, Location);
	}
}

//UWidgetComponent* UUIManager::CreateWidgetComponent(ABaseCharacter* Parents)
//{
//	UWidgetComponent* Widget = NewObject<UWidgetComponent>(Parents, UWidgetComponent::StaticClass(), TEXT("HpBarWidget"));
//
//	if (Widget)
//	{
//		Widget->RegisterComponent();
//
//		Widget->AttachToComponent(Parents->GetMesh(), FAttachmentTransformRules::KeepRelativeTransform);
//
//		Widget->CreationMethod = EComponentCreationMethod::Instance;
//
//		Widget->SetWidgetClass(LoadClass<UUserWidget>(nullptr, TEXT("/Game/S1/UI/InGame/Enemy/WBP_EnemyHpBar.WBP_EnemyHpBar_C")));
//
//	}
//
//	return Widget;
//}