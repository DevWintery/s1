// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "ParticleManager.generated.h"

/**
 * 
 */
UCLASS()
class S1_API UParticleManager : public UGameInstanceSubsystem
{
	GENERATED_BODY()

public:
	UParticleManager();


public:
	virtual void Initialize(FSubsystemCollectionBase& _Collection) override;
	virtual void Deinitialize() override;
	
};
