// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "QuestManager.generated.h"

/**
 *
 */
UCLASS()
class S1_API UQuestManager : public UGameInstanceSubsystem
{
	GENERATED_BODY()

public:
	UQuestManager();

public:
	void SetQuestData(int QuestKey);
	void SetGoalActor();

public:
	FORCEINLINE FVector GetLocation() const { return GoalLocation; }
	FORCEINLINE FString GetSummary() const { return QuestSummary; }
	FORCEINLINE bool GetPlayTimer() const { return bPlayingTimer; }
	FORCEINLINE bool GetPlayCine() const { return bPlayingCine; }
	FORCEINLINE FString GetNameOfCine() const { return NameOfCine; }
	FORCEINLINE float GetTimerDelay() const { return TimerDelay; }
	FORCEINLINE bool GetIsClearStep() const{return bIsClearStep;}
	FORCEINLINE bool GetIsGameClear() const { return bIsGameClear; }

private:
	UPROPERTY(EditAnywhere)
	class UDataTable* QuestData;

	FString QuestSummary;
	FVector GoalLocation;
	bool bPlayingTimer;
	float TimerDelay;
	bool bPlayingCine;
	FString NameOfCine;
	bool bIsClearStep;
	bool bIsGameClear;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	TSubclassOf<class AGoalActor> GoalActorClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	class AGoalActor* GoalActor;



};
