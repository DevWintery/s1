// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "S1Defines.h"
#include "UIManager.generated.h"

/**
 *
 */
UCLASS()
class S1_API UUIManager : public UGameInstanceSubsystem
{
	GENERATED_BODY()

public:
	UUIManager();

public:
	virtual void Initialize(FSubsystemCollectionBase& _Collection) override;

	virtual void Deinitialize() override;

public:
	void CreateUIWidget(EUIType UIType);

public:
	void ShowUI(EUIType UIType);
	void HideUI(EUIType UIType);

public:
	class UUserWidget* GetUI(EUIType UIType);

public:
	void RemoveGoalIcon();

public:
	void SetGoalIconState(bool bIsInViewport);
	void SetGoalIconTarget(class ABasePlayer* Target);
	void SetItemSummary(FString ItemSummary, FName ItemName, ETypeOfStat TypeOfStat, int Stat, FVector2D Location);

private:
	UPROPERTY(EditAnywhere)
	TSubclassOf<class UUserWidget> DamageFloaterClass;

	UPROPERTY(EditAnywhere)
	class UUserWidget* DamageFloaterWidget;

	UPROPERTY(EditAnywhere)
	TSubclassOf<class UUserWidget> GoalIconClass;

	UPROPERTY(EditAnywhere)
	class UUserWidget* GoalIconWidget;

	UPROPERTY(EditAnywhere)
	TSubclassOf<class UUserWidget> GameOverClass;

	UPROPERTY(EditAnywhere)
	class UUserWidget* GameOverWidget;

	UPROPERTY(EditAnywhere)
	TSubclassOf<class UUserWidget> SpectatorClass;
	
	UPROPERTY(EditAnywhere)
	class UUserWidget* SpectatorWidget;

	UPROPERTY(EditAnywhere)
	TSubclassOf<class UUserWidget> ClearClass;

	UPROPERTY(EditAnywhere)
	class UUserWidget* ClearWidget;

	UPROPERTY(EditAnywhere)
	TSubclassOf<class UUserWidget> GameEndClass;

	UPROPERTY(EditAnywhere)
	class UUserWidget* GameEndWidget;


	UPROPERTY(EditAnywhere)
	TSubclassOf<class UUserWidget> ErrorMsgClass;

	UPROPERTY(EditAnywhere)
	class UUserWidget* ErrorMsgWidget;


	UPROPERTY(EditAnywhere)
	TSubclassOf<class UUserWidget> ItemSummaryClass;

	UPROPERTY(EditAnywhere)
	class UUserWidget* ItemSummaryWidget;

private:
	UPROPERTY(EditAnywhere)
	class UWidgetComponent* PlayerInfoWidget;

private:
	TMap<EUIType, class UUserWidget*> UIList;
	
};


