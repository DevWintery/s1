// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "S1Defines.h"
#include "DataManager.generated.h"

/**
 * 
 */
UCLASS()
class S1_API UDataManager : public UGameInstanceSubsystem
{
	GENERATED_BODY()

public:
	UDataManager();

public:
	virtual void Initialize(FSubsystemCollectionBase& _Collection) override;

	virtual void Deinitialize() override;

public:
	struct FItemData* GetItemData(int ItemID);
	TArray<FName> GetAllItemData();
	FString GetSummaryOfStage(FString Stage);
	FString GetErrorMessage(int ErrorID);
	struct FPlayerData* GetPlayerData();
	int GetCurrentGold();
	int GetCurrentSp();
	struct FCurrentEquipmentData* GetCurrentEquipItem();
	struct FRewardData* GetRewardData(FString RowName);

public:
	void SetCurrentGold(int Gold);
	void SetCurrentSp(int Sp);
	void SetEquipment(ESkeletalMeshParts Parts, int Id);

private:
	class UDataTable* ItemDT;
	class UDataTable* InventoryDT;
	class UDataTable* SummaryOfStageDT;
	class UDataTable* ErrorDT;
	class UDataTable* PlayerDT;
	class UDataTable* CurrentEquipmentDT;
	class UDataTable* RewardDT;
};
