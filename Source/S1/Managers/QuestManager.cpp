// Fill out your copyright notice in the Description page of Project Settings.


#include "Managers/QuestManager.h"
#include "Kismet/GameplayStatics.h"
#include "Blueprint/WidgetLayoutLibrary.h"
#include "QuestData.h"
#include "Engine/DataTable.h"
#include "Components/WidgetComponent.h"
#include "GoalActor.h"
#include "UIManager.h"
#include "HeroController.h"
#include "S1Defines.h"
#include "GoalIconWidget.h"

UQuestManager::UQuestManager()
{
	static ConstructorHelpers::FObjectFinder<UDataTable> QUESTDATA(TEXT("/Game/S1/DataTables/DT_QuestData.DT_QuestData"));

	if (QUESTDATA.Succeeded())
	{
		QuestData = QUESTDATA.Object;
	}

	static ConstructorHelpers::FClassFinder<AGoalActor> GOALACTOR(TEXT("/Game/S1/Blueprints/Content/Object/BP_GoalActor.BP_GoalActor_C"));

	if (GOALACTOR.Succeeded())
	{
		GoalActorClass = GOALACTOR.Class;
	}
}

void UQuestManager::SetQuestData(int QuestKey)
{
	FString QuestStep = UGameplayStatics::GetCurrentLevelName(GetWorld());
	QuestStep += FString::FromInt(QuestKey);

	FQuestData* CurrentQuest = QuestData->FindRow<FQuestData>(FName(*QuestStep), FString(""));

	if (CurrentQuest)
	{
		QuestSummary = CurrentQuest->QuestSummary;
		GoalLocation = CurrentQuest->GoalLocation;
		bPlayingTimer = CurrentQuest->bPlayingTimer;
		TimerDelay = CurrentQuest->TimerDelay;
		bPlayingCine = CurrentQuest->bPlayingCine;
		NameOfCine = CurrentQuest->NameOfCine;
		bIsClearStep = CurrentQuest->bIsClearStep;
		bIsGameClear = CurrentQuest->bIsGameClear;

		SetGoalActor();
	}
}

void UQuestManager::SetGoalActor()
{
	if(GoalActor)
	{
		GoalActor->SetLocation(GoalLocation);
	}

	else
	{
		if (GoalActorClass)
		{
			GoalActor = Cast<AGoalActor>(GetWorld()->SpawnActor(GoalActorClass));
			GoalActor->SetLocation(GoalLocation);
		}
	}

	UGameInstance* GameInstace = GetGameInstance();
	UUIManager* UIManager = GameInstace->GetSubsystem<UUIManager>();

	if (nullptr == Cast<UGoalIconWidget>(UIManager->GetUI(EUIType::GOALICON)))
	{
		UIManager->CreateUIWidget(EUIType::GOALICON);
	
	}
	else
	{
		UGoalIconWidget* GoalIcon = Cast<UGoalIconWidget>(UIManager->GetUI(EUIType::GOALICON));
		UIManager->ShowUI(EUIType::GOALICON);
		GoalIcon->SetGoalLocation(GoalLocation);
	}
}
