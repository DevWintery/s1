// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseCharacter.h"
#include "S1Defines.h"
#include "BasePlayer.generated.h"

UCLASS()
class S1_API ABasePlayer : public ABaseCharacter
{
	GENERATED_BODY()

public:
	ABasePlayer();
	virtual ~ABasePlayer();

protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

public:
	virtual void Attack(FVector StartLocation, FVector EndLocation);
	virtual void TakeDamaged(int Damage) override;

public:
	void SetCharacterMoveSpeed(const float MoveSpeed);

public:
	bool IsMyPlayer();

public:
	float GetAngle();

public:
	void SetPlayerInfo(const Protocol::PosInfo& Info);
	void SetDestInfo(const Protocol::PosInfo& Info);
	Protocol::PosInfo* GetPlayerInfo() { return PlayerInfo; }

	Protocol::MoveState GetMoveState() { return PlayerInfo->state(); }
	void SetMoveState(Protocol::MoveState State);

	void SetEquipmentMesh(int ItemId);

public:
	bool GetIsAim() { return bIsAim; }
	void SetIsAim(bool IsAim, bool IsSendPacket = true);

	bool GetIsCrouch() { return bIsCrouch; }
	void SetIsCrouch(bool IsCrouch, bool IsSendPacket = true);

	bool GetIsReload() { return bIsReload; }
	void SetIsReload(bool IsReload, bool IsSendPacket = true);

	bool GetIsAnimMirror() { return bIsAnimMirror; }
	void SetIsAnimMirror(bool IsAnimMirror, bool IsSendPacket = true);

	bool GetIsAttack() { return bIsAttack; }
	void SetIsAttack(bool IsAttack, bool IsSendPacket = true);

	bool GetIsDie() { return bIsDie; }
	void SetIsDie(bool IsDie, bool IsSendPacket = true);

	EPlayerState GetPlayerState() { return PlayerState; }
	void SetPlayerState(EPlayerState State, bool IsSendPacket = true);

	void SetPartOfMesh(ESkeletalMeshParts NameOfKey, class USkeletalMesh* PartsOfMesh, int MeshID);
	class USkeletalMeshComponent* GetPartsOfMesh(ESkeletalMeshParts NameOfKey) const;

	void SetClothesInfo(struct FCurrentEquipmentData EquipmentData);
	void SetClothesInfo(const Protocol::ClothesInfo& CurrentClothedInfoInfo);
	Protocol::ClothesInfo* GetClothesInfo() { return CurClothesInfo; }

	int GetItemID(ESkeletalMeshParts Name) const;
	
	void SetCanDamanged(bool Result) { bCanDamaged = Result; }
	bool GetCanDamaged() const { return bCanDamaged; }


	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

protected:
	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = true))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = true))
	class UCameraComponent* FollowCamera;

; private:
	void SyncronizeInfo();
	void ProcessMovePacket(float DeltaSeconds);

protected:
	class Protocol::PosInfo* PlayerInfo; // ���� ��ġ
	class Protocol::PosInfo* DestInfo; // ������
	class Protocol::ClothesInfo* CurClothesInfo; // ������

	const float RE_POSITION_TIME = 0.2f;
	float RePosTotalTime = 0.f;
	float LerpAmount = 0.f;
	float CurrentAngle;
	EPlayerState PlayerState;

private:
	bool bIsAim;
	bool bIsCrouch;
	bool bIsReload;
	bool bIsAnimMirror;
	bool bIsAttack;
	bool bIsDie;
private:
	bool bCanDamaged = false;

private:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Parts, meta = (AllowPrivateAccess = true))
	TMap<ESkeletalMeshParts, class USkeletalMeshComponent*> MeshList;
	TMap<ESkeletalMeshParts, int> ItemIDList;
};