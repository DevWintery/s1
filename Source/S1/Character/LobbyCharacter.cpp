// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/LobbyCharacter.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Components/SceneCaptureComponent2D.h"

#include "S1GameInstance.h"
#include "DataManager.h"
#include "CurrentEquipmentData.h"
 
// Sets default values
ALobbyCharacter::ALobbyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneCaptureArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SceneCaptureArm"));
	SceneCaptureArm->SetupAttachment(RootComponent);

	SceneCaptureCam = CreateDefaultSubobject<USceneCaptureComponent2D>(TEXT("SceneCaptureCam"));
	SceneCaptureCam->SetupAttachment(SceneCaptureArm);

}

// Called when the game starts or when spawned
void ALobbyCharacter::BeginPlay()
{
	Super::BeginPlay();

	//SceneCaptureCam->ShowOnlyActorComponents(this);

	//SetEquipWeapon(1);

	if (US1GameInstance* GI = Cast<US1GameInstance>(GetGameInstance()))
	{
		if (UDataManager* DM = GI->GetDataManager())
		{
			if (FCurrentEquipmentData* EquipmentData = DM->GetCurrentEquipItem())
			{
				SetClothesInfo(*EquipmentData);
			}
		}
	}	
}

// Called every frame
void ALobbyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ALobbyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

