// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseCharacter.h"
#include "LineTraceGun.h"
#include "RocketLauncher.h"
#include "Grenade.h"
#include "Punch.h"
#include "PaperSpriteComponent.h"
#include "InputActionValue.h"

// Sets default values
ABaseCharacter::ABaseCharacter()
	: MaxHp(100.f), CurrentHp(MaxHp), Amor(10.f)
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MinimapIcon = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("MinimapIcon"));
	MinimapIcon->SetupAttachment(RootComponent);
	MinimapIcon->SetVisibleInSceneCaptureOnly(true);
	MinimapIcon->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

// Called when the game starts or when spawned
void ABaseCharacter::BeginPlay()
{
	Super::BeginPlay();

	// Spawn GunActor
	if (nullptr != LineTraceGunClass)
	{
		GunActor = Cast<ALineTraceGun>(GetWorld()->SpawnActor(LineTraceGunClass));
		GunActor->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, TEXT("Skt_Rifle01"));
		GunActor->SetOwner(this);
	}

	if (nullptr != PunchClass)
	{
		PunchActor = Cast<APunch>(GetWorld()->SpawnActor(PunchClass));
		PunchActor->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, TEXT("Skt_Rifle01"));
		PunchActor->SetOwner(this);
	}

	if (nullptr != GunActor)
	{
		EquipWeapon = GunActor;
	}

	//if (MinimapIcon)
	//{
	//	// Preserve the sprite's world rotation by detaching and reattaching with KeepWorld transform rule
	//	MinimapIcon->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
	//	MinimapIcon->AttachToComponent(RootComponent, FAttachmentTransformRules(EAttachmentRule::KeepWorld, false));
	//}
}

// Called every frame
void ABaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//if (MinimapIcon)
	//{
	//	FRotator WorldRotation = FRotator(0.f, 0.f, 90.0f);// or any desired fixed world rotation
	//	MinimapIcon->SetWorldRotation(WorldRotation);
	//}
}

// Called to bind functionality to input
void ABaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ABaseCharacter::Attack(FVector StartLocation, FVector EndLocation)
{

}

void ABaseCharacter::TakeDamaged(int Damage)
{
	if (0 >= CurrentHp)
	{
		//UGameplayStatics::PlaySoundAtLocation(this, DeathSound, GetActorLocation());
	}

	else
	{
		UGameplayStatics::PlaySoundAtLocation(this, DamagedSound, GetActorLocation());
	}
}

void ABaseCharacter::Move(const FInputActionValue& Value)
{
}

void ABaseCharacter::Look(const FInputActionValue& Value)
{
}

void ABaseCharacter::SetEquipWeapon(ABaseWeapon* CurrentWeapon)
{
	if (EquipWeapon == nullptr)
	{
		return;
	}

	EquipWeapon->SetEquip(false, GetMesh());

	EquipWeapon = CurrentWeapon;

	EquipWeapon->SetEquip(true, GetMesh());
}

void ABaseCharacter::SetEquipWeapon(int Idx)
{
	switch (Idx)
	{
	case 1:
		SetEquipWeapon(GetGunActor());
		break;
	case 2:
		SetEquipWeapon(GetRocketLauncherActor());
		break;
	default:
		break;
	}
}
