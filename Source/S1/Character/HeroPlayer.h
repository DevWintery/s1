// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Character/BasePlayer.h"
#include "HeroPlayer.generated.h"

class USpringArmComponent;
class UCameraComponent;
class UInputMappingContext;
class UInputAction;
struct FInputActionValue;


/**
 *
 */
UCLASS()
class S1_API AHeroPlayer : public ABasePlayer
{
	GENERATED_BODY()

public:
	AHeroPlayer();

protected:
	/** Called for movement input */
	virtual void Move(const FInputActionValue& Value) override;

	/** Called for looking input */
	virtual void Look(const FInputActionValue& Value) override;

	/** Called for Aim input */
	void ZoomIn();

	void ZoomOut();

	/** Called for Attack Input */
	void InputAttack();

	void Fusillade();

	/** Called for ChangeWeapon Input */
	void InputChangeWeapon();

	void ChangeWeapon(int idx);

	/** Called for Crouch Input */
	void InputCrouch();

	void Reload();

	void Interact();

protected: /* virtual Function */
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// To add mapping context
	virtual void BeginPlay();

	virtual void Tick(float DeltaSeconds) override;;

	virtual void TakeDamaged(int Damage) override;

private:
	void Aim(float _DeltaSeconds);

public:
	void FinishReload();
	int GetNumOfInteractObject() const { return InteractObjectIDs.Num(); }
	

public:
	class UPlayerInfoViewModel* GetPlayerInfoViewModel() const { return InfoVM; }

	class UMaterialInstanceDynamic* GetMinimapMaterial() { return MinimapMaterial; }

	void AddInteractObject(uint64 ID);
	void RemoveInteractObject(uint64 ID);

private:
	/** MappingContext */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = true))
	UInputMappingContext* DefaultMappingContext;

	/** Aim Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = true))
	UInputAction* AimAction;

	/** Attack Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = true))
	UInputAction* AttackAction;

	/** ChangeWeapon Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = true))
	UInputAction* ChangeWeaponAction;

	/** Crouch Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = true))
	UInputAction* CrouchAction;

	/** Reload Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = true))
	UInputAction* ReloadAction;

	/** Interact Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = true))
	UInputAction* InteractAction;

	// ���� �� ����, �� �ƿ� �Ǵ� �ӵ� - 24/02/29 ����
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Zoom, meta = (AllowPrivateAccess = true))
	float ZoomSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Widget, meta = (AllowPrivateAccess = true))
	class UWidgetComponent* PlayerInfoWidget;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ViewModel, meta = (AllowPrivateAccess = true))
	class UPlayerInfoViewModel* InfoVM;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = SceneCapture, meta = (AllowPrivateAccess = true))
	USpringArmComponent* MinimapArm;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = SceneCapture, meta = (AllowPrivateAccess = true))
	class USceneCaptureComponent2D* MinimapCam;

private:
	void SendMovePacket(float DeltaSeconds);

private:
	float CamStartSocketX;
	float CamStartSocketY;
	float CamStartSocketZ;
	float DeltaTimeAcc;

	FVector RightDirection;
	FVector InfoWidgetLocation;

	// Zoom ���� ������ X, Y ���� - 24/02/28 ����
	FVector CAM_END_SOCKET_AIM = FVector(185.f, 70.f, 25.f);
	FVector CAM_END_SOCKET_NORMAL = FVector(40.f, 80.f, 20.f);

	bool bIsAttackable = true;
	int WeaponIndex;

protected:
	const float MOVE_PACKET_SEND_DELAY = 0.1f;
	float MovePacketSendTimer = MOVE_PACKET_SEND_DELAY;

	// Cache
	FVector2D DesiredInput;

	// Dirty Flag Test
	FVector2D LastDesiredInput;

private:
	TArray<uint64> InteractObjectIDs;

private:
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true))
	class UMaterialInterface* ParentMaterial;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true))
	class UMaterialInstanceDynamic* MinimapMaterial;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true))
	TSubclassOf<class UCameraShakeBase> AttackCameraShakeClass;
};