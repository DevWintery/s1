// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Protocol.pb.h"
#include "BaseCharacter.generated.h"

UCLASS()
class S1_API ABaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABaseCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

public:
	virtual void Attack(FVector StartLocation, FVector EndLocation);
	virtual void TakeDamaged(int Damage);
	virtual void Move(const struct FInputActionValue& Value);
	virtual void Look(const struct FInputActionValue& Value);

public:
	virtual Protocol::CreatureType GetCreatureType() { return CreatureType; }
	virtual void SetCreatureType(Protocol::CreatureType Type) { CreatureType = Type; }

	class ABaseWeapon* GetEquipWeapon() const { return EquipWeapon; }
	void SetEquipWeapon(ABaseWeapon* CurrentWeapon);
	void SetEquipWeapon(int Idx);

	class ALineTraceGun* GetGunActor() const { return GunActor; }
	void SetGunActor(ALineTraceGun* CurrentGun) { GunActor = CurrentGun; }

	class ARocketLauncher* GetRocketLauncherActor() const { return RocketLauncherActor; }
	void SetRocketLauncherActor(ARocketLauncher* CurrentRocketLauncher) { RocketLauncherActor = CurrentRocketLauncher; }

	class APunch* GetPunchActor() const { return PunchActor; }
	void SetRocketLauncherActor(APunch* CurrnetPunchActor) { PunchActor = CurrnetPunchActor; }

	FORCEINLINE float GetCurrentHp() const { return CurrentHp; }
	void SetCurrnetHp(float Hp) { CurrentHp = Hp; }

	FORCEINLINE float GetMaxHp() const { return MaxHp; }
	void SetMaxHp(float Hp) { MaxHp = Hp; }

	FORCEINLINE float GetAmor() const { return Amor; }
	void SetAmor(float AmorValue) { Amor = AmorValue; }

	class UPaperSpriteComponent* GetMinimapIcon() const { return MinimapIcon; }

	class TSubclassOf<ARocketLauncher> GetRocketLauncherClass() const { return RocketLauncherClass; }

protected:
	Protocol::CreatureType CreatureType;

	UPROPERTY(EditAnywhere)
	class USoundBase* DamagedSound;

	UPROPERTY(EditAnywhere)
	class USoundBase* DeathSound;

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Info, meta = (AllowPrivateAccess = true))
	float MaxHp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Info, meta = (AllowPrivateAccess = true))
	float CurrentHp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Info, meta = (AllowPrivateAccess = true))
	float Amor;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true));
	TSubclassOf<class ALineTraceGun> LineTraceGunClass;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true));
	TSubclassOf<class AGrenade> GrenadeClass;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true));
	TSubclassOf<class ARocketLauncher> RocketLauncherClass;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true));
	TSubclassOf<class APunch> PunchClass;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true));
	TSubclassOf<class ABaseWeapon> WeaponClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Icon, meta = (AllowPrivateAccess = true))
	class UPaperSpriteComponent* MinimapIcon;

private:
	class ALineTraceGun* GunActor;

	class AGrenade* GrenadeActor;

	class ARocketLauncher* RocketLauncherActor;

	class APunch* PunchActor;

	// ���� �������� ���� - 24/03/02 ����
	class ABaseWeapon* EquipWeapon;


};
