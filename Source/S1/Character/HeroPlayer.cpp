
// Fill out your copyright notice in the Description page of Project Settings.

#include "Character/HeroPlayer.h"
#include "Engine/LocalPlayer.h"
#include "S1Defines.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/Controller.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputActionValue.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "LineTraceGun.h"
#include "Grenade.h"
#include "Components/SplineComponent.h"
#include "Components/SplineMeshComponent.h"
#include "HeroController.h"
#include "Kismet/GameplayStatics.h"
#include "UIManager.h"
#include "CoverComponent.h"
#include "Components/WidgetComponent.h"
#include "PlayerInfoViewModel.h"
#include "HeroInfoWidget.h"
#include "MVVMSubsystem.h"
#include "View/MVVMView.h"
#include "Components/SceneCaptureComponent2D.h"
#include "Kismet/KismetRenderingLibrary.h"
#include "Kismet/KismetMaterialLibrary.h"
#include "StageGameMode.h"
#include "RocketLauncher.h"
#include "HeroInGameHUD.h"
//////////////////////////////////////////////////////////////////////////
// AS1Character

AHeroPlayer::AHeroPlayer()
	: ZoomSpeed(3.f), CamStartSocketX(0.f), CamStartSocketY(80.f), DeltaTimeAcc(1.f), WeaponIndex(0)
{
	MinimapArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("MinimapArm"));
	MinimapArm->SetupAttachment(RootComponent);

	MinimapCam = CreateDefaultSubobject<USceneCaptureComponent2D>(TEXT("MinimapCam"));
	MinimapCam->SetupAttachment(MinimapArm);

	PlayerInfoWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("PlayerInfo"));
	PlayerInfoWidget->SetupAttachment(FollowCamera);


	static ConstructorHelpers::FClassFinder<UCameraShakeBase> CAMERASHAKE(TEXT("/Game/S1/etc/BP_ShootCameraShake.BP_ShootCameraShake_C"));

	if (CAMERASHAKE.Succeeded())
	{
		AttackCameraShakeClass = CAMERASHAKE.Class;
	}

	SetCharacterMoveSpeed(PLAYER_RUN_SPEED);
}

void AHeroPlayer::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	if (APlayerController* PlayerController = Cast<APlayerController>(Controller))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(DefaultMappingContext, 0);
		}		
	}

	if (AHeroInGameHUD* hud = UGameplayStatics::GetPlayerController(this, 0)->GetHUD<AHeroInGameHUD>())
	{
		MinimapCam->TextureTarget = UKismetRenderingLibrary::CreateRenderTarget2D(GetWorld());
		MinimapMaterial = UKismetMaterialLibrary::CreateDynamicMaterialInstance(GetWorld(), ParentMaterial);
		MinimapMaterial->SetTextureParameterValue(TEXT("RenderTexture"), MinimapCam->TextureTarget);
		MinimapCam->TextureTarget->UpdateResource();
		MinimapCam->ShowFlags.DynamicShadows = false;
		hud->SetMinimapTexture();
	}

	if (nullptr != PlayerInfoWidget)
	{
		PlayerInfoWidget->SetWidgetClass(LoadClass<UUserWidget>(nullptr, TEXT("/Game/S1/UI/InGame/Player/WBP_HeroInfoWidget.WBP_HeroInfoWidget_C")));
		PlayerInfoWidget->SetWidgetSpace(EWidgetSpace::Screen);
		PlayerInfoWidget->GetWidget()->SetVisibility(ESlateVisibility::HitTestInvisible);

		InfoVM = Cast<UHeroInfoWidget>(PlayerInfoWidget->GetWidget())->GetInfoViewModel();
	}
	
	if (InfoVM)
	{
		InfoVM->SetMaxHealth(GetMaxHp());
		InfoVM->SetCurrentHealth(GetMaxHp());

		FString AmmoStr;
		FText AmmoText;
		AmmoStr = FString::FromInt(FMath::Max(Cast<ALineTraceGun>(GetEquipWeapon())->GetCurrentAmmo(), 0));
		AmmoText = FText::FromString(AmmoStr);
		InfoVM->SetEquipAmmo(AmmoText);

		AmmoStr = FString::FromInt(FMath::Max(Cast<ALineTraceGun>(GetEquipWeapon())->GetToTalAmmo(), 0));
		AmmoText = FText::FromString(AmmoStr);
		InfoVM->SetTotalAmmo(AmmoText);
	}
}

void AHeroPlayer::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	SendMovePacket(DeltaSeconds);
	
	if (AStageGameMode* GameMode = Cast<AStageGameMode>(UGameplayStatics::GetGameMode(GetWorld())))
	{
		AHeroController* HC = Cast<AHeroController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));

		if (nullptr == HC)
		{
			return;
		}

		else if (GameMode->GetPlayingCine() || GameMode->GetIsClear() || GetIsDie())
		{
			PlayerInfoWidget->GetWidget()->SetVisibility(ESlateVisibility::Collapsed);
		}

		else
		{
			PlayerInfoWidget->GetWidget()->SetVisibility(ESlateVisibility::HitTestInvisible);
		}
	}

	// Zoom 함수
	Aim(DeltaSeconds);
}

//////////////////////////////////////////////////////////////////////////
// Input

void AHeroPlayer::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent))
	{
		// ZoomIn
		EnhancedInputComponent->BindAction(AimAction, ETriggerEvent::Started, this, &AHeroPlayer::ZoomIn);

		// ZoomOut
		EnhancedInputComponent->BindAction(AimAction, ETriggerEvent::Completed, this, &AHeroPlayer::ZoomOut);

		// Attack
		EnhancedInputComponent->BindAction(AttackAction, ETriggerEvent::Started, this, &AHeroPlayer::InputAttack);

		// Fusillade : 연속 사격
		EnhancedInputComponent->BindAction(AttackAction, ETriggerEvent::Triggered, this, &AHeroPlayer::Fusillade);

		// ChangeWeapon
		EnhancedInputComponent->BindAction(ChangeWeaponAction, ETriggerEvent::Started, this, &AHeroPlayer::InputChangeWeapon);
		
		// Cover
		EnhancedInputComponent->BindAction(CrouchAction, ETriggerEvent::Started, this, &AHeroPlayer::InputCrouch);

		// Reload
		EnhancedInputComponent->BindAction(ReloadAction, ETriggerEvent::Started, this, &AHeroPlayer::Reload);

		// Interact
		EnhancedInputComponent->BindAction(InteractAction, ETriggerEvent::Started, this, &AHeroPlayer::Interact);
	}

	else
	{
		//UE_LOG(LogTemplateCharacter, Error, TEXT("'%s' Failed to find an Enhanced Input component! This template is built to use the Enhanced Input system. If you intend to use the legacy system, then you will need to update this C++ file."), *GetNameSafe(this));
	}
}

void AHeroPlayer::Move(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D MovementVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		/*	if (!GetIsCrouch())
			{*/
			// get forward vector
			const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

			AddMovementInput(ForwardDirection, MovementVector.Y);

			// get right vector 
			RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		//}

		AddMovementInput(RightDirection, MovementVector.X);

		SetPlayerState(EPlayerState::MOVE);

		if (MovementVector.X > 0)
		{
			// 오른쪽으로 이동 중
			SetIsAnimMirror(false);
		}

		else if (MovementVector.X < 0)
		{
			// 왼쪽으로 이동 중
			SetIsAnimMirror(true);
		}

		// Cache
		{
			DesiredInput = MovementVector;
		}
	}


}

void AHeroPlayer::Look(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D LookAxisVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// add yaw and pitch input to controller
		AddControllerYawInput(LookAxisVector.X);
		AddControllerPitchInput(LookAxisVector.Y);
	}
}

void AHeroPlayer::ZoomIn()
{
	if (!bIsAttackable)
	{
		return;
	}

	InfoWidgetLocation = PlayerInfoWidget->GetRelativeLocation();

	DeltaTimeAcc = 0.f;

	CamStartSocketX = CameraBoom->SocketOffset.X;
	CamStartSocketY = CameraBoom->SocketOffset.Y;
	CamStartSocketZ = CameraBoom->SocketOffset.Z;

	SetIsAim(true);

	/*바로 회전 - 24/03/01 - 임희섭*/
	//bUseControllerRotationYaw = true;

	/*부드럽게 회전 - 24/03/01 - 임희섭*/
	GetCharacterMovement()->bOrientRotationToMovement = false;
	GetCharacterMovement()->bUseControllerDesiredRotation = true;
	//GetCharacterMovement()->RotationRate.Yaw += (GetCharacterMovement()->RotationRate.Yaw / 3.f);
	SetCharacterMoveSpeed(PLAYER_WALK_SPEED);

}

void AHeroPlayer::ZoomOut()
{
	DeltaTimeAcc = 0.f;

	CamStartSocketX = CameraBoom->SocketOffset.X;
	CamStartSocketY = CameraBoom->SocketOffset.Y;
	CamStartSocketZ = CameraBoom->SocketOffset.Z;

	SetIsAim(false);

	/*바로 회전 - 24/03/01 - 임희섭*/
	//bUseControllerRotationYaw = false;

	/*부드럽게 회전 - 24/03/01 - 임희섭*/
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->bUseControllerDesiredRotation = false;
	//GetCharacterMovement()->RotationRate.Yaw -= (GetCharacterMovement()->RotationRate.Yaw / 3.f);
	SetCharacterMoveSpeed(PLAYER_RUN_SPEED);

}

void AHeroPlayer::InputAttack()
{
	ABaseWeapon* CurrentWeapon = GetEquipWeapon();

	if (nullptr != CurrentWeapon && GetIsAim())
	{
		if(0 < CurrentWeapon->GetCurrentAmmo())
		{
			if (APlayerController* PC = UGameplayStatics::GetPlayerController(GetWorld(), 0))
			{
				if (nullptr != AttackCameraShakeClass)
				{
					PC->ClientStartCameraShake(AttackCameraShakeClass);
				}

				if (AHeroInGameHUD* hud = PC->GetHUD<AHeroInGameHUD>())
				{
					hud->PlayerHUDAnimation(EUIType::CROSSHAIR);
				}
			}
			SetIsAttack(true);
		}

		CurrentWeapon->SendAtackPKT();

		FString AmmoStr;
		if (CurrentWeapon->GetCurrentAmmo() < 10)
		{
			AmmoStr += "0";
		}

		AmmoStr += FString::FromInt(FMath::Max(CurrentWeapon->GetCurrentAmmo() - 1, 0));
		FText AmmoText = FText::FromString(AmmoStr);
		InfoVM->SetEquipAmmo(AmmoText);
	}

	else
	{
		//GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Blue, TEXT("EquipWeapon Is Null"));
	}
}

void AHeroPlayer::Fusillade()
{
	//if (nullptr != GetGunActor())
	//{
	//	Cast<ALineTraceGun>(GetGunActor())->SetIsFusillade(true);
	//	GetEquipWeapon()->Attack();

	//	Cast<AHeroController>(UGameplayStatics::GetPlayerController(GetWorld(), 0))->PlayerAttackUIAnimation();
	//	SetIsAttack(true);
	//}

	//else
	//{
	//	GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Blue, TEXT("GunActor Is Null"));
	//}
}

void AHeroPlayer::InputChangeWeapon()
{
	if(GetIsAim() || GetIsReload())
	{	
		return;
	}
	
	APlayerController* PlayerController = Cast<APlayerController>(Controller);
	if (PlayerController == nullptr)
	{
		return;
	}

	UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer());
	if (Subsystem == nullptr)
	{
		return;
	}

	TArray<FKey> Inputs;
	Inputs = Subsystem->QueryKeysMappedToAction(ChangeWeaponAction);

	for (int idx = 0; idx < Inputs.Num(); idx++)
	{
		if (PlayerController->IsInputKeyDown(Inputs[idx]) && WeaponIndex != idx)
		{
			WeaponIndex = idx;
			ChangeWeapon(idx + 1);

			Protocol::C_CHANGE_WEAPON pkt;
			pkt.set_object_id(PlayerInfo->object_id());
			pkt.set_weapon_id(idx + 1);

			SEND_PACKET(pkt);

			break;
		}
	}
}

void AHeroPlayer::ChangeWeapon(int Idx)
{
	switch (Idx)
	{
	case 1:
		SetEquipWeapon(GetGunActor());
		break;
	case 2:
		SetEquipWeapon(GetRocketLauncherActor());
		break;
	default:
		break;
	}

	Cast<UHeroInfoWidget>(PlayerInfoWidget->GetWidget())->SetEquipWeapon(Idx);

	FinishReload();
}

void AHeroPlayer::InputCrouch()
{
	//죽는 화면 디버깅 코드
	Protocol::C_HIT Hitpkt;

	Hitpkt.set_damage(GetMaxHp() / 2.f);
	Hitpkt.set_object_id(GetPlayerInfo()->object_id());
	SEND_PACKET(Hitpkt);
	//SetCanDamanged(false);
	/*SetCurrnetHp(GetMaxHp());
	InfoVM->SetCurrentHealth(GetCurrentHp());*/
}

void AHeroPlayer::Reload()
{
	//GetEquipWeapon()->SetEquip(true, GetMesh());

	if (GetIsAim())
	{
		return;
	}

	if (ABaseWeapon* Weapon = GetEquipWeapon())
	{
		if (Weapon->GetMaxAmmo() == Weapon->GetCurrentAmmo() || 0 == Weapon->GetToTalAmmo())
		{
			return;
		}

		else
		{
			SetIsReload(true);
			bIsAttackable = false;
		}
	}
}

void AHeroPlayer::Interact()
{
	if (0 >= InteractObjectIDs.Num())
	{
		return;
	}

	uint64 ID = InteractObjectIDs[0];
	Protocol::C_INTERACT pkt;

	pkt.set_object_id(ID);
	pkt.set_interact_type(Protocol::INTERACT_NEXT_STEP);

	SEND_PACKET(pkt);
}

void AHeroPlayer::TakeDamaged(int Damage)
{
	Super::TakeDamaged(Damage);

	InfoVM->SetCurrentHealth(GetCurrentHp());

	if (0 >= GetCurrentHp())
	{
		if (AHeroController* HC = Cast<AHeroController>(UGameplayStatics::GetPlayerController(GetWorld(), 0)))
		{
			HC->PawnKilled();
			UHeroInfoWidget* Widget = Cast<UHeroInfoWidget>(PlayerInfoWidget->GetWidget());
			Widget->SetVisibility(ESlateVisibility::Collapsed);

			UUIManager* UIManager = GetGameInstance()->GetSubsystem<UUIManager>();
			UIManager->CreateUIWidget(EUIType::GAMEOVER);
			UIManager->RemoveGoalIcon();
		}

		DetachFromControllerPendingDestroy();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	}
}

void AHeroPlayer::Aim(float _DeltaSeconds)
{
	if (1.f == DeltaTimeAcc)
	{
		return;
	}

	DeltaTimeAcc = FMath::Min(DeltaTimeAcc + _DeltaSeconds * ZoomSpeed, 1.f); // DeltaTimeAcc의 최댓값을 1로 제한

	float CamEndSocketX = GetIsAim() ? CAM_END_SOCKET_AIM.X : CAM_END_SOCKET_NORMAL.X;
	float CamEndSocketY = GetIsAim() ? CAM_END_SOCKET_AIM.Y : CAM_END_SOCKET_NORMAL.Y;
	float CamEndSocketZ = GetIsAim() ? CAM_END_SOCKET_AIM.Z : CAM_END_SOCKET_NORMAL.Z;

	CameraBoom->SocketOffset.X = FMath::Lerp(CamStartSocketX, CamEndSocketX, DeltaTimeAcc);
	CameraBoom->SocketOffset.Y = FMath::Lerp(CamStartSocketY, CamEndSocketY, DeltaTimeAcc);
	CameraBoom->SocketOffset.Z = FMath::Lerp(CamStartSocketZ, CamEndSocketZ, DeltaTimeAcc);
}

void AHeroPlayer::FinishReload()
{
	if (ABaseWeapon* CurrentWeapon = GetEquipWeapon())
	{
		CurrentWeapon->Reload();

		FString AmmoStr;
		FText AmmoText;
		if (CurrentWeapon->GetCurrentAmmo() < 10)
		{
			AmmoStr += "0";
		}

		AmmoStr += FString::FromInt(FMath::Max(CurrentWeapon->GetCurrentAmmo(), 0));
		AmmoText = FText::FromString(AmmoStr);
		InfoVM->SetEquipAmmo(AmmoText);

		FString TotalAmmoStr;
		if (CurrentWeapon->GetToTalAmmo() < 10)
		{
			TotalAmmoStr += "0";
		}

		TotalAmmoStr += FString::FromInt(FMath::Max(CurrentWeapon->GetToTalAmmo(), 0));
		AmmoText = FText::FromString(TotalAmmoStr);
		InfoVM->SetTotalAmmo(AmmoText);
	}

	SetIsReload(false);
	bIsAttackable = true;
}

void AHeroPlayer::AddInteractObject(uint64 ID)
{
	InteractObjectIDs.Add(ID);

	//if (AHeroController* HC = Cast<AHeroController>(UGameplayStatics::GetPlayerController(GetWorld(), 0)))
	//{
	//	HC->Interactable(true);
	//}
}

void AHeroPlayer::RemoveInteractObject(uint64 ID)
{
	InteractObjectIDs.Remove(ID);

	//if (0 >= InteractObjectIDs.Num())
	//{
	//	if (AHeroController* HC = Cast<AHeroController>(UGameplayStatics::GetPlayerController(GetWorld(), 0)))
	//	{
	//		HC->Interactable(false);
	//	}
	//}
}

void AHeroPlayer::SendMovePacket(float DeltaSeconds)
{
	bool ForceSendPacket = false;

	if (LastDesiredInput != DesiredInput)
	{
		ForceSendPacket = true;
		LastDesiredInput = DesiredInput;
	}

	// State 정보
	if (DesiredInput == FVector2D::Zero())
	{
		SetMoveState(Protocol::MOVE_STATE_IDLE);
	}
	else
	{
		SetMoveState(Protocol::MOVE_STATE_RUN);
	}

	MovePacketSendTimer -= DeltaSeconds;

	if (MovePacketSendTimer <= 0 || ForceSendPacket)
	{
		MovePacketSendTimer = MOVE_PACKET_SEND_DELAY;

		Protocol::C_MOVE MovePkt;
		{
			Protocol::PosInfo* Info = MovePkt.mutable_info();

			//다음 위치 예측해서 던져주기
			FVector CurrentVelocity = GetVelocity();
			FVector NextFrameLocation = GetActorLocation() + CurrentVelocity * MOVE_PACKET_SEND_DELAY;
			Info->CopyFrom(*PlayerInfo);
			Info->set_x(NextFrameLocation.X);
			Info->set_y(NextFrameLocation.Y);
			Info->set_z(NextFrameLocation.Z);
			Info->set_yaw(GetActorRotation().Yaw);
			Info->set_state(GetMoveState());
			Info->set_speed(CurrentVelocity.Size());
		}

		SEND_PACKET(MovePkt);
	}
}
