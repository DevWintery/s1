// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BasePlayer.h"
#include "LobbyCharacter.generated.h"

UCLASS()
class S1_API ALobbyCharacter : public ABasePlayer
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ALobbyCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = SceneCapture, meta = (AllowPrivateAccess = true))
	class USpringArmComponent* SceneCaptureArm;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = SceneCapture, meta = (AllowPrivateAccess = true))
	class USceneCaptureComponent2D* SceneCaptureCam;
};
