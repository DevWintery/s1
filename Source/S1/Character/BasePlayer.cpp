// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/BasePlayer.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "HeroPlayer.h"
#include "Kismet/KismetMathLibrary.h"
#include "S1Defines.h"
#include "LineTraceGun.h"
#include "RocketLauncher.h"
#include "Grenade.h"
#include "PaperSpriteComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "DataManager.h"
#include "ItemData.h"
#include "CurrentEquipmentData.h"

ABasePlayer::ABasePlayer()
	: CurrentAngle(0.f), bIsAim(false), bIsCrouch(false), bIsReload(false)
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 500.0f, 0.0f); // ...at this rotation rate

	// Note: For faster iteration times these variables, and many more, can be tweaked in the Character Blueprint
	// instead of recompiling to adjust them
	GetCharacterMovement()->JumpZVelocity = 700.f;
	GetCharacterMovement()->AirControl = 0.35f;
	GetCharacterMovement()->MaxWalkSpeed = 500.f;
	GetCharacterMovement()->MinAnalogWalkSpeed = 20.f;
	GetCharacterMovement()->BrakingDecelerationWalking = 2000.f;
	GetCharacterMovement()->BrakingDecelerationFalling = 1500.0f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 250.f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	USkeletalMeshComponent* PartsOfHelmet = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("PartsOfHelmet"));
	PartsOfHelmet->SetupAttachment(GetMesh());
	PartsOfHelmet->SetLeaderPoseComponent(GetMesh(), true);
	MeshList.Emplace(ESkeletalMeshParts::HELMET, PartsOfHelmet);

	USkeletalMeshComponent* PartsOfAmor = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("PartsOfAmor"));
	PartsOfAmor->SetupAttachment(GetMesh());
	PartsOfAmor->SetLeaderPoseComponent(GetMesh(), true);
	MeshList.Emplace(ESkeletalMeshParts::AMOR, PartsOfAmor);

	USkeletalMeshComponent* PartsOfBelts = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("PartsOfBelts"));
	PartsOfBelts->SetupAttachment(GetMesh());
	PartsOfBelts->SetLeaderPoseComponent(GetMesh(), true);
	MeshList.Emplace(ESkeletalMeshParts::BELTS, PartsOfBelts);

	USkeletalMeshComponent* PartsOfBoots = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("PartsOfBoots"));
	PartsOfBoots->SetupAttachment(GetMesh());
	PartsOfBoots->SetLeaderPoseComponent(GetMesh(), true);
	MeshList.Emplace(ESkeletalMeshParts::BOOTS, PartsOfBoots);

	USkeletalMeshComponent* PartsOfGlasses = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("PartsOfGlasses"));
	PartsOfGlasses->SetupAttachment(GetMesh());
	PartsOfGlasses->SetLeaderPoseComponent(GetMesh(), true);
	MeshList.Emplace(ESkeletalMeshParts::GLASSES, PartsOfGlasses);

	USkeletalMeshComponent* PartsOfGloves = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("PartsOfGloves"));
	PartsOfGloves->SetupAttachment(GetMesh());
	PartsOfGloves->SetLeaderPoseComponent(GetMesh(), true);
	MeshList.Emplace(ESkeletalMeshParts::GLOVES, PartsOfGloves);

	USkeletalMeshComponent* PartsOfJaket = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("PartsOfJaket"));
	PartsOfJaket->SetupAttachment(GetMesh());
	PartsOfJaket->SetLeaderPoseComponent(GetMesh(), true);
	MeshList.Emplace(ESkeletalMeshParts::JACKET, PartsOfJaket);

	USkeletalMeshComponent* PartsOfPants = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("PartsOfPants"));
	PartsOfPants->SetupAttachment(GetMesh());
	PartsOfPants->SetLeaderPoseComponent(GetMesh(), true);
	MeshList.Emplace(ESkeletalMeshParts::PANTS, PartsOfPants);

	USkeletalMeshComponent* PartsOfMask = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("PartsOfMask"));
	PartsOfMask->SetupAttachment(GetMesh());
	PartsOfMask->SetLeaderPoseComponent(GetMesh(), true);
	MeshList.Emplace(ESkeletalMeshParts::MASK, PartsOfMask);

	USkeletalMeshComponent* PartsOfNVD = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("PartsOfNVD"));
	PartsOfNVD->SetupAttachment(GetMesh());
	PartsOfNVD->SetLeaderPoseComponent(GetMesh(), true);
	MeshList.Emplace(ESkeletalMeshParts::NVD, PartsOfNVD);

	USkeletalMeshComponent* PartsOfRadio = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("PartsOfRadio"));
	PartsOfRadio->SetupAttachment(GetMesh());
	PartsOfRadio->SetLeaderPoseComponent(GetMesh(), true);
	MeshList.Emplace(ESkeletalMeshParts::RADIO, PartsOfRadio);

	PlayerInfo = new Protocol::PosInfo();
	DestInfo = new Protocol::PosInfo();

	PlayerState = EPlayerState::IDLE;
}

ABasePlayer::~ABasePlayer()
{
	delete PlayerInfo;
	delete DestInfo;
	PlayerInfo = nullptr;
	DestInfo = nullptr;
}

void ABasePlayer::BeginPlay()
{
	Super::BeginPlay();

	// Spawn RocketLauncher
	if (nullptr != GetRocketLauncherClass())
	{
		SetRocketLauncherActor(Cast<ARocketLauncher>(GetWorld()->SpawnActor(GetRocketLauncherClass())));

		GetRocketLauncherActor()->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, TEXT("WeaponEquipRocketALauncher"));
		GetRocketLauncherActor()->SetOwner(this);
	}

	FVector Location = GetActorLocation();
	DestInfo->set_x(Location.X);
	DestInfo->set_y(Location.Y);
	DestInfo->set_z(Location.Z);
	DestInfo->set_yaw(GetControlRotation().Yaw);

	SetMoveState(Protocol::MOVE_STATE_IDLE);
}

void ABasePlayer::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	SyncronizeInfo();
	ProcessMovePacket(DeltaSeconds);
}

void ABasePlayer::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void ABasePlayer::Attack(FVector StartLocation, FVector EndLocation)
{
	if (GetEquipWeapon())
	{
		GetEquipWeapon()->Attack(StartLocation, EndLocation);
	}
}

void ABasePlayer::TakeDamaged(int Damage)
{
	Super::TakeDamaged(Damage);

	float CurrentDamage = FMath::Min(GetCurrentHp(), Damage);

	SetCurrnetHp(GetCurrentHp() - CurrentDamage);

	if (0 >= GetCurrentHp())
	{
		bIsDie = true;
		PlayerState = EPlayerState::DIE;
		 
		US1GameInstance* GI = Cast<US1GameInstance>(GetGameInstance());

		int NumOfCurrnetPlayer = GI->GetNumOfPlayer();

		GI->SetNumOfPlayer(--NumOfCurrnetPlayer);
	}
}

void ABasePlayer::SetCharacterMoveSpeed(const float MoveSpeed)
{
	GetCharacterMovement()->MaxWalkSpeed = MoveSpeed;
}

bool ABasePlayer::IsMyPlayer()
{
	return Cast<AHeroPlayer>(this) != nullptr;
}

float ABasePlayer::GetAngle()
{
	return CurrentAngle;
}

void ABasePlayer::SetMoveState(Protocol::MoveState State)
{
	if (PlayerInfo->state() == State)
		return;

	PlayerInfo->set_state(State);

	// TODO
}

void ABasePlayer::SetEquipmentMesh(int ItemId)
{
	if (US1GameInstance* GI = Cast<US1GameInstance>(GetGameInstance()))
	{
		if (UDataManager* DataManager = GI->GetDataManager())
		{
			FItemData* ItemData = DataManager->GetItemData(ItemId);

			if (USkeletalMesh* PartsOfMesh = LoadObject<USkeletalMesh>(nullptr, *ItemData->MeshPath))
			{
				SetPartOfMesh(ItemData->ItemType, PartsOfMesh, ItemId);
			}
			
			switch (ItemData->TypeOfStat)
			{
			case ETypeOfStat::NONE:
				break;
			case ETypeOfStat::HP:
			{
				//SetMaxHp(GetMaxHp() + ItemData->Stat);
			}
				break;
			case ETypeOfStat::OFFENCT:
				break;
			case ETypeOfStat::DEFENCE:
			{
				//SetAmor(GetAmor() + ItemData->Stat);
			}
				break;
			case ETypeOfStat::AMMO:
			{
				//GetGunActor()->SetTotalAmmo(GetGunActor()->GetToTalAmmo() + ItemData->Stat);
				if(nullptr != GetRocketLauncherActor())
				{
					GetRocketLauncherActor()->SetTotalAmmo(GetRocketLauncherActor()->GetToTalAmmo() + (ItemData->Stat));
				}
			}
				break;
			default:
				break;
			}
		}
	}
}

void ABasePlayer::SetIsAim(bool IsAim, bool IsSendPacket)
{
	bIsAim = IsAim;

	if (IsSendPacket)
	{

		if (bIsCrouch && !IsAim)
		{
			SetIsCrouch(true);
			return;
		}

		Protocol::C_ANIMATION_STATE pkt;
		pkt.set_object_id(GetPlayerInfo()->object_id());
		pkt.set_animation_state(bIsAim ? Protocol::ANIMATION_STATE_AIM : Protocol::ANIMATION_STATE_NONE);

		SEND_PACKET(pkt);
	}
}

void ABasePlayer::SetIsCrouch(bool IsCrouch, bool IsSendPacket)
{
	bIsCrouch = IsCrouch;

	if (IsSendPacket)
	{
		if (!IsCrouch && bIsAim)
		{
			SetIsAim(true);
			return;
		}

		Protocol::C_ANIMATION_STATE pkt;
		pkt.set_object_id(GetPlayerInfo()->object_id());
		pkt.set_animation_state(bIsCrouch ? Protocol::ANIMATION_STATE_CROUCH : Protocol::ANIMATION_STATE_NONE);

		SEND_PACKET(pkt);
	}
}

void ABasePlayer::SetIsReload(bool IsReload, bool IsSendPacket)
{
	bIsReload = IsReload;

	if (IsSendPacket)
	{
		Protocol::C_ANIMATION_STATE pkt;
		pkt.set_object_id(GetPlayerInfo()->object_id());
		pkt.set_animation_state(bIsReload ? Protocol::ANIMATION_STATE_RELOAD : Protocol::ANIMATION_STATE_NONE);

		SEND_PACKET(pkt);
	}
}

void ABasePlayer::SetIsAnimMirror(bool IsAnimMirror, bool IsSendPacket)
{
	bIsAnimMirror = IsAnimMirror;

	//애니메이션 좌우 반대로 하기 위한 bool값 입니다.

}

void ABasePlayer::SetIsAttack(bool IsAttack, bool IsSendPacket)
{
	bIsAttack = IsAttack;
}

void ABasePlayer::SetIsDie(bool IsDie, bool IsSendPacket)
{
	bIsDie = IsDie;

	if (IsSendPacket)
	{
		Protocol::C_ANIMATION_STATE pkt;
		pkt.set_object_id(GetPlayerInfo()->object_id());
		pkt.set_animation_state(bIsDie ? Protocol::ANIMATION_STATE_DIE : Protocol::ANIMATION_STATE_NONE);

		SEND_PACKET(pkt);
	}
}

void ABasePlayer::SetPlayerState(EPlayerState State, bool IsSendPacket)
{
	PlayerState = State;
}

void ABasePlayer::SetPartOfMesh(ESkeletalMeshParts NameOfKey, USkeletalMesh* PartsOfMesh, int MeshID)
{
	for (auto& iter : MeshList)
	{
		if (iter.Key == NameOfKey)
		{
			iter.Value->SetSkeletalMesh(PartsOfMesh);
			ItemIDList.Emplace(NameOfKey, MeshID);
			break;
		}
	}

	if (US1GameInstance* GI = Cast<US1GameInstance>(GetGameInstance()))
	{
		if (UDataManager* DM = GI->GetDataManager())
		{
			DM->SetEquipment(NameOfKey, MeshID);
		}
	}

	return;
}

USkeletalMeshComponent* ABasePlayer::GetPartsOfMesh(ESkeletalMeshParts NameOfKey) const
{
	if(MeshList.Contains(NameOfKey))
	{ 
		return MeshList[NameOfKey]; 
	}

	else
	{
		return nullptr;
	}
}

void ABasePlayer::SetClothesInfo(FCurrentEquipmentData EquipmentData)
{
	if (JACKET_MIN <= EquipmentData.JacketID && JACKET_MAX >= EquipmentData.JacketID)
	{
		SetEquipmentMesh(EquipmentData.JacketID);
	}

	if (PANTS_MIN <= EquipmentData.PantsID && PANTS_MAX >= EquipmentData.PantsID)
	{
		SetEquipmentMesh(EquipmentData.PantsID);
	}

	if (BOOTS_MIN <= EquipmentData.BootsID && BOOTS_MAX >= EquipmentData.BootsID)
	{
		SetEquipmentMesh(EquipmentData.BootsID);
	}

	if (NVD_MIN <= EquipmentData.NvdID && NVD_MAX >= EquipmentData.NvdID)
	{
		SetEquipmentMesh(EquipmentData.NvdID);
	}

	if (AMOR_MIN <= EquipmentData.AmorID && AMOR_MAX >= EquipmentData.AmorID)
	{
		SetEquipmentMesh(EquipmentData.AmorID);
	}

	if (HELMET_MIN <= EquipmentData.HelmetID && HELMET_MAX >= EquipmentData.HelmetID)
	{
		SetEquipmentMesh(EquipmentData.HelmetID);
	}

	if (GLASSES_MIN <= EquipmentData.GlassesID && GLASSES_MAX >= EquipmentData.GlassesID)
	{
		SetEquipmentMesh(EquipmentData.GlassesID);
	}

	if (MASK_MIN <= EquipmentData.MaskID && MASK_MAX > EquipmentData.MaskID)
	{
		SetEquipmentMesh(EquipmentData.MaskID);
	}

	if (GLOVES_MIN <= EquipmentData.GlovesID && GLOVES_MAX > EquipmentData.GlovesID)
	{
		SetEquipmentMesh(EquipmentData.GlovesID);
	}

	if (BELTS_MIN <= EquipmentData.BeltsID && BELTS_MAX >= EquipmentData.BeltsID)
	{
		SetEquipmentMesh(EquipmentData.BeltsID);
	}

	if (RADIO_MIN <= EquipmentData.RadioID && RADIO_MAX >= EquipmentData.RadioID)
	{
		SetEquipmentMesh(EquipmentData.RadioID);
	}
}

void ABasePlayer::SetClothesInfo(const Protocol::ClothesInfo& CurrentClothedInfoInfo)
{
	if (JACKET_MIN <= CurrentClothedInfoInfo.jacket_id() && JACKET_MAX >= CurrentClothedInfoInfo.jacket_id())
	{	
		SetEquipmentMesh(CurrentClothedInfoInfo.jacket_id());
	}

	if (PANTS_MIN <= CurrentClothedInfoInfo.pants_id() && PANTS_MAX >= CurrentClothedInfoInfo.pants_id())
	{
		SetEquipmentMesh(CurrentClothedInfoInfo.pants_id());
	}

	if (BOOTS_MIN <= CurrentClothedInfoInfo.boots_id() && BOOTS_MAX >= CurrentClothedInfoInfo.boots_id())
	{
		SetEquipmentMesh(CurrentClothedInfoInfo.boots_id());
	}

	if (NVD_MIN <= CurrentClothedInfoInfo.nvd_id() && NVD_MAX >= CurrentClothedInfoInfo.nvd_id())
	{
		SetEquipmentMesh(CurrentClothedInfoInfo.nvd_id());
	}

	if (AMOR_MIN <= CurrentClothedInfoInfo.amor_id() && AMOR_MAX >= CurrentClothedInfoInfo.amor_id())
	{
		SetEquipmentMesh(CurrentClothedInfoInfo.amor_id());
	}

	if (HELMET_MIN <= CurrentClothedInfoInfo.helmet_id() && HELMET_MAX >= CurrentClothedInfoInfo.helmet_id())
	{
		SetEquipmentMesh(CurrentClothedInfoInfo.helmet_id());
	}

	if (GLASSES_MIN <= CurrentClothedInfoInfo.glasses_id() && GLASSES_MAX >= CurrentClothedInfoInfo.glasses_id())
	{
		SetEquipmentMesh(CurrentClothedInfoInfo.glasses_id());
	}

	if (MASK_MIN <= CurrentClothedInfoInfo.mask_id() && MASK_MAX > CurrentClothedInfoInfo.mask_id())
	{
		SetEquipmentMesh(CurrentClothedInfoInfo.mask_id());
	}

	if (GLOVES_MIN <=  CurrentClothedInfoInfo.gloves_id() && GLOVES_MAX > CurrentClothedInfoInfo.gloves_id())
	{
		SetEquipmentMesh(CurrentClothedInfoInfo.gloves_id());
	}

	if (BELTS_MIN <= CurrentClothedInfoInfo.belts_id() && BELTS_MAX >= CurrentClothedInfoInfo.belts_id())
	{
		SetEquipmentMesh(CurrentClothedInfoInfo.belts_id());
	}

	if (RADIO_MIN <= CurrentClothedInfoInfo.radio_id() && RADIO_MAX >= CurrentClothedInfoInfo.radio_id())
	{
		SetEquipmentMesh(CurrentClothedInfoInfo.radio_id());
	}
}

int ABasePlayer::GetItemID(ESkeletalMeshParts Name) const
{
	if (ItemIDList.Contains(Name))
	{
		return ItemIDList[Name];
	}

	else
	{
		return 0;
	}
}

void ABasePlayer::SetPlayerInfo(const Protocol::PosInfo& Info)
{
	if (PlayerInfo->object_id() != 0)
	{
		assert(PlayerInfo->object_id() == Info.object_id());
	}

	if (nullptr == PlayerInfo)
	{
		PlayerInfo = new Protocol::PosInfo();
	}

	PlayerInfo->CopyFrom(Info);

	FVector Location(Info.x(), Info.y(), Info.z());
	SetActorLocation(Location);
}

void ABasePlayer::SetDestInfo(const Protocol::PosInfo& Info)
{
	if (PlayerInfo->object_id() != 0)
	{
		assert(PlayerInfo->object_id() == Info.object_id());
	}

	// Dest에 최종 상태 복사.
	DestInfo->CopyFrom(Info);

	//플레이어 Info에 적용할것들
	SetMoveState(Info.state());
	PlayerInfo->set_speed(DestInfo->speed());
}

void ABasePlayer::SyncronizeInfo()
{
	FVector Location = GetActorLocation();
	PlayerInfo->set_x(Location.X);
	PlayerInfo->set_y(Location.Y);
	PlayerInfo->set_z(Location.Z);
	PlayerInfo->set_yaw(GetControlRotation().Yaw);
}

void ABasePlayer::ProcessMovePacket(float DeltaSeconds)
{
	if (IsMyPlayer() == false)
	{
		const Protocol::MoveState State = PlayerInfo->state();

		FQuat NewQuat(FRotator(0, DestInfo->yaw(), 0));
		FQuat CurrentQuat = GetActorQuat();
		FQuat LerpedQuat = FQuat::Slerp(CurrentQuat, NewQuat, DeltaSeconds * 10.f);  // 예측에 사용되는 보간

		FVector DestLocation = FVector(DestInfo->x(), DestInfo->y(), DestInfo->z());
		SetActorLocation(FMath::VInterpConstantTo(GetActorLocation(), DestLocation, DeltaSeconds, DestInfo->speed()));
		SetActorRotation(LerpedQuat.Rotator());

		////////////////////////////////////애니메이션에 사용되는 Angle값 구하기 용도
		DestLocation.Z = GetActorLocation().Z;

		FVector DestVector = DestLocation - GetActorLocation();
		DestVector.Normalize();

		CurrentAngle = FMath::RadiansToDegrees(FMath::Acos(FVector::DotProduct(GetActorForwardVector(), DestVector)));

		float DotProduct = FVector::DotProduct(FVector(0.f, 0.f, 1.f), FVector::CrossProduct(GetActorForwardVector(), DestVector));

		if (0.f > DotProduct)
		{
			CurrentAngle *= -1.f;
		}
	}
}