// Copyright Epic Games, Inc. All Rights Reserved.

using System.Numerics;
using System.Xml.Linq;
using UnrealBuildTool;

public class S1 : ModuleRules
{
	public S1(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "EnhancedInput", "Sockets", "Networking", "UMG", "AIModule", "Niagara", "ModelViewViewModel", "FieldNotification", "Paper2D", "Slate", "SlateCore", "GameplayCameras", "LevelSequence", "MovieScene", "Http" });

		PrivateDependencyModuleNames.AddRange(new string[] { "ProtobufCore"});

		PrivateIncludePaths.AddRange(new string[]
		{
			"S1/",
			"S1/Actor",
            "S1/AnimInstance",
            "S1/Character",
			"S1/Component",
			"S1/Controller",
			"S1/Effects",
			"S1/Enemy",
			"S1/Etc",
			"S1/GameInstance",
			"S1/GameMode",
			"S1/Managers",
			"S1/Network",
            "S1/Network/Protocol",
			"S1/Objects",
			"S1/Objects/Ingame",
			"S1/Objects/Ingame/Tutorial",
            "S1/UI",
			"S1/UI/InGame",
            "S1/UI/InGame/Enemy",
			"S1/UI/InGame/Etc",
            "S1/UI/InGame/Player",
			"S1/UI/Lobby",
			"S1/UI/Login",
			"S1/Utils",
			"S1/ViewModel",
			"S1/Weapon",
        });
	}
}
