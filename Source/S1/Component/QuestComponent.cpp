// Fill out your copyright notice in the Description page of Project Settings.


#include "Component/QuestComponent.h"
#include "Components/WidgetComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values for this component's properties
UQuestComponent::UQuestComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	GoalIconWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("QuestWidget"));
	
	static ConstructorHelpers::FClassFinder<UUserWidget> QUEST_WIDGET(TEXT("/Game/S1/UI/InGame/Etc/WBP_GoalIcon.WBP_GoalIcon_C"));
	if (QUEST_WIDGET.Succeeded())
	{
		GoalIconWidget->SetWidgetSpace(EWidgetSpace::Screen);
		GoalIconWidget->SetWidgetClass(QUEST_WIDGET.Class);
		GoalIconWidget->SetDrawSize(FVector2D(47, 54));
	}
}


// Called when the game starts
void UQuestComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UQuestComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

