// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractComponent.h"
#include "Components/ShapeComponent.h"
#include "HeroPlayer.h"

UInteractComponent::UInteractComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

bool UInteractComponent::Initialize(uint64 ID, UShapeComponent* Collision)
{
	if (Collision == nullptr)
	{
		return false;
	}

	OwnerID = ID;

	Collision->OnComponentBeginOverlap.AddDynamic(this, &UInteractComponent::OnPlayerOverlapBegin);
	Collision->OnComponentEndOverlap.AddDynamic(this, &UInteractComponent::OnPlayerOverlapEnd);

	return true;
}

bool UInteractComponent::DeInitialize(uint64 ID, UShapeComponent* Collision)
{
	if (Collision == nullptr)
	{
		return false;
	}

	UWorld* World = GetWorld();
	if (World == nullptr)
	{
		return false;
	}

	AHeroPlayer* Player = Cast<AHeroPlayer>(UGameplayStatics::GetPlayerCharacter(World, 0));

	if (Player == nullptr)
	{
		return false;
	}

	Collision->OnComponentBeginOverlap.Clear();
	Collision->OnComponentEndOverlap.Clear();

	Player->RemoveInteractObject(ID);

	return true;
}

void UInteractComponent::OnPlayerOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (!OtherActor->IsA(AHeroPlayer::StaticClass()))
	{
		return;
	}

	AHeroPlayer* Player = Cast<AHeroPlayer>(OtherActor);
	if (Player == nullptr)
	{
		return;
	}

	Player->AddInteractObject(OwnerID);
}

void UInteractComponent::OnPlayerOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (!OtherActor->IsA(AHeroPlayer::StaticClass()))
	{
		return;
	}

	AHeroPlayer* Player = Cast<AHeroPlayer>(OtherActor);
	if (Player == nullptr)
	{
		return;
	}

	Player->RemoveInteractObject(OwnerID);
}

