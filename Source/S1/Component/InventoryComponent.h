// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "InventoryComponent.generated.h"

DECLARE_MULTICAST_DELEGATE(FOnRefreshInventory);


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class S1_API UInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UInventoryComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:
	FORCEINLINE int GetMaxSize() const { return SizeOfInven; }
	FORCEINLINE int GetCurrentSize() const { return Inventory.Num(); }
	FORCEINLINE FString GetInventoryAtIndex(int Index) {return Inventory[Index]; }

public:
	void PushItemInInventory(FString ItemID);
	void SortInventory();
	void WriteInvenDataTable();
	bool AddInvenItemData(FString ItemID);
	void ReadInvenDataTable();

public:
	FOnRefreshInventory OnRefreshInventory;

private:
	class UDataTable* InvenData;

	TArray<FString> Inventory;

	int SizeOfInven = 20;
	int NumOfRow = 0;

};
