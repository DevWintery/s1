// Fill out your copyright notice in the Description page of Project Settings.


#include "CoverComponent.h"
#include "HeroPlayer.h"
#include "Components/CapsuleComponent.h"

// Sets default values for this component's properties
UCoverComponent::UCoverComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UCoverComponent::BeginPlay()
{
	Super::BeginPlay();

	// ..
	
}

// Called every frame
void UCoverComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool UCoverComponent::CheckCoverObject()
{
	FVector TraceStart = GetOwner()->GetActorLocation();

	float CapsuleSize = (Cast<AHeroPlayer>(GetOwner())->GetCapsuleComponent())->GetScaledCapsuleHalfHeight();

	TraceStart.Z += CapsuleSize;

	// SphereTrace를 끝낼 위치 설정 (앞으로의 방향으로 1000 유닛만큼 이동)
	FVector TraceEnd = TraceStart + GetOwner()->GetActorForwardVector() * 100.f;

	// SphereTrace 반지름 설정
	float TraceRadius = 10.0f;

	// SphereTrace 쿼리 파라미터 설정
	FCollisionQueryParams TraceParams(FName(TEXT("SphereTrace")), false, GetOwner());

	// SphereTrace를 수행하여 결과 얻기
	FHitResult HitResult;
	bool bHit = GetWorld()->SweepSingleByChannel(HitResult, TraceStart, TraceEnd, FQuat::Identity, ECC_WorldStatic, FCollisionShape::MakeSphere(TraceRadius), TraceParams);

	// SphereTrace 결과를 디버그로 그리기
	DrawDebugSphere(GetWorld(), HitResult.ImpactPoint, TraceRadius, 12, FColor::Red, false, 5.0f);

	return bHit;

}


