// Fill out your copyright notice in the Description page of Project Settings.


#include "Component/InventoryComponent.h"
#include "ItemData.h"
#include "InventoryData.h"
#include "Engine/DataTable.h"

// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...

	NumOfRow = 0;

	static ConstructorHelpers::FObjectFinder<UDataTable> INVENDATA(TEXT("/Game/S1/DataTables/DT_InventoryData.DT_InventoryData"));

	if (INVENDATA.Succeeded())
	{
		InvenData = INVENDATA.Object;
	}

	ReadInvenDataTable();
}

// Called when the game starts
void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...

}

// Called every frame
void UInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UInventoryComponent::PushItemInInventory(FString ItemID)
{
	Inventory.Emplace(ItemID);
	//SortInventory();
}

void UInventoryComponent::SortInventory()
{
	Inventory.Sort([](const FString& A, const FString& B) 
	{

		int AToInt = FCString::Atoi(*A);
		int BtoInt = FCString::Atoi(*B);

		return AToInt < BtoInt;
	});
}

void UInventoryComponent::WriteInvenDataTable()
{
	//SortInventory();

	NumOfRow = 0;

	for (auto& Item : Inventory)
	{
		FInventoryData InventoryData;
		FName Key(*FString::FromInt(NumOfRow));

		InventoryData.ItemID = Item;

		InvenData->AddRow(Key, InventoryData);
		++NumOfRow;
	}
}

bool  UInventoryComponent::AddInvenItemData(FString ItemID)
{
	if (Inventory.Contains(ItemID))
	{
		return false;
	}

	FInventoryData InventoryData;
	FName Key(*FString::FromInt(NumOfRow));

	InventoryData.ItemID = ItemID;
	PushItemInInventory(ItemID);

	InvenData->AddRow(Key, InventoryData);
	++NumOfRow;

	OnRefreshInventory.Broadcast();

	return true;
}

void UInventoryComponent::ReadInvenDataTable()
{
	for (int i = 0; i < SizeOfInven; ++i)
	{
		FName Key(*FString::FromInt(i));

		FInventoryData* InventoryData = InvenData->FindRow<FInventoryData>(Key, FString(""));

		if (InventoryData)
		{
			PushItemInInventory(InventoryData->ItemID);

			++NumOfRow;
		}
	}

	//다 읽고 비움
	//InvenData->EmptyTable();
}


